package main

import (
	"Golang/cmd/router"
	"Golang/pkg/db"
	"log"
	"net/http"
	//"api/pkg/db"
)

func main() {
	// Create db connection
	dbConn, err := db.Connect()
	if err != nil {
		log.Fatalf("db connection failed: %v", err)
	}
	defer dbConn.Close()
	log.Printf("db connected successfully")

	// Init router
	r := router.Init(dbConn)

	// Start server
	log.Printf("server started on port %s", "5000")
	// log.Fatal(http.ListenAndServe(":5000", r))
	http.ListenAndServeTLS(":5000", "cert.pem", "key.pem", corsMiddleware(r))
}

func corsMiddleware(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Set Access-Control-Allow-Origin header to allow access from all domains
		w.Header().Set("Access-Control-Allow-Origin", "*")

		// Set Access-Control-Allow-Headers header to allow access to custom headers
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")

		// Set Access-Control-Allow-Methods header to allow access to custom methods
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")

		// Set Access-Control-Max-Age header to allow preflight requests
		w.Header().Set("Access-Control-Max-Age", "86400")

		// Handle preflight request
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		h.ServeHTTP(w, r)
	}
}

package router

import (
	"context"
	"net/http"

	"github.com/go-chi/jwtauth/v5"
)

func JWTRouterContext(tokenAuth *jwtauth.JWTAuth) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_, claims, err := jwtauth.FromContext(r.Context())

			if err != nil {
				http.Error(w, "Invalid token", http.StatusUnauthorized)
				return
			}

			// Lưu username vào context
			ctx := r.Context()
			ctx = context.WithValue(ctx, "username", claims["username"])
			r = r.WithContext(ctx)

			// Tiếp tục xử lý các middleware và handler tiếp theo
			next.ServeHTTP(w, r)
		})
	}
}

package model

import "time"

type Account struct {
	ID        int64 `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	Gender    int `json:"gender"`
	Avatar    string `json:"avatar"`
	Password  string `json:"password"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

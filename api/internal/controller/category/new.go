package category

import (
	"Golang/internal/repository/category"
)

type Controller struct {
	categoryRepo category.Repository
}

func New(categoryRepo category.Repository) Controller {
	return Controller{categoryRepo: categoryRepo}
}
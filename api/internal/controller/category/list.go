package category

import (
	"github.com/volatiletech/null/v8"
)

type CategoryListOutput struct {
	CategoryID   int
	CategoryName null.String
}
type CategoryListPercentOutput struct {
	PercentID   int
	Percent null.Float64
}

func (c Controller) ListCategory() ([]CategoryListOutput, error) {
	users, err := c.categoryRepo.GetListCategory()
	if err != nil {
		return nil, err
	}

	var result = make([]CategoryListOutput, len(users))
	for ind, cate := range users {
		result[ind] = CategoryListOutput{CategoryID: cate.CategoryID, CategoryName: cate.CategoryName}
	}

	return result, nil

}


func (c Controller) ListCategoryGoal() ([]CategoryListOutput, error) {
	users, err := c.categoryRepo.GetListCategoryGoal()
	if err != nil {
		return nil, err
	}

	var result = make([]CategoryListOutput, len(users))
	for ind, cate := range users {
		result[ind] = CategoryListOutput{CategoryID: cate.CategoryID, CategoryName: cate.CategoryName}
	}

	return result, nil

}

func (c Controller) ListCategoryByID(id int) (CategoryListOutput, error) {
	cate, err := c.categoryRepo.GetListCategoryByID(id)
	if err != nil {
		return CategoryListOutput{}, err
	}

	return CategoryListOutput{CategoryID: cate.CategoryID, CategoryName: cate.CategoryName}, nil

}


func (c Controller) ListCategoryGoalByID(id int) (CategoryListOutput, error) {
	cate, err := c.categoryRepo.GetListCategoryGoalByID(id)
	if err != nil {
		return CategoryListOutput{}, err
	}

	return CategoryListOutput{CategoryID: cate.CategoryID, CategoryName: cate.CategoryName}, nil

}

func (c Controller) ListCategoryPercentByID(id int) (CategoryListPercentOutput, error) {
	cate, err := c.categoryRepo.GetListCategoryPercentByID(id)
	if err != nil {
		return CategoryListPercentOutput{}, err
	}

	return CategoryListPercentOutput{PercentID: cate.PercentID, Percent: cate.Percent}, nil

}


func (c Controller) ListCategoryPercent() ([]CategoryListPercentOutput, error) {
	users, err := c.categoryRepo.GetListCategoryPercent()
	if err != nil {
		return nil, err
	}

	var result = make([]CategoryListPercentOutput, len(users))
	for ind, cate := range users {
		result[ind] = CategoryListPercentOutput{PercentID: cate.PercentID, Percent: cate.Percent}
	}

	return result, nil

}
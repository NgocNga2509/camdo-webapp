package category

func (c Controller) DeleteCategory(id int) error {
	err := c.categoryRepo.DeleteCategory(id)
	if err != nil {
		return err
	}
	return nil
}
func (c Controller) DeleteCategoryGoal(id int) error {
	err := c.categoryRepo.DeleteCategoryGoal(id)
	if err != nil {
		return err
	}
	return nil
}
func (c Controller) DeleteCategoryPercent(id int) error {
	err := c.categoryRepo.DeleteCategoryPercent(id)
	if err != nil {
		return err
	}
	return nil
}
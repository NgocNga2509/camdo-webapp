package customer

import "Golang/internal/repository/customer"

type Controller struct {
	customerRepo customer.Repository
}

func New(customerRepo customer.Repository) Controller {
	return Controller{customerRepo: customerRepo}
}
package customer

import (
	models "Golang/internal/repository/dbmodel"
	"database/sql"
	"errors"

	"github.com/volatiletech/null/v8"
)

type CustomerListOutput struct {
	CustomerID   int64
	CustomerName string
	Address      null.String
	Gender       null.String
	CCCD         null.String
	BirthDate    null.Time
	Phone        null.String
	AddressCCCD  null.String
	DayCCCD      null.Time
}

func (c Controller) ListCustomer() ([]CustomerListOutput, error) {
	users, err := c.customerRepo.GetListCustomer()
	if err != nil {
		return nil, err
	}

	var result = make([]CustomerListOutput, len(users))
	for ind, customer := range users {
		result[ind] = CustomerListOutput{
			CustomerID: customer.CustomerID,
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

func (c Controller) GetNewestCustomer() (models.Customer, error) {
	customer, err := c.customerRepo.GetNewestCustomer()
	if err != nil {
		return models.Customer{}, err
	}

	return models.Customer{
		CustomerID: customer.CustomerID,
		CustomerName: customer.CustomerName,
		Address:      null.String(customer.Address),
		Gender:       null.String(customer.Gender),
		CCCD:         null.String(customer.CCCD),
		BirthDate:    null.Time(customer.BirthDate),
		Phone:        null.String(customer.Phone),
		AddressCCCD:  null.String(customer.AddressCCCD),
		DayCCCD:      null.Time(customer.DayCCCD),
	}, nil
}

func (c Controller) GetCustomerByID(id int64) (models.Customer, error) {
	customer, err := c.customerRepo.GetCustomerByID(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return models.Customer{}, errors.New("Customer not found!")
		}
		return models.Customer{}, err
	}

	return models.Customer{
		CustomerID: customer.CustomerID,
		CustomerName: customer.CustomerName,
		Address:      null.String(customer.Address),
		Gender:       null.String(customer.Gender),
		CCCD:         null.String(customer.CCCD),
		BirthDate:    null.Time(customer.BirthDate),
		Phone:        null.String(customer.Phone),
		AddressCCCD:  null.String(customer.AddressCCCD),
		DayCCCD:      null.Time(customer.DayCCCD),
	}, nil
}

package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"time"

	"github.com/volatiletech/null/v8"
)

type CreatePayInput struct {
	OrderID      null.Int64
	Amount       null.Int
	PaymentDate  null.Time
	InterestPaid null.Bool
	CountDate    null.Int
	NextPaymentDate null.Time
}

func (c Controller) CreatePay(inp CreatePayInput) (models.InterestPaymentsVehicle, error) {
	return c.orderRepo.CreatePayVehicle(models.InterestPaymentsVehicle{
		OrderID:     inp.OrderID,
		Amount:      inp.Amount,
		PaymentDate: inp.PaymentDate,
		NextPaymentDate: inp.NextPaymentDate,
	})
}

func (c Controller) ListDayPaymentNewest(id int64) (time.Time, error) {
	payment, err := c.orderRepo.ListDayPaymentNewest(id)
	if err != nil {
		return time.Time{}, err
	}
	return payment, nil
}

func (c Controller) CalculateInterest(amount int, interest float64, startDate, endDate time.Time) float64 {
	interest = c.orderRepo.CalculateInterest(amount, interest,startDate, endDate)
	return interest
}
func (c Controller) ListOrderVehicleByPaymentOver() (models.OrderVehicleSlice, models.InterestPaymentsVehicleSlice, error) {
	orderGoal,overduePayments, err := c.orderRepo.ListOrderVehicleByPaymentOver()
	if err != nil {
		return models.OrderVehicleSlice{},models.InterestPaymentsVehicleSlice{}, err
	}
	return orderGoal,overduePayments, nil
}
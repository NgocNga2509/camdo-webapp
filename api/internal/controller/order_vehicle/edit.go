package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type OrderInputEdit struct {
	ProductName       null.String
	EstimatedPrice    null.Int
	Price             null.Int
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Float64
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
	LicensePlate null.String
}


func (c Controller) EditOrder(orderID int64, update models.OrderVehicle) (models.OrderVehicle, error) {
	// Gọi hàm EditOrder từ repository để xử lý cập nhật order
	updatedOrder, err := c.orderRepo.EditOrder(orderID, update)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return updatedOrder, nil
}


func (c Controller) UpdateNote(note null.String,miss null.Int,orderID int64) (models.OrderVehicle, error){
	updatedOrder, err := c.orderRepo.UpdateNote(note,miss, orderID)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return updatedOrder, nil
}
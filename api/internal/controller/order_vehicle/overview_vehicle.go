package ordervehicle

import models "Golang/internal/repository/dbmodel"

type OutputOverView struct {
	AmountRevenue int64
	AmountCollect int64
	AmountSpend   int64
}

type Data struct {
	Thang    int
	Nam      int
	LoiNhuan int64
	Thu      int64
	Chi      int64
}

type RevenueOverviewOutput struct {
	Order    models.OrderVehicle            `json:"order"`
	Payments models.InterestPaymentsVehicle `json:"payments"`
}
type RedeemOverviewOutput struct {
	Order  models.OrderVehicle       `json:"order"`
	Redeem models.RedeemOrderVehicle `json:"redeem"`
}

type AddPaymentOverviewOutput struct {
	Order      models.OrderVehicle       `json:"order"`
	AddPayment models.AddPaymentsVehicle `json:"add_payment"`
}

func (c Controller) ExportOverView(month int, year int) (Data, error) {
	amountSpend, err := c.orderRepo.GetSpendVehicle(month, year)
	if err != nil {
		amountSpend = 0
	}

	amountRevenue, err := c.orderRepo.GetRevenueVehicle(month, year)
	if err != nil {
		amountRevenue = 0
	}

	amountCollect, err := c.orderRepo.GetCollectVehicle(month, year)
	if err != nil {
		amountCollect = 0
	}
	return Data{
		Thang:    month,
		Nam:      year,
		LoiNhuan: amountRevenue,
		Thu:      amountCollect,
		Chi:      amountSpend,
	}, nil
}

func (c Controller) GetOverView(month int, year int) (OutputOverView, error) {
	amountSpend, err := c.orderRepo.GetSpendVehicle(month, year)
	if err != nil {
		amountSpend = 0
	}

	amountRevenue, err := c.orderRepo.GetRevenueVehicle(month, year)
	if err != nil {
		amountRevenue = 0
	}

	amountCollect, err := c.orderRepo.GetCollectVehicle(month, year)
	if err != nil {
		amountCollect = 0
	}

	return OutputOverView{
		AmountRevenue: amountRevenue,
		AmountCollect: amountCollect,
		AmountSpend:   amountSpend,
	}, nil
}

func (c Controller) ListOrderVehicleByMonthAndYear(month int, year int) ([]models.OrderVehicle, error) {
	orderQuery, err := c.orderRepo.ListOrderVehicleByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}

	var orders []models.OrderVehicle
	for _, o := range orderQuery {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			LicensePlate:      o.LicensePlate,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Code:              o.Code,
			STT:               o.STT,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (c Controller) ListPaymentByMonthAndYear(month int, year int) ([]RevenueOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListPaymentsVehicleByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}
	var result []RevenueOverviewOutput
	for _, inp := range orderQuery {
		payments := models.InterestPaymentsVehicle{
			PaymentID:       inp.PaymentID,
			OrderID:         inp.OrderID,
			PaymentDate:     inp.PaymentDate,
			NextPaymentDate: inp.NextPaymentDate,
			Amount:          inp.Amount,
		}
		order, err := c.orderRepo.GetOrderVehicleByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := models.OrderVehicle{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			LicensePlate:      order.LicensePlate,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
			Code:              order.Code,
			STT:               order.STT,
		}

		paymentOrder := RevenueOverviewOutput{
			Order:    convertedOrder,
			Payments: payments,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}
func (c Controller) ListRedeemByMonthAndYear(month int, year int) ([]RedeemOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListRedeemVehicleByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}

	var result []RedeemOverviewOutput
	for _, inp := range orderQuery {
		payments := models.RedeemOrderVehicle{
			ID:         inp.ID,
			DateRedeem: inp.DateRedeem,
			OrderID:    inp.OrderID,
			Amount:     inp.Amount,
		}
		order, err := c.orderRepo.GetOrderVehicleByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := models.OrderVehicle{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			LicensePlate:      order.LicensePlate,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
			Code:              order.Code,
			STT:               order.STT,
		}

		paymentOrder := RedeemOverviewOutput{
			Order:  convertedOrder,
			Redeem: payments,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}
func (c Controller) ListAddPaymentByMonthAndYear(month int, year int) ([]AddPaymentOverviewOutput, error) {
	addPayments, err := c.orderRepo.ListAddPaymentByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}

	var result []AddPaymentOverviewOutput
	for _, addPayment := range addPayments {
		addPaymentCoverted := models.AddPaymentsVehicle{
			ID:             addPayment.ID,
			IDVehicle:      addPayment.IDVehicle,
			Amount:         addPayment.Amount,
			DateAddPayment: addPayment.DateAddPayment,
		}
		order, err := c.orderRepo.GetOrderVehicleByID(addPayment.IDVehicle.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := models.OrderVehicle{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			LicensePlate:      order.LicensePlate,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
			Code:              order.Code,
			STT:               order.STT,
		}

		paymentOrder := AddPaymentOverviewOutput{
			AddPayment: addPaymentCoverted,
			Order:      convertedOrder,
		}
		result = append(result, paymentOrder)

	}
	return result, nil

}

package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

func (c Controller) UpdateStatus(id int64, status null.String) (models.OrderVehicle, error) {
	order, err := c.orderRepo.UpdateStatus(id, status)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return models.OrderVehicle{
		ID:                order.ID,
		ProductName:       order.ProductName,
		Price:             order.Price,
		OrderDate:         order.OrderDate,
		Percent:           order.Percent,
		CustomerID:        order.CustomerID,
		ReturnDate:        order.ReturnDate,
		InterestPaidCount: order.InterestPaidCount,
		Status:            order.Status,
	}, nil

}

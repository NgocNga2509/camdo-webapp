package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type OrderInput struct {
	ProductName       null.String
	LicensePlate      null.String
	AddPayments    null.Int
	Price             null.Int
	OrderDate         null.Time
	ReturnDate        null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	Note              null.String
	Status            null.String
	InterestPaidCount null.Int
	STT               null.Int64
	Code              null.String
}

func (c Controller) CreateOrderVehicle(inp OrderInput) (models.OrderVehicle, error) {
	return c.orderRepo.CreateOrderVehicle(models.OrderVehicle{
		LicensePlate:      inp.LicensePlate,
		ProductName:       inp.ProductName,
		Price:             inp.Price,
		OrderDate:         inp.OrderDate,
		ReturnDate:        inp.ReturnDate,
		CustomerID:        inp.CustomerID,
		Percent:           inp.Percent,
		Note:              inp.Note,
		Code:              inp.Code,
		STT:               inp.STT,
		Status:            inp.Status,
		InterestPaidCount: inp.InterestPaidCount,
	})
}

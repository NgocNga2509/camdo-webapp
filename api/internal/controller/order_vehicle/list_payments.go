package ordervehicle

import models "Golang/internal/repository/dbmodel"

func (c Controller) GetListPaymentByOrderID(id int64) ([]models.InterestPaymentsGoal, error) {
	payments, err := c.orderRepo.GetListPaymentByOrderID(id)
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsGoal, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsGoal{Amount:inp.Amount,PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate }
	}

	return result, nil
}

func (c Controller) GetListPaymentsVehicle() ([]models.InterestPaymentsVehicle, error) {
	payments, err := c.orderRepo.GetListPaymentsVehicle()
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsVehicle, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsVehicle{NextPaymentDate: inp.NextPaymentDate,OrderID: inp.OrderID, Amount: inp.Amount, PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate}
	}

	return result, nil
}
func (c Controller) GetCountOrderVehicleOver() (int, error) {
	orders, err := c.orderRepo.ListOrdersVehicleOver()
	count := 0
	if err != nil {
		return 0, err
	} else {
		count += len(orders)
	}
	orderVehicle,_, err := c.orderRepo.ListOrderVehicleByPaymentOver()
	if err != nil {
		return 0, err
	} else {
		count += len(orderVehicle)
	}
	return count, nil

}

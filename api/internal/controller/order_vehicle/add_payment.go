package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"Golang/internal/repository/order_vehicle"

	"github.com/volatiletech/null/v8"
)

type UpdateInputAddPayment = ordervehicle.UpdateInputAddPayment

type UpdateSpendVehicle struct {
	Amount null.Int64
	Month  null.Int
	Year   null.Int
}

func (c Controller) UpdateAddPaymentVehicle(inp UpdateInputAddPayment) (models.OrderVehicle, error) {
	order, err := c.orderRepo.UpdatePaymentVehicle(inp)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return order, nil
}
func (c Controller) UpdateSpendVehicle(month, year int, amount int64) (models.SpendVehicle, error) {
	order, err := c.orderRepo.UpdateSpendVehicle(month, year, amount)
	if err != nil{
		return models.SpendVehicle{}, err
	}
	return order, nil
}


func (c Controller) GetAddPaymentInfo(id int64) ([]models.AddPaymentsVehicle, error){
	addPayment, err := c.orderRepo.GetAddPaymentInfo(id)
	if err!= nil{
		return nil, err
	}
	var result = make([]models.AddPaymentsVehicle, len(addPayment))
	for ind, m := range addPayment {
		result[ind] = models.AddPaymentsVehicle{
			ID:             m.ID,
			IDVehicle:         m.IDVehicle,
			Amount:         m.Amount,
			DateAddPayment: m.DateAddPayment,
		}
	}

	return result, nil

}
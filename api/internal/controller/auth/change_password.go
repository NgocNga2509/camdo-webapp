package auth

import (
	"Golang/internal/repository/account"
	models "Golang/internal/repository/dbmodel"
)

type InputUpdatePass = account.InputUpdatePass

func (c Controller) ChangePassword(input InputUpdatePass) error {
	err := c.accountRepo.ChangePassword(input)
	if err != nil {
		return err
	}
	return nil
}

func (c Controller) GetByUserName(username string) (models.Account, error) {
	user, err := c.accountRepo.GetByUserName(username)
	if err != nil {
		return models.Account{}, err
	}

	return models.Account{
		Name: user.Name,
		Username: user.Username,
		Password: user.Password,
	}, nil
}

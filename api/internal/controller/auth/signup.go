package auth

import (
	models "Golang/internal/repository/dbmodel"
	"errors"
	"fmt"
)

// Request body for sign in endpoint
type SignUpInput struct {
	Name     string
	Username string
	Gender   int
	Avatar   string
	ID       string
	Password string
}

func (c Controller) SignUp(inp SignUpInput) (models.Account, error) {
	checkUser, err := c.accountRepo.GetByUserName(inp.Username)
	if err == nil  {
		return models.Account{}, errors.New("Username is duplicate!")
	}
	fmt.Print(checkUser)

	return c.accountRepo.CreateAccount(models.Account{
		Name:     inp.Name,
		Username: inp.Username,
		Gender:   inp.Gender,
		Avatar:   inp.Avatar,
		ID:       inp.ID,
		Password: inp.Password,
	})

}

package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"Golang/internal/repository/order_goal"

	"github.com/volatiletech/null/v8"
)

type UpdateInputAddPayment = order_goal.UpdateInputAddPayment

type UpdateSpendGoal struct {
	Amount null.Int64
	Month  null.Int
	Year   null.Int
}

func (c Controller) UpdateAddPaymentGoal(inp UpdateInputAddPayment) (models.OrderGoal, error) {
	order, err := c.orderRepo.UpdatePaymentGoal(inp)
	if err != nil {
		return models.OrderGoal{}, err
	}

	return order, nil
}
func (c Controller) UpdateSpendGoal(month, year int, amount int64) (models.SpendGoal, error) {
	order, err := c.orderRepo.UpdateSpendGoal(month, year, amount)
	if err != nil{
		return models.SpendGoal{}, err
	}
	return order, nil
}

func (c Controller) GetAddPaymentInfo(id int64) ([]models.AddPaymentsGoal, error){
	addPayment, err := c.orderRepo.GetAddPaymentInfo(id)
	if err!= nil{
		return nil, err
	}
	var result = make([]models.AddPaymentsGoal, len(addPayment))
	for ind, m := range addPayment {
		result[ind] = models.AddPaymentsGoal{
			ID:             m.ID,
			IDGold:         m.IDGold,
			Amount:         m.Amount,
			DateAddPayment: m.DateAddPayment,
		}
	}

	return result, nil

}
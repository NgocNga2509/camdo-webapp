package order_goal

import models "Golang/internal/repository/dbmodel"

func (c Controller) GetListPaymentByOrderID(id int64) ([]models.InterestPaymentsGoal, error) {
	payments, err := c.orderRepo.GetListPaymentByOrderID(id)
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsGoal, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsGoal{Amount: inp.Amount, PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate}
	}

	return result, nil
}
func (c Controller) GetListPaymentsGoal() ([]models.InterestPaymentsGoal, error) {
	payments, err := c.orderRepo.GetListPaymentsGoal()
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsGoal, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsGoal{NextPaymentDate: inp.NextPaymentDate, OrderID: inp.OrderID, Amount: inp.Amount, PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate}
	}

	return result, nil
}

func (c Controller) GetCountOrderGoalOver() (int, error) {
	orders, err := c.orderRepo.ListOrdersGoalOver()
	count := 0
	if err != nil {
		return 0, err
	} else {
		count += len(orders)
	}
	orderGoal, _, err := c.orderRepo.ListOrderGoalByPaymentOver()
	if err != nil {
		return 0, err
	} else {
		count += len(orderGoal)
	}
	return count, nil

}


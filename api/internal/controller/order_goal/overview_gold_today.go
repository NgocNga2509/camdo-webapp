package order_goal

import (
	models "Golang/internal/repository/dbmodel"
)

type OverviewTodayOutput struct {
	Revenue int64
	Collect int64
	Spend   int64
}

func (c Controller) ListOrderGoldToday() ([]OrderOverviewOutput, error) {
	orderQuery, err := c.orderRepo.ListOrderGoldToday()
	if err != nil {
		return nil, err
	}

	var result []OrderOverviewOutput
	for _, order := range orderQuery {
		convertedOrder := OrderGoalOutput{
			ID:                order.ID,
			ProductName:       order.ProductName,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			AddPayments:       order.AddPayments,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
			Code:              order.Code,
			STT:               order.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := OrderOverviewOutput{
			Order:    convertedOrder,
			Products: details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}

func (c Controller) ListPaymentToday() ([]RevenueOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListPaymentsGoldToday()
	if err != nil {
		return nil, err
	}
	var result []RevenueOverviewOutput
	for _, inp := range orderQuery {
		payments := models.InterestPaymentsGoal{
			PaymentID:       inp.PaymentID,
			OrderID:         inp.OrderID,
			PaymentDate:     inp.PaymentDate,
			NextPaymentDate: inp.NextPaymentDate,
			Amount:          inp.Amount,
		}
		order, err := c.orderRepo.GetOrderGoalByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := OrderGoalOutput{
			ID:                order.ID,
			ProductName:       order.ProductName,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			AddPayments:       order.AddPayments,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
			Code:              order.Code,
			STT:               order.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := RevenueOverviewOutput{
			Order:    convertedOrder,
			Payments: payments,
			Products: details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}
func (c Controller) ListRedeemToday() ([]RedeemOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListRedeemGoldToday()
	if err != nil {
		return nil, err
	}

	var result []RedeemOverviewOutput
	for _, inp := range orderQuery {
		payments := models.RedeemOrderGoal{
			ID:         inp.ID,
			DateRedeem: inp.DateRedeem,
			OrderID:    inp.OrderID,
			Amount:     inp.Amount,
		}
		order, err := c.orderRepo.GetOrderGoalByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := OrderGoalOutput{
			ID:                order.ID,
			ProductName:       order.ProductName,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			AddPayments:       order.AddPayments,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
			Code:              order.Code,
			STT:               order.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := RedeemOverviewOutput{
			Order:    convertedOrder,
			Redeem:   payments,
			Products: details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}

func (c Controller) ListAddPaymentToday() ([]AddPaymentOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListAddPaymentGoldToday()
	if err != nil {
		return nil, err
	}

	var result []AddPaymentOverviewOutput
	for _, order := range orderQuery {
		convertedOrder := models.AddPaymentsGoal{
			ID:             order.ID,
			IDGold:         order.IDGold,
			Amount:         order.Amount,
			DateAddPayment: order.DateAddPayment,
		}
		o, err := c.orderRepo.GetOrderGoalByID(int64(order.IDGold.Int64))
		if err != nil {
			return nil, err
		}
		convertedOrder1 := OrderGoalOutput{
			ID:                o.ID,
			ProductName:       o.ProductName,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			OrderDate:         o.OrderDate,
			CustomerID:        o.CustomerID,
			Percent:           o.Percent,
			AddPayments:       o.AddPayments,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			InterestPaidCount: o.InterestPaidCount,
			Note:              o.Note,
			Code:              o.Code,
			STT:               o.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.IDGold.Int64))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := AddPaymentOverviewOutput{
			Order:      convertedOrder1,
			AddPayment: convertedOrder,
			Products:   details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}

func (c Controller) GetOverviewToday() (OverviewTodayOutput, error) {
	amountSpend := 0
	amountCollect := 0
	amountRevenue := 0
	//Chi
	orders, err := c.orderRepo.ListOrderGoldToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, order := range orders {
		amountSpend += order.PriceOld.Int
	}

	addPayments, err := c.orderRepo.GetListAddPaymentGoldToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, addPayment := range addPayments {
		amountSpend += addPayment.Amount.Int
	}
	//Thu
	payments, err := c.orderRepo.GetListPaymentsGoldToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, payment := range payments {
		//Loi nhuan
		amountRevenue += payment.Amount.Int
		//Thu
		amountCollect += payment.Amount.Int
	}
	redeems, err := c.orderRepo.GetListRedeemGoldToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, redeem := range redeems {
		amountCollect += redeem.Amount.Int
	}
	return OverviewTodayOutput{
		Revenue: int64(amountRevenue),
		Collect: int64(amountCollect),
		Spend:   int64(amountSpend),
	}, nil
}

// func (c Controller) GetOverviewMonth() (OverviewTodayOutput, error) {
// 	amountSpend := 0
// 	amountCollect := 0
// 	amountRevenue := 0
// 	//Chi
// 	orders, err := c.orderRepo.ListOrderGoldToday()
// 	if err != nil {
// 		return OverviewTodayOutput{}, err
// 	}
// 	for _, order := range orders {
// 		amountSpend += order.PriceOld.Int
// 	}

// 	addPayments, err := c.orderRepo.GetListAddPaymentGoldToday()
// 	if err != nil {
// 		return OverviewTodayOutput{}, err
// 	}
// 	for _, addPayment := range addPayments {
// 		amountSpend += addPayment.Amount.Int
// 	}
// 	//Thu
// 	payments, err := c.orderRepo.GetListPaymentsGoldToday()
// 	if err != nil {
// 		return OverviewTodayOutput{}, err
// 	}
// 	for _, payment := range payments {
// 		//Loi nhuan
// 		amountRevenue += payment.Amount.Int
// 		//Thu
// 		amountCollect += payment.Amount.Int
// 	}
// 	redeems, err := c.orderRepo.GetListRedeemGoldToday()
// 	if err != nil {
// 		return OverviewTodayOutput{}, err
// 	}
// 	for _, redeem := range redeems {
// 		amountCollect += redeem.Amount.Int
// 	}
// 	return OverviewTodayOutput{
// 		Revenue: int64(amountRevenue),
// 		Collect: int64(amountCollect),
// 		Spend:   int64(amountSpend),
// 	}, nil
// }

func (c Controller) GetOverviewByMonth(month int, year int) (OverviewTodayOutput, error) {
	amountSpend := 0
	amountCollect := 0
	amountRevenue := 0
	// Chi
	orders, err := c.orderRepo.ListOrderGoalByMonthAndYear(month, year)
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, order := range orders {
		amountSpend += order.PriceOld.Int
	}

	addPayments, err := c.orderRepo.ListAddPaymentByMonthAndYear(month, year)
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, addPayment := range addPayments {
		amountSpend += addPayment.Amount.Int
	}

	// Thu
	payments, err := c.orderRepo.GetListPaymentsGoalByMonthAndYear(month, year)
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, payment := range payments {
		// Lợi nhuận
		amountRevenue += payment.Amount.Int
		// Thu
		amountCollect += payment.Amount.Int
	}

	redeems, err := c.orderRepo.GetListRedeemGoalByMonthAndYear(month, year)
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, redeem := range redeems {
		amountCollect += redeem.Amount.Int
	}

	return OverviewTodayOutput{
		Revenue: int64(amountRevenue),
		Collect: int64(amountCollect),
		Spend:   int64(amountSpend),
	}, nil
}

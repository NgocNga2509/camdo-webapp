package order_goal

import "Golang/internal/repository/order_goal"

type Controller struct {
	orderRepo order_goal.Repository
}

func New(orderRepo order_goal.Repository) Controller {
	return Controller{orderRepo: orderRepo}
}

package order_goal

import "github.com/volatiletech/null/v8"

type OrderOverOutput struct {
	Order      OrderGoalOutput      `json:"order"`
	Products   []ProductsOutput     `json:"products"`
	AddPayment []AddPaymentOutput   `json:"add_payments"`
	Payments   []InterestPaidOutput `json:"payments"`
}

func (c Controller) GetOrderGoldOver() ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.GetOverGold()
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			AddPayments:       inp.AddPayments,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
			Code:              inp.Code,
			STT:               inp.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			Products:   details,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) GetOrderHavePaymentsOver() ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.GetListOrderHavePaymentOver()
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			AddPayments:    inp.AddPayments,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Code:              inp.Code,
			STT:               inp.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			Products:   details,
			AddPayment: convertedAddPayments,
			Payments:   convertedPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) SearchOrderGoalOverByID(id int64) ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.SearchOrderOverGoalByID(id)
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			AddPayments:    inp.AddPayments,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Code:              inp.Code,
			STT:               inp.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}

		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			Products:   details,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}
func (c Controller) SearchOrderGoalOverPaymentByID(id int64) ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.SearchOrderOverPaymentGoalByID(id)
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			AddPayments:    inp.AddPayments,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Code:              inp.Code,
			STT:               inp.STT,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			Products:   details,
			AddPayment: convertedAddPayments,
			Payments:   convertedPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

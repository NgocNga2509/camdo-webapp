package order_goal

import (
	"github.com/volatiletech/null/v8"
)

func (c Controller) SearchOrderGoalByID(id int64) ([]OrderProduct, error) {
	order, err := c.orderRepo.SearchOrderGoalByID(id)
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, o := range order {
		order := OrderGoalOutput{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
		// Lấy danh sách sản phẩm tương ứng với đơn hàng
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(o.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(o.ID))
		if err != nil {
			return nil, err
		}
		// Convert the products to the appropriate type
		var convertedProducts []ProductGoldOutput
		for _, product := range products {
			convertedProduct := ProductGoldOutput{
				ID:               product.ID,
				CategoryGoalID:   product.CategoryGoalID,
				CategoryGoldName: product.CategoryGoldName,
			}
			convertedProducts = append(convertedProducts, convertedProduct)
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(o.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderProduct{
			Order:      order,
			Products:   details,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) SearchOrderGoalBySTT(stt int64, code string) ([]OrderProduct, error) {
	order, err := c.orderRepo.SearchOrderGoalBySTT(stt, code)
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, o := range order {
		order := OrderGoalOutput{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
		// Lấy danh sách sản phẩm tương ứng với đơn hàng
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(o.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(o.ID))
		if err != nil {
			return nil, err
		}
		// Convert the products to the appropriate type
		var convertedProducts []ProductGoldOutput
		for _, product := range products {
			convertedProduct := ProductGoldOutput{
				ID:               product.ID,
				CategoryGoalID:   product.CategoryGoalID,
				CategoryGoldName: product.CategoryGoldName,
			}
			convertedProducts = append(convertedProducts, convertedProduct)
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(o.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderProduct{
			Order:      order,
			Products:   details,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) SearchOrderGoalByMissAndCode(stt int64, code string, miss int) ([]OrderProduct, error) {
	order, err := c.orderRepo.SearchOrderGoalByMissAndCode(stt, code, miss)
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, o := range order {
		order := OrderGoalOutput{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
		// Lấy danh sách sản phẩm tương ứng với đơn hàng
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(o.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(o.ID))
		if err != nil {
			return nil, err
		}
		// Convert the products to the appropriate type
		var convertedProducts []ProductGoldOutput
		for _, product := range products {
			convertedProduct := ProductGoldOutput{
				ID:               product.ID,
				CategoryGoalID:   product.CategoryGoalID,
				CategoryGoldName: product.CategoryGoldName,
			}
			convertedProducts = append(convertedProducts, convertedProduct)
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(o.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderProduct{
			Order:      order,
			Products:   details,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) SearchOrderGoalByMiss(miss int) ([]OrderProduct, error) {
	order, err := c.orderRepo.SearchOrderGoalByMiss(miss)
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, o := range order {
		order := OrderGoalOutput{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
		// Lấy danh sách sản phẩm tương ứng với đơn hàng
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(o.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(o.ID))
		if err != nil {
			return nil, err
		}
		// Convert the products to the appropriate type
		var convertedProducts []ProductGoldOutput
		for _, product := range products {
			convertedProduct := ProductGoldOutput{
				ID:               product.ID,
				CategoryGoalID:   product.CategoryGoalID,
				CategoryGoldName: product.CategoryGoldName,
			}
			convertedProducts = append(convertedProducts, convertedProduct)
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(o.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderProduct{
			Order:      order,
			Products:   details,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

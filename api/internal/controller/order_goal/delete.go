package order_goal

import "context"

func (c Controller) DeleteOrderGoal(ctx context.Context, id int64) error {
	err := c.orderRepo.DeleteOrderGoal(ctx, id)
	if err != nil {
		return err
	}
	return nil
}
package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"time"

	"github.com/volatiletech/null/v8"
)

type CreatePayInput struct {
	OrderID      null.Int64
	Amount       null.Int
	PaymentDate  null.Time
	NextPaymentDate null.Time
	InterestPaid null.Bool
	CountDate    null.Int
}

func (c Controller) CreatePay(inp CreatePayInput) (models.InterestPaymentsGoal, error) {
	return c.orderRepo.CreatePayGoal(models.InterestPaymentsGoal{
		OrderID:     inp.OrderID,
		Amount:      inp.Amount,
		PaymentDate: inp.PaymentDate,
		NextPaymentDate: inp.NextPaymentDate,
	})
}
func (c Controller) ListDayPaymentNewest(id int64) (time.Time, error) {
	payment, err := c.orderRepo.ListDayPaymentNewest(id)
	if err != nil {
		return time.Time{}, err
	}
	return payment, nil
}



func (c Controller) CalculateInterest(amount int, interest float64, startDate, endDate time.Time) float64 {
	interest = c.orderRepo.CalculateInterest(amount, interest, startDate, endDate)
	return interest
}

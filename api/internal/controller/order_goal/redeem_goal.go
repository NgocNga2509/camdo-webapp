package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"Golang/internal/repository/order_goal"
	"Golang/pkg/db"

	"github.com/volatiletech/null/v8"
)

type RedeemInput struct {
	ID                int64                       `json:"id"`
	Status            null.String                 `json:"status"`
	ModelInput        models.InterestPaymentsGoal `json:"model_input"`
	CreateRedeemInput models.RedeemOrderGoal      `json:"create_redeem"`
}

func (c Controller) RedeemOrderGoal(inp RedeemInput) (models.OrderGoal, error) {

	tx, err := db.Database.Db.Begin()
	if err != nil {
		return models.OrderGoal{}, err
	}

	txOrderRepo := order_goal.New(tx)
	if inp.ModelInput.Amount.Valid && inp.ModelInput.Amount.Int > 0 {
		_, err = txOrderRepo.CreatePayGoal(inp.ModelInput)
		if err != nil {
			tx.Rollback()
			return models.OrderGoal{}, err
		}
	}

	order, err := txOrderRepo.UpdateStatus(inp.ID, inp.Status)
	if err != nil {
		tx.Rollback()
		return models.OrderGoal{}, err
	}

	_, err = txOrderRepo.CreateRedeemGoal(inp.CreateRedeemInput)
	if err != nil {
		tx.Rollback()
		return models.OrderGoal{}, err
	}

	tx.Commit()

	return models.OrderGoal{
		ID:                order.ID,
		ProductName:       order.ProductName,
		Price:             order.Price,
		OrderDate:         order.OrderDate,
		CustomerID:        order.CustomerID,
		Percent:           order.Percent,
		AddPayments:       order.AddPayments,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		InterestPaidCount: order.InterestPaidCount,
	}, nil

}

func (c Controller) ListRedeemGoal(orderID int64) (models.RedeemOrderGoal, error) {
	redeemGoal, err := c.orderRepo.ListRedeemGoal(orderID)
	if err != nil {
		return models.RedeemOrderGoal{}, err
	}

	return models.RedeemOrderGoal{
		ID:         redeemGoal.ID,
		OrderID:    redeemGoal.OrderID,
		Amount:     redeemGoal.Amount,
		DateRedeem: redeemGoal.DateRedeem,
	}, nil
}

func (c Controller) GetListRedeemGoal() ([]models.RedeemOrderGoal, error) {
	redeemGoal, err := c.orderRepo.GetListRedeemGoal()
	if err != nil {
		return nil, err
	}
	var result = make([]models.RedeemOrderGoal, len(redeemGoal))
	for ind, inp := range redeemGoal {
		result[ind] = models.RedeemOrderGoal{Amount: inp.Amount, OrderID: inp.OrderID, DateRedeem: inp.DateRedeem, ID: inp.ID}
	}

	return result, nil
}

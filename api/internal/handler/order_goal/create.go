package order_goal

import (
	"Golang/internal/controller/order_goal"
	models "Golang/internal/repository/dbmodel"
	"encoding/json"
	"net/http"

	"github.com/volatiletech/null/v8"
)

type CreateOrderInput struct {
	ProductName    null.String
	AddPayments    null.Int
	Price          null.Int
	OrderDate      null.Time
	CustomerID     null.Int64
	Percent        null.Int64
	WeightGoal     null.Float64
	Note           null.String
}

type CreateOrderOutput struct {
	ProductName       null.String  `boil:"product_name" json:"product_name,omitempty" toml:"product_name" yaml:"product_name,omitempty"`
	AddPayments    null.Int     `json:"add_payments"`
	Price             null.Int     `boil:"price" json:"price,omitempty" toml:"price" yaml:"price,omitempty"`
	OrderDate         null.Time    `boil:"order_date" json:"order_date,omitempty" toml:"order_date" yaml:"order_date,omitempty"`
	CustomerID        null.Int64   `boil:"customer_id" json:"customer_id,omitempty" toml:"customer_id" yaml:"customer_id,omitempty"`
	Percent           null.Int64 `boil:"percent" json:"percent,omitempty" toml:"percent" yaml:"percent,omitempty"`
	ReturnDate        null.Time    `boil:"return_date" json:"return_date,omitempty" toml:"return_date" yaml:"return_date,omitempty"`
	Status            null.String  `boil:"status" json:"status,omitempty" toml:"status" yaml:"status,omitempty"`
	InterestPaidCount null.Int     `boil:"interest_paid_count" json:"interest_paid_count,omitempty" toml:"interest_paid_count" yaml:"interest_paid_count,omitempty"`
	Note              null.String  `json:"note"`
}
type ProductInput struct {
	IDGoal         int `json:"idGoal"`
	CategoryID     int `json:"categoryID"`
	CategoryGoalID int `json:"categoryGoalID"`
}

//	func validateDataInput(inp CreateOrderOutput) (order_goal.OrderInput, error) {
//		return order_goal.OrderInput{
//			CategoryGoalID:    inp.CategoryGoalID,
//			Status:            inp.Status,
//			InterestPaidCount: inp.InterestPaidCount,
//			ReturnDate:        inp.ReturnDate,
//			CategoryID:        inp.CategoryID,
//			Percent:           inp.Percent,
//			ProductName:       null.String(inp.ProductName),
//			EstimatedPrice:    inp.EstimatedPrice,
//			Price:             null.Int(inp.Price),
//			WeightGoal:        inp.WeightGoal,
//			OrderDate:         null.Time(inp.OrderDate),
//			CustomerID:        inp.CustomerID,
//			Note:              inp.Note,
//		}, nil
//	}

type ProductsStruct struct {
	Product        models.ProductGold         `json:"product"`
	DetailProducts []models.DetailProductGold `json:"detail_products"`
}

func convertDataProduct(inp []ProductsStruct) []order_goal.ProductsStruct {
	var result []order_goal.ProductsStruct
	for _, p := range inp {
		result = append(result, order_goal.ProductsStruct{
			Product:        p.Product,
			DetailProducts: p.DetailProducts,
		})
	}
	return result
}

func (h Handler) CreateOrder(w http.ResponseWriter, r *http.Request) {
	// Đọc dữ liệu từ yêu cầu HTTP
	var requestData struct {
		Order    CreateOrderOutput `json:"order"`
		Products []ProductsStruct  `json:"products"`
	}

	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	productsData := convertDataProduct(requestData.Products)

	create, err := h.orderController.CreateOrder(models.OrderGoal{
		ProductName:       requestData.Order.ProductName,
		AddPayments:       requestData.Order.AddPayments,
		Price:             requestData.Order.Price,
		OrderDate:         requestData.Order.OrderDate,
		Percent:           requestData.Order.Percent,
		CustomerID:        requestData.Order.CustomerID,
		ReturnDate:        requestData.Order.ReturnDate,
		Status:            requestData.Order.Status,
		InterestPaidCount: requestData.Order.InterestPaidCount,
		Note:              requestData.Order.Note,
	}, productsData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Trả về đối tượng JSON đã tạo
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(create)
}

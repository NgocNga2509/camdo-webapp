package order_goal

import (
	"encoding/json"
	"net/http"
	"Golang/internal/controller/order_goal"

)

type InputSearchSTT struct {
	STT int64 `json:"stt"`
	Code string `json:"code"`
	Miss int `json:"miss"`
}

func (h Handler) SearchOrderGoalByID(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err := h.orderController.SearchOrderGoalByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(order)
}


func (h Handler) SearchOrderGoalBySTT(w http.ResponseWriter, r *http.Request) {
	var input InputSearchSTT
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var order []order_goal.OrderProduct
	if input.Miss == 1 && input.Code == "" && input.STT == 0 {
		order, err = h.orderController.SearchOrderGoalByMiss(input.Miss)
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else if input.Miss == 1 && input.Code != "" && input.STT != 0{
		order, err = h.orderController.SearchOrderGoalByMissAndCode(input.STT, input.Code, input.Miss)
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else if input.Miss == 0 && input.Code != "" && input.STT != 0{
		order, err = h.orderController.SearchOrderGoalBySTT(input.STT, input.Code)
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else if input.Miss == 0 && input.Code == "" && input.STT == 0{ 
		order, err = h.orderController.GetOrderGoalAndProducts()
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
	// Send token as response
	json.NewEncoder(w).Encode(order)
}


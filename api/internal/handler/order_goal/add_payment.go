package order_goal

import (
	"Golang/internal/controller/order_goal"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/volatiletech/null/v8"
)

type UpdateInputAddPayment struct {
	ID              int64     `json:"id"`
	AmountPayment   null.Int  `json:"amount_payment"`
	DatePayment     null.Time `json:"date_payment"`
	NextDatePayment null.Time `json:"next_date_payment"`
}

func convertData(inp UpdateInputAddPayment) order_goal.UpdateInputAddPayment {
	return order_goal.UpdateInputAddPayment{
		ID:              inp.ID,
		AmountPayment:   inp.AmountPayment,
		DatePayment:     inp.DatePayment,
		NextDatePayment: inp.NextDatePayment,
	}
}

func (h Handler) UpdateOrderGoalAddPayment(w http.ResponseWriter, r *http.Request) {
	// Trích xuất orderID từ URL hoặc body request (tùy thuộc vào thiết kế API của bạn)
	var req UpdateInputAddPayment
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	layout := "2006-01-02 15:04:05.999 -0700 MST"
	t, err := time.Parse(layout, req.DatePayment.Time.String())
	month := t.Month()
	year := t.Year()

	_, err = h.orderController.UpdateSpendGoal(int(month), year, int64(req.AmountPayment.Int))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	updatedOrder, err := h.orderController.UpdateAddPaymentGoal(convertData(req))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Trả về thông tin order đã được cập nhật
	json.NewEncoder(w).Encode(updatedOrder)
}
func (h Handler) GetAddPaymentInfo(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}

	addPayment, err := h.orderController.GetAddPaymentInfo(int64(orderID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(addPayment)

}

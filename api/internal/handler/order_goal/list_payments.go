package order_goal

import (
	"github.com/go-chi/chi"
	"encoding/json"
	"net/http"
	"strconv"
)

func (h Handler) GetListPaymentsByOrderID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.orderController.GetListPaymentByOrderID(int64(orderID))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListPaymentsGoal(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetListPaymentsGoal()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetCountOrderGoalOver(w http.ResponseWriter, r *http.Request){
	count, err := h.orderController.GetCountOrderGoalOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(count)
}
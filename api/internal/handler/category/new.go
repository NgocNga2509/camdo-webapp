package category

import (
	"Golang/internal/controller/category"
)

type Handler struct {
	categoryController category.Controller
}

func New(categoryController category.Controller) Handler {
	return Handler{categoryController: categoryController}
}

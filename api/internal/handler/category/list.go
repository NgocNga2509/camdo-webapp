package category

import (
	"github.com/go-chi/chi"
	"encoding/json"
	"net/http"
	"strconv"
)

func (h Handler) GetListCategory(w http.ResponseWriter, r *http.Request) {
	res, err := h.categoryController.ListCategory()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListCategoryGoal(w http.ResponseWriter, r *http.Request) {
	res, err := h.categoryController.ListCategoryGoal()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListCategoryByID(w http.ResponseWriter, r *http.Request) {
	idIDStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.categoryController.ListCategoryByID(int(id))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListCategoryGoalByID(w http.ResponseWriter, r *http.Request) {
	idIDStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.categoryController.ListCategoryGoalByID(int(id))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) GetListCategoryPercent(w http.ResponseWriter, r *http.Request) {
	res, err := h.categoryController.ListCategoryPercent()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) GetListCategoryPercentByID(w http.ResponseWriter, r *http.Request) {
	idIDStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.categoryController.ListCategoryPercentByID(int(id))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
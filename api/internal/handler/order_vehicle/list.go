package ordervehicle

import (
	"github.com/go-chi/chi"
	"encoding/json"
	"net/http"
	"strconv"
)

func (h Handler) GetOrderVehicle(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderVehicle()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

// func (h Handler) GetOrderVehicleOver(w http.ResponseWriter, r *http.Request) {
// 	res, err := h.orderController.ListOrdersVehicleOver()
// 	if err != nil {
// 		// handle controller err
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}
// 	// Send token as response
// 	json.NewEncoder(w).Encode(res)
// }

func (h Handler) GetOrderVehicleOver60d(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.ListOrdersVehicleOver60d()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderVehicleDue(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.ListOrdersVehicleDue()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderVehicleUpComming(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.ListOrdersVehicleUpComming()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderVehicleByID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.orderController.GetOrderVehicleByID(int64(orderID))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderVehicleByCustomerID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "customerID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.orderController.GetOrderVehicleByCustomerID(int64(orderID))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetNewestOrderVehicle(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetNewestOrderVehicle()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

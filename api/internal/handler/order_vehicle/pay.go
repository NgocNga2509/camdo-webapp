package ordervehicle

import (
	ordervehicle "Golang/internal/controller/order_vehicle"
	models "Golang/internal/repository/dbmodel"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"github.com/volatiletech/null/v8"
)

type CreatePayOutput struct {
	PaymentID       int64      `boil:"payment_id" json:"payment_id" toml:"payment_id" yaml:"payment_id"`
	OrderID         null.Int64 `boil:"order_id" json:"order_id,omitempty" toml:"order_id" yaml:"order_id,omitempty"`
	PaymentDate     null.Time  `boil:"payment_date" json:"payment_date,omitempty" toml:"payment_date" yaml:"payment_date,omitempty"`
	Amount          null.Int   `boil:"amount" json:"amount,omitempty" toml:"amount" yaml:"amount,omitempty"`
	NextPaymentDate null.Time  `json:"next_payment_date"`
}
type CalculateInput struct {
	OrderID   null.Int64   `boil:"order_id" json:"order_id,omitempty" toml:"order_id" yaml:"order_id,omitempty"`
	EndDate   null.Time    `json:"end_date"`
	Percent   null.Float64 `json:"percent"`
	StartDate null.Time    `json:"start_date"`
}

func validateDataInputPay(inp CreatePayOutput) (ordervehicle.CreatePayInput, error) {
	return ordervehicle.CreatePayInput{OrderID: inp.OrderID,
		Amount:          inp.Amount,
		PaymentDate:     inp.PaymentDate,
		NextPaymentDate: inp.NextPaymentDate,
	}, nil
}

func (h Handler) CreatePayVehicle(w http.ResponseWriter, r *http.Request) {
	var req CreatePayOutput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input, err := validateDataInputPay(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.orderController.CreatePay(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)

}
func (h Handler) ListDayPaymentVehicleNewest(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "orderID")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	paymentDate, err := h.orderController.ListDayPaymentNewest(int64(id))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(paymentDate)
}
func (h Handler) CalculateInterest(w http.ResponseWriter, r *http.Request) {
	var req CalculateInput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	order, err := h.orderController.GetOrderVehicleByID(req.OrderID.Int64)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	interestAmount := h.orderController.CalculateInterest(order.Price.Int, req.Percent.Float64, req.StartDate.Time, req.EndDate.Time)
	// fmt.Fprintf(w, "Interest amount: %.2f\n", interestAmount)
	json.NewEncoder(w).Encode(interestAmount)
}

func (h Handler) ListOrderVehicleByPaymentOver(w http.ResponseWriter, r *http.Request) {

	paymentDate, overPayment, err := h.orderController.ListOrderVehicleByPaymentOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	response := struct {
		PaymentDate []*models.OrderVehicle         `json:"paymentDate"`
		OverPayment []*models.InterestPaymentsVehicle `json:"overPayment"`
	}{
		PaymentDate: paymentDate,
		OverPayment: overPayment,
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

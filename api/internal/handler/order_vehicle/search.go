package ordervehicle

import (
	"encoding/json"
	"net/http"
	"Golang/internal/controller/order_vehicle"

)
type InputSearchSTT struct {
	STT int64 `json:"stt"`
	Code string `json:"code"`
	Miss int `json:"miss"`
}
func (h Handler) SearchOrderVehicleByID(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	order, err := h.orderController.SearchOrderVehicleByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(order)
}

func (h Handler) SearchOrderVehicleBySTT(w http.ResponseWriter, r *http.Request) {
	var input InputSearchSTT
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var order []ordervehicle.OrderProduct
	if input.Miss == 1 && input.Code == "" && input.STT == 0 {
		order, err = h.orderController.SearchOrderVehicleByMiss(input.Miss)
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else if input.Miss == 1 && input.Code != "" && input.STT != 0{
		order, err = h.orderController.SearchOrderVehicleByMissAndCode(input.STT, input.Code, input.Miss)
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else if input.Miss == 0 && input.Code != "" && input.STT != 0{
		order, err = h.orderController.SearchOrderVehicleBySTT(input.STT, input.Code)
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}else if input.Miss == 0 && input.Code == "" && input.STT == 0{
		order, err = h.orderController.GetOrderVehicle()
		if err != nil {
			// handle controller err
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	// Send token as response
	json.NewEncoder(w).Encode(order)
}

package ordervehicle

import (
	"encoding/json"
	"net/http"
)

func (h Handler) GetOrdersVehicleToday(w http.ResponseWriter, r *http.Request) {

	order, err := h.orderController.ListOrderVehicleToday()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)
}

func (h Handler) GetListPaymentsVehicleToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.ListPaymentToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListRedeemVehicleToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.ListRedeemToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) GetListAddPaymentVehicleToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.ListAddPaymentToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOverviewVehicleToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.GetOverviewToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOverviewVehicleByMonth(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	res, err := h.orderController.GetOverviewByMonth(req.Month, req.Year)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

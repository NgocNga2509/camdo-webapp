package customer

import (
	"Golang/internal/controller/customer"
)

type Handler struct {
	customerController customer.Controller
}

func New(customerController customer.Controller) Handler {
	return Handler{customerController: customerController}
}

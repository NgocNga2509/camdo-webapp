package customer

import (
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

func (h Handler) DeleteCustomer(w http.ResponseWriter, r *http.Request){
	idStr := chi.URLParam(r,"id")
	id, err := strconv.Atoi(idStr)
	if err != nil{
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.customerController.DeleteCustomer(int64(id))
	if err != nil {
		http.Error(w, "Lỗi trong quá trình xóa khách hàng", http.StatusInternalServerError)
		return
	}

	
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Khách hàng đã được xóa thành công"))
}
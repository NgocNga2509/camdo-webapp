package customer

import (
	"Golang/internal/controller/customer"
	"encoding/json"
	"net/http"

	"github.com/volatiletech/null/v8"
)

type CreateCustomerInput struct {
	CustomerName string
	Address      null.String
	Gender       null.String
	CCCD         null.String
	BirthDate    null.Time
	Phone        null.String
	AddressCCCD null.String
	DayCCCD		null.Time
}

type CreateCustomerOutput struct {
	CustomerName string      `boil:"customer_name" json:"customer_name" toml:"customer_name" yaml:"customer_name"`
	CCCD         null.String `boil:"cccd" json:"cccd,omitempty" toml:"cccd" yaml:"cccd,omitempty"`
	Gender       null.String `boil:"gender" json:"gender,omitempty" toml:"gender" yaml:"gender,omitempty"`
	Address      null.String `boil:"address" json:"address,omitempty" toml:"address" yaml:"address,omitempty"`
	BirthDate    null.Time   `boil:"birth_date" json:"birth_date,omitempty" toml:"birth_date" yaml:"birth_date,omitempty"`
	Phone        null.String `boil:"phone" json:"phone,omitempty" toml:"phone" yaml:"phone,omitempty"`
	DayCCCD      null.Time   `boil:"day_cccd" json:"day_cccd,omitempty" toml:"day_cccd" yaml:"day_cccd,omitempty"`
	AddressCCCD  null.String `boil:"address_cccd" json:"address_cccd,omitempty" toml:"address_cccd" yaml:"address_cccd,omitempty"`
}

func validateDataInput(inp CreateCustomerOutput) (customer.CustomerInput, error) {
	var birthDate null.Time
    if inp.BirthDate.Valid {
        birthDate = inp.BirthDate
    }

    var dayCCCD null.Time
    if inp.DayCCCD.Valid {
        dayCCCD = inp.DayCCCD
    }
	return customer.CustomerInput{
		CustomerName: inp.CustomerName,
		CCCD: null.String(inp.CCCD),
		Gender: null.String(inp.Gender), 
		Address: inp.Address, 
		BirthDate: birthDate,
		Phone: inp.Phone,
		AddressCCCD: inp.AddressCCCD,
		DayCCCD: dayCCCD,
	}, nil
}

func (h Handler) CreateCustomer(w http.ResponseWriter, r *http.Request) {
	var req CreateCustomerOutput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input, err := validateDataInput(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.customerController.CreateCustomer(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

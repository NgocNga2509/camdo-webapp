package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) SearchOrderGoalByID(id int64) ([]models.OrderGoal, error) {
	order, err := models.OrderGoals(qm.Where("id = ?", id)).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderGoal, len(order))
	for ind, o := range order {
		result[ind] = models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			ProductName:       o.ProductName,
			PriceOld:          o.PriceOld,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
	}

	return result, nil
}
func (rep Repository) SearchOrderGoalByMissAndCode(stt int64,code string, miss int) ([]models.OrderGoal, error) {
	order, err := models.OrderGoals(qm.Where("stt = ?", stt), qm.And("code LIKE ?", code), qm.And("miss = ?", miss)).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderGoal, len(order))
	for ind, o := range order {
		result[ind] = models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			ProductName:       o.ProductName,
			PriceOld:          o.PriceOld,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
	}

	return result, nil
}

func (rep Repository) SearchOrderGoalBySTT(stt int64,code string) ([]models.OrderGoal, error) {
	order, err := models.OrderGoals(qm.Where("stt = ?", stt), qm.And("code LIKE ?", code)).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderGoal, len(order))
	for ind, o := range order {
		result[ind] = models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			ProductName:       o.ProductName,
			PriceOld:          o.PriceOld,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
	}

	return result, nil
}
func (rep Repository) SearchOrderGoalByMiss(miss int) ([]models.OrderGoal, error) {
	order, err := models.OrderGoals(qm.Where("miss = ?", miss)).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderGoal, len(order))
	for ind, o := range order {
		result[ind] = models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			ProductName:       o.ProductName,
			PriceOld:          o.PriceOld,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
	}

	return result, nil
}
package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) DeleteOrderGoal(ctx context.Context, id int64) error {
	_, err := models.OrderGoals(qm.Where("id=?", id)).DeleteAll(ctx, rep.db)
	if err !=nil{
		return err
	}
	return nil
}


package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) CreateRedeemGoal(m models.RedeemOrderGoal) (models.RedeemOrderGoal, error) {
	redeem := &models.RedeemOrderGoal{
		Amount:     m.Amount,
		DateRedeem: m.DateRedeem,
		OrderID:    m.OrderID,
	}

	if err := redeem.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.RedeemOrderGoal{}, err
	}

	return m, nil
}

func (rep Repository) ListRedeemGoal(orderID int64) (models.RedeemOrderGoal,error) {
	redeemGoal, err := models.RedeemOrderGoals(qm.Where("order_id=?", orderID)).One(context.Background(),rep.db)
	if err != nil{
		return models.RedeemOrderGoal{}, err
	}

	return *redeemGoal, nil
}
func (rep Repository) GetListRedeemGoal() ([]models.RedeemOrderGoal,error) {
	redeemGoal, err := models.RedeemOrderGoals().All(context.Background(),rep.db)
	if err != nil{
		return nil, err
	}
	var result = make([]models.RedeemOrderGoal, len(redeemGoal))
	for ind, inp := range redeemGoal {
		result[ind] = models.RedeemOrderGoal{Amount: inp.Amount, OrderID: inp.OrderID, DateRedeem: inp.DateRedeem, ID: inp.ID}
	}

	return result, nil
}

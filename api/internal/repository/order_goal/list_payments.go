package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) GetListPaymentByOrderID(id int64) ([]models.InterestPaymentsGoal, error) {
	payments, err := models.InterestPaymentsGoals(qm.Where("order_id=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsGoal, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsGoal{Amount: inp.Amount, PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate, NextPaymentDate: inp.NextPaymentDate}
	}

	return result, nil
}

func (rep Repository) GetListPaymentsGoal() ([]models.InterestPaymentsGoal, error) {
	payments, err := models.InterestPaymentsGoals().All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsGoal, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsGoal{NextPaymentDate: inp.NextPaymentDate, OrderID: inp.OrderID, Amount: inp.Amount, PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate}
	}

	return result, nil
}

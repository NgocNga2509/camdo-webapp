package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/null/v8"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

type OrderProduct struct {
	Order    OrderGoalOutput     `json:"order"`
	Products []ProductGoldOutput `json:"products"`
}
type OrderGoalOutput struct {
	ID                int64
	ProductName       null.String
	PriceOld          null.Int
	AddPayments       null.Int
	Price             null.Int
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
	Note              null.String
	Miss              null.Int
	Code              null.String
	STT               null.Int64
}

type ProductGoldOutput struct {
	ID               int64
	CategoryGoalID   null.Int64
	CategoryGoldName null.String
	WeightGoal       null.Float64
}
type ProductDetailOutput struct {
	ID           int64
	CategoryID   null.Int64
	CategoryName null.String
	Count        null.Int
}
type ProductsOutput struct {
	Product        ProductGoldOutput
	DetailProducts []ProductDetailOutput
}

func (rep Repository) GetOrderGoal() ([]OrderGoalOutput, error) {
	orders, err := models.OrderGoals(qm.OrderBy("status ASC, id DESC")).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]OrderGoalOutput, len(orders))
	for ind, inp := range orders {
		result[ind] = OrderGoalOutput{
			ID:                int64(inp.ID),
			Percent:           inp.Percent,
			PriceOld:          inp.PriceOld,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
			Code:              inp.Code,
			STT:               inp.STT,
		}
	}

	return result, nil
}

func (rep Repository) GetOrderGoalByID(id int64) (OrderGoalOutput, error) {
	order, err := models.OrderGoals(qm.Where("id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return OrderGoalOutput{}, err
	}
	result := OrderGoalOutput{
		ID:                order.ID,
		ProductName:       order.ProductName,
		AddPayments:       order.AddPayments,
		Price:             order.Price,
		PriceOld:          order.PriceOld,
		OrderDate:         order.OrderDate,
		CustomerID:        order.CustomerID,
		Percent:           order.Percent,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		InterestPaidCount: order.InterestPaidCount,
		Note:              order.Note,
		Miss:              order.Miss,
		Code:              order.Code,
		STT:               order.STT,
	}
	return result, nil
}

func (rep Repository) GetOrderPercentByID(id int64) (models.OrderGoal, error) {
	order, err := models.OrderGoals(qm.Where("id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return models.OrderGoal{}, err
	}
	return *order, nil
}

func (rep Repository) GetOrderGoalByCustomerID(id int64) ([]models.OrderGoal, error) {
	order, err := models.OrderGoals(qm.Where("customer_id=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.OrderGoal, len(order))
	for ind, m := range order {
		result[ind] = models.OrderGoal{
			ID:                m.ID,
			ProductName:       m.ProductName,
			AddPayments:       m.AddPayments,
			PriceOld:          m.PriceOld,
			Price:             m.Price,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			ReturnDate:        m.ReturnDate,
			Note:              m.Note,
			Code:              m.Code,
			STT:               m.STT,
			Miss:              m.Miss,
			Status:            m.Status,
			InterestPaidCount: m.InterestPaidCount}
	}
	return result, nil

}

func (rep Repository) GetNewestOrderGoal() (OrderGoalOutput, error) {
	order, err := models.OrderGoals(qm.OrderBy("created_at DESC"), qm.Limit(1)).One(context.Background(), rep.db)
	if err != nil {
		return OrderGoalOutput{}, err
	}
	result := OrderGoalOutput{
		ID:                order.ID,
		ProductName:       order.ProductName,
		AddPayments:       order.AddPayments,
		PriceOld:          order.PriceOld,
		Price:             order.Price,
		OrderDate:         order.OrderDate,
		CustomerID:        order.CustomerID,
		Percent:           order.Percent,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		InterestPaidCount: order.InterestPaidCount,
		Note:              order.Note,
		Miss:              order.Miss,
		Code:              order.Code,
		STT:               order.STT,
	}
	return result, nil
}

func (rep Repository) ListOrdersGoalOver() ([]models.OrderGoal, error) {
	overdueOrders, err := models.OrderGoals(
		qm.Where("return_date < ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.And("interest_paid_count = ?", 0),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderGoal
	for _, o := range overdueOrders {
		order := models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
			Code:              o.Code,
			STT:               o.STT,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) ListOrdersDueGoal() ([]models.OrderGoal, error) {
	overdueOrders, err := models.OrderGoals(
		qm.Where("return_date = ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderGoal
	for _, o := range overdueOrders {
		order := models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			PriceOld:          o.PriceOld,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			Note:              o.Note,
			Miss:              o.Miss,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) ListOrdersUpcommingGoal() ([]models.OrderGoal, error) {
	dueDate := time.Now().AddDate(0, 0, 3)

	upcomingDueOrders, err := models.OrderGoals(
		qm.Where("return_date > ?", time.Now().Format(time.RFC3339)),
		qm.And("return_date <= ?", dueDate.Format(time.RFC3339)),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderGoal
	for _, o := range upcomingDueOrders {
		order := models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Miss:              o.Miss,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) ListOrdersGoalOver60Days() ([]models.OrderGoal, error) {
	overdueOrders, err := models.OrderGoals(
		qm.Where("order_date < ?", time.Now().AddDate(0, 0, -60)),
		qm.And("status = ?", "0"),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderGoal
	for _, o := range overdueOrders {
		order := models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) GetListProductByIDOrder(id int64) ([]ProductGoldOutput, error) {
	order, err := models.ProductGolds(qm.Where("id_goal=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var products []ProductGoldOutput
	for _, o := range order {
		product := ProductGoldOutput{
			ID:             o.ID,
			CategoryGoalID: o.CategoryGoalID,
			WeightGoal:     o.WeightGoal,
		}
		categoryGold, err := models.CategoryGoals(qm.Where("category_id=?", o.CategoryGoalID)).One(context.Background(), rep.db)
		if err != nil {
			return nil, err
		}
		// Lấy tên category từ bảng Category
		product.CategoryGoldName = categoryGold.CategoryName
		// product.CategoryName = category.CategoryName

		products = append(products, product)
	}

	return products, nil
}
func (rep Repository) GetListProductDetail(id int64) ([]ProductDetailOutput, error) {
	productsDetail, err := models.DetailProductGolds(qm.Where("detail_product_id=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var details []ProductDetailOutput
	for _, d := range productsDetail {
		detail := ProductDetailOutput{
			ID:         d.ID,
			CategoryID: d.CategoryID,
			Count:      d.Count,
		}

		category, err := models.Categories(qm.Where("category_id=?", d.CategoryID)).One(context.Background(), rep.db)
		if err != nil {
			return nil, err
		}

		detail.CategoryName = category.CategoryName
		details = append(details, detail)
	}

	return details, nil
}

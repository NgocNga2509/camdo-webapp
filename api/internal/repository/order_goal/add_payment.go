package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

type UpdateInputAddPayment struct {
	ID              int64
	AmountPayment   null.Int
	DatePayment     null.Time
	NextDatePayment null.Time
}
type CreateInputAddPayment struct {
	ID              int64
	IDGold          int64
	AmountPayment   null.Int
	DateAddPayment  null.Time
	NextDatePayment null.Time
}

func (rep Repository) UpdatePaymentGoal(inp UpdateInputAddPayment) (models.OrderGoal, error) {
	order, err := models.FindOrderGoal(context.Background(), rep.db, inp.ID)
	if err != nil {
		return models.OrderGoal{}, err
	}

	addPayment := &models.AddPaymentsGoal{
		IDGold:         null.Int64From(order.ID),
		DateAddPayment: inp.DatePayment,
		Amount:         null.IntFrom(inp.AmountPayment.Int),
	}
	if err = addPayment.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.OrderGoal{}, err
	}
	// Cập nhật thông tin order
	if inp.AmountPayment.Valid {
		order.Price = null.IntFrom(order.Price.Int + inp.AmountPayment.Int)
	}
	
	order.AddPayments = null.IntFrom(1)
	order.ReturnDate = inp.NextDatePayment
	// Lưu các thay đổi vào cơ sở dữ liệu
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderGoal{}, err
	}

	return *order, nil
}
func (rep Repository) UpdateSpendGoal(month, year int, amount int64) (models.SpendGoal, error) {
	order, err := models.SpendGoals(qm.Where("month = ?", month), qm.Where("year = ?", year)).One(context.Background(), rep.db)
	if err != nil {
		// No spend goal found, create a new one
		spendGoal := &models.SpendGoal{
			Month:  null.IntFrom(month),
			Year:   null.IntFrom(year),
			Amount: null.Int64From(amount),
		}
		err = spendGoal.Insert(context.Background(), rep.db, boil.Infer())
		if err != nil {
			return models.SpendGoal{}, err
		}
		return *spendGoal, nil
	}

	// Spend goal found, update the amount by adding the new amount
	order.Amount = null.Int64From(order.Amount.Int64 + amount)
	_, err = order.Update(context.Background(), rep.db, boil.Whitelist("amount"))
	if err != nil {
		return models.SpendGoal{}, err
	}

	return *order, nil

}

func (rep Repository) GetAddPaymentInfo(id int64) ([]models.AddPaymentsGoal, error) {
	addPayment, err := models.AddPaymentsGoals(qm.Where("id_gold=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil,err
	}
	var result = make([]models.AddPaymentsGoal, len(addPayment))
	for ind, m := range addPayment {
		result[ind] = models.AddPaymentsGoal{
			ID:             m.ID,
			IDGold:         m.IDGold,
			Amount:         m.Amount,
			DateAddPayment: m.DateAddPayment,
		}
	}

	return result, nil
}

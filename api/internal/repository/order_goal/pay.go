package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) CreatePayGoal(m models.InterestPaymentsGoal) (models.InterestPaymentsGoal, error) {
	pay := &models.InterestPaymentsGoal{
		OrderID:         m.OrderID,
		Amount:          m.Amount,
		PaymentDate:     m.PaymentDate,
		NextPaymentDate: m.NextPaymentDate,
	}
	if err := pay.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.InterestPaymentsGoal{}, err
	}
	return m, nil
}

func (rep Repository) ListDayPaymentNewest(id int64) (time.Time, error) {
	payment, err := models.InterestPaymentsGoals(
		qm.Where("order_id = ?", id),
		qm.OrderBy("created_at DESC"),
		qm.Limit(1)).One(context.Background(), rep.db)
	if err != nil {
		return time.Time{}, err
	}
	return payment.PaymentDate.Time, nil
}

func (rep Repository) ListOrderGoalByPaymentOver() ([]*models.OrderGoal, []*models.InterestPaymentsGoal, error) {
	currentTime := time.Now()

	// Lấy danh sách các đơn hàng có lần thanh toán gần nhất trễ hạn
	orders, err := models.OrderGoals(
		qm.InnerJoin("interest_payments_goal ON interest_payments_goal.order_id = order_goal.id"),
		qm.Where("interest_payments_goal.next_payment_date < ?", currentTime),
		qm.And("status = ?", "0"),
		qm.GroupBy("order_goal.id"),
		qm.Load(models.OrderGoalRels.OrderInterestPaymentsGoals, qm.OrderBy("interest_payments_goal.next_payment_date DESC")),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, nil, err
	}

	// Lọc danh sách các đơn hàng chỉ lấy đơn hàng có lần thanh toán gần nhất trễ hạn
	overdueOrders := make([]*models.OrderGoal, 0)
	overduePayments := make([]*models.InterestPaymentsGoal, 0)
	for _, order := range orders {
		if order.R.OrderInterestPaymentsGoals != nil && len(order.R.OrderInterestPaymentsGoals) > 0 {
			latestPayment := order.R.OrderInterestPaymentsGoals[0]
			if latestPayment.NextPaymentDate.Valid && latestPayment.NextPaymentDate.Time.Before(currentTime) {
				overdueOrders = append(overdueOrders, order)
				overduePayments = append(overduePayments, latestPayment)
			}
		}
	}

	return overdueOrders, overduePayments, nil
}

func (rep Repository) CalculateInterest(amount int, interest float64, startDate, endDate time.Time) float64 {
	duration := endDate.Sub(startDate)
	days := int(duration.Hours() / 24)
	interestPerDay := (float64(amount) * (interest / 100)) / 30
	interestAmount := interestPerDay * float64(days)
	return interestAmount
}

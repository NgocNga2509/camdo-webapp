package account

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

type Repository struct {
	db *sql.DB
}

func New(db *sql.DB)  Repository{
	return Repository{db:db}
}

func (rep Repository) GetByUserName(username string) (models.Account, error) {
	user, err := models.Accounts(qm.Where("username=?", username)).One(context.Background(), rep.db)
	if err != nil {
		return models.Account{}, err
	}

	return *user, nil
}
func (rep Repository) GetByPassword(password string) (models.Account, error) {
	user, err := models.Accounts(qm.Where("password=?", password)).One(context.Background(), rep.db)
	if err != nil {
		return models.Account{}, err
	}

	return *user, nil
}


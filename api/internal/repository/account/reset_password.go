package account

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (rep Repository) UpdateOTP(otp string) (models.Account, error) {
	account, err := models.FindAccount(context.Background(), rep.db, "1")
	if err != nil {
		return models.Account{}, err
	}

	account.Avatar = otp
	_, err = account.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.Account{}, err
	}

	return *account, nil
}

func (rep Repository) GetOTP(otp string) (string, error) {
	account, err := models.FindAccount(context.Background(), rep.db, "1")
	if err != nil {
		return "Lỗi khi tìm tài khoản", err
	}

	return account.Avatar, nil
}

func (rep Repository) UpdateOTPAfterResetPassword() (models.Account, error) {
	account, err := models.FindAccount(context.Background(), rep.db, "1")
	if err != nil {
		return models.Account{}, err
	}

	account.Avatar = "null"
	_, err = account.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.Account{}, err
	}

	return *account, nil
}

package account

import (
	models "Golang/internal/repository/dbmodel"
	"Golang/pkg/db"
	// "Golang/vendor/github.com/volatiletech/sqlboiler/v4/boil"
	"time"

	"github.com/dgrijalva/jwt-go"

	//"Golang/vendor/github.com/volatiletech/sqlboiler/v4/queries/qm"
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

// Request body for sign in endpoint
type SignInRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Response body for sign in endpoint
type SignInResponse struct {
	Token string `json:"token"`
}

// JWT secret key
const secretKey = "secret"

func SignIn(w http.ResponseWriter, r *http.Request) {
	// Parse request body
	var req SignInRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Find the user by username
	user, err := models.Accounts(qm.Where("username=?", req.Username)).One(r.Context(), db.Database.Db)
	if err != nil {
		if err == sql.ErrNoRows {
			http.Error(w, "Invalid username", http.StatusUnauthorized)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}else{
		if user.Password != req.Password {
			if err == sql.ErrNoRows {
				http.Error(w, "Invalid password", http.StatusUnauthorized)
			} else {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
	}

	// // Check password
	// if !types.PByte(user.Password).EqualsString(req.Password) {
	// 	http.Error(w, "Invalid username or password", http.StatusUnauthorized)
	// 	return
	// }

	// Create JWT token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
		"exp":      time.Now().Add(time.Hour * 24).Unix(),
	})
	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Send token as response
	resp := SignInResponse{Token: tokenString}
	json.NewEncoder(w).Encode(resp)

}

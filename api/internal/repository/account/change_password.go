package account

import (
	"context"
    "golang.org/x/crypto/bcrypt"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

type InputUpdatePass struct {
	Username    string
	Password    string
	NewPassword string
}

func (rep Repository) ChangePassword(input InputUpdatePass) error {
	user, err := rep.GetByUserName(input.Username)
	if err != nil {
		return err
	}
	hashedNewPassword, err := bcrypt.GenerateFromPassword([]byte(input.NewPassword), bcrypt.DefaultCost)
    if err != nil {
        return err
    }
	user.Password = string(hashedNewPassword)
	_, err = user.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return err
	}

	return nil
}

package ordervehicle

import (
	"github.com/volatiletech/sqlboiler/v4/boil"
)

type Repository struct {
	db boil.ContextExecutor
}

func New(db boil.ContextExecutor) Repository {
	return Repository{db: db}
}

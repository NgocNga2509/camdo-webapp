package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) CreateRedeemVehicle(m models.RedeemOrderVehicle) (models.RedeemOrderVehicle, error) {
	redeem := &models.RedeemOrderVehicle{
		Amount:     m.Amount,
		DateRedeem: m.DateRedeem,
		OrderID:    m.OrderID,
	}

	if err := redeem.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.RedeemOrderVehicle{}, err
	}

	return m, nil
}

func (rep Repository) ListRedeemVehicle(orderID int64) (models.RedeemOrderVehicle, error) {
	redeemVehicle, err := models.RedeemOrderVehicles(qm.Where("order_id=?", orderID)).One(context.Background(), rep.db)
	if err != nil {
		return models.RedeemOrderVehicle{}, err
	}

	return *redeemVehicle, nil
}


package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/null/v8"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

type OrderVehicleOutput struct {
	ID                int64
	ProductName       null.String
	AddPayments       null.Int
	Price             null.Int
	PriceOld          null.Int
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	LicensePlate      null.String
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
	Note              null.String
	Miss              null.Int
	Code              null.String
	STT               null.Int64
}

func (rep Repository) GetOrderVehicle() ([]OrderVehicleOutput, error) {
	orders, err := models.OrderVehicles(qm.OrderBy("status ASC, id DESC")).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	var result = make([]OrderVehicleOutput, len(orders))
	for ind, inp := range orders {
		result[ind] = OrderVehicleOutput{ID: int64(inp.ID),
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			ReturnDate:        null.Time(inp.ReturnDate),
			CustomerID:        inp.CustomerID,
			Status:            inp.Status,
			LicensePlate:      inp.LicensePlate,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
			Code:              inp.Code,
			STT:               inp.STT,
		}
	}

	return result, nil
}
func (rep Repository) GetOrderVehicleByID(id int64) (models.OrderVehicle, error) {
	order, err := models.OrderVehicles(qm.Where("id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return models.OrderVehicle{}, err
	}
	return *order, nil
}

func (rep Repository) GetOrderVehicleByCustomerID(id int64) ([]models.OrderVehicle, error) {
	order, err := models.OrderVehicles(qm.Where("customer_id=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.OrderVehicle, len(order))
	for ind, m := range order {
		result[ind] = models.OrderVehicle{
			ID:                m.ID,
			ProductName:       m.ProductName,
			AddPayments:       m.AddPayments,
			Price:             m.Price,
			PriceOld:          m.PriceOld,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			ReturnDate:        m.ReturnDate,
			Status:            m.Status,
			Code:              m.Code,
			STT:               m.STT,
			LicensePlate:      m.LicensePlate,
			InterestPaidCount: m.InterestPaidCount}
	}
	return result, nil
}

func (rep Repository) GetNewestOrderVehicle() (models.OrderVehicle, error) {
	order, err := models.OrderVehicles(qm.OrderBy("created_at DESC"), qm.Limit(1)).One(context.Background(), rep.db)
	if err != nil {
		return models.OrderVehicle{}, nil
	}

	return *order, nil
}

func (rep Repository) ListOrdersVehicleOver60d() ([]models.OrderVehicle, error) {
	overdueOrders, err := models.OrderVehicles(
		qm.Where("order_date < ?", time.Now().AddDate(0, 0, -60)),
		qm.And("status = ?", "0"),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderVehicle
	for _, o := range overdueOrders {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			LicensePlate:      o.LicensePlate,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			Code:              o.Code,
			STT:               o.STT,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) ListOrdersVehicleUpComming() ([]models.OrderVehicle, error) {
	dueDate := time.Now().AddDate(0, 0, 3)

	upcomingDueOrders, err := models.OrderVehicles(
		qm.Where("return_date > ?", time.Now().Format(time.RFC3339)),
		qm.And("return_date <= ?", dueDate.Format(time.RFC3339)),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderVehicle
	for _, o := range upcomingDueOrders {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			LicensePlate:      o.LicensePlate,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Code:              o.Code,
			STT:               o.STT,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) ListOrdersVehicleDue() ([]models.OrderVehicle, error) {
	overdueOrders, err := models.OrderVehicles(
		qm.Where("return_date = ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderVehicle
	for _, o := range overdueOrders {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			LicensePlate:      o.LicensePlate,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			Code:              o.Code,
			STT:               o.STT,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

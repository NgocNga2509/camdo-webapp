package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (rep *Repository) EditOrder(orderID int64, update models.OrderVehicle) (models.OrderVehicle, error) {
	order, err := models.FindOrderVehicle(context.Background(), rep.db, orderID)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	// Cập nhật thông tin order
	order.ProductName = update.ProductName
	order.AddPayments = update.AddPayments
	order.Price = update.Price
	order.LicensePlate = update.LicensePlate
	order.OrderDate = update.OrderDate
	order.CustomerID = update.CustomerID
	order.Percent = update.Percent
	order.ReturnDate = update.ReturnDate
	order.Status = update.Status
	//order.InterestPaidCount = update.InterestPaidCount

	// Lưu các thay đổi vào cơ sở dữ liệu
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return *order, nil
}

func (rep Repository) UpdateNote(note null.String,miss null.Int, orderID int64) (models.OrderVehicle, error) {
	order, err := models.FindOrderVehicle(context.Background(), rep.db, orderID)
	if err != nil {
		return models.OrderVehicle{}, err
	}
	order.Note = note
	order.Miss = miss
	// Lưu các thay đổi vào cơ sở dữ liệu
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return *order, nil
}

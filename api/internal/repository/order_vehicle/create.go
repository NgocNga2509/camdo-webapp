package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"github.com/volatiletech/null/v8"
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (rep Repository) CreateOrderVehicle(m models.OrderVehicle) (models.OrderVehicle, error) {

	orderDate := m.OrderDate.Time

	// Format the OrderDate as "01" for month and "06" for year
	mm := orderDate.Format("01")
	yy := orderDate.Format("06")

	code := mm + yy

	var maxSTT sql.NullInt64

	// Truy vấn cơ sở dữ liệu để lấy giá trị MAX(STT)
	query := `
		SELECT MAX(stt)
		FROM order_vehicle
		WHERE code LIKE $1
	`

	err := rep.db.QueryRow(query, code+"%").Scan(&maxSTT)

	if err != nil {
		return models.OrderVehicle{}, err
	}

	var newSTT int64

	if maxSTT.Valid {
		newSTT = maxSTT.Int64 + 1
	} else {
		newSTT = 1
	}

	order := &models.OrderVehicle{
		ProductName:       m.ProductName,
		AddPayments:       m.AddPayments,
		Price:             m.Price,
		PriceOld:          m.Price,
		LicensePlate:      m.LicensePlate,
		OrderDate:         m.OrderDate,
		CustomerID:        m.CustomerID,
		Percent:           m.Percent,
		ReturnDate:        m.ReturnDate,
		Note:              m.Note,
		Status:            m.Status,
		InterestPaidCount: m.InterestPaidCount,
		Code:              null.StringFrom(code),
		STT:               null.Int64From(newSTT),
	}
	if err := order.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.OrderVehicle{}, err
	}

	return m, nil
}

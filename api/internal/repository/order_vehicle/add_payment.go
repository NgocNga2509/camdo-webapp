package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

type UpdateInputAddPayment struct {
	ID              int64
	AmountPayment   null.Int
	DatePayment     null.Time
	NextDatePayment null.Time
}

func (rep Repository) UpdatePaymentVehicle(inp UpdateInputAddPayment) (models.OrderVehicle, error) {
	order, err := models.FindOrderVehicle(context.Background(), rep.db, inp.ID)
	if err != nil {
		return models.OrderVehicle{}, err
	}
	addPayment := &models.AddPaymentsVehicle{
		IDVehicle:      null.Int64From(order.ID),
		DateAddPayment: inp.DatePayment,
		Amount:         null.IntFrom(inp.AmountPayment.Int),
	}
	if err = addPayment.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.OrderVehicle{}, err
	}
	if inp.AmountPayment.Valid {
		order.Price = null.IntFrom(order.Price.Int + inp.AmountPayment.Int)
	}
	//order.OrderDate = inp.DatePayment
	order.AddPayments = null.IntFrom(1)
	order.ReturnDate = inp.NextDatePayment
	// Lưu các thay đổi vào cơ sở dữ liệu
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return *order, nil
}

func (rep Repository) UpdateSpendVehicle(month, year int, amount int64) (models.SpendVehicle, error) {
	order, err := models.SpendVehicles(qm.Where("month = ?", month), qm.Where("year = ?", year)).One(context.Background(), rep.db)
	if err != nil {
		// No spend goal found, create a new one
		spendVehicle := &models.SpendVehicle{
			Month:  null.IntFrom(month),
			Year:   null.IntFrom(year),
			Amount: null.Int64From(amount),
		}
		err = spendVehicle.Insert(context.Background(), rep.db, boil.Infer())
		if err != nil {
			return models.SpendVehicle{}, err
		}
		return *spendVehicle, nil
	}

	// Spend goal found, update the amount by adding the new amount
	order.Amount = null.Int64From(order.Amount.Int64 + amount)
	_, err = order.Update(context.Background(), rep.db, boil.Whitelist("amount"))
	if err != nil {
		return models.SpendVehicle{}, err
	}

	return *order, nil

}

func (rep Repository) GetAddPaymentInfo(id int64) ([]models.AddPaymentsVehicle, error) {
	addPayment, err := models.AddPaymentsVehicles(qm.Where("id_vehicle=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.AddPaymentsVehicle, len(addPayment))
	for ind, m := range addPayment {
		result[ind] = models.AddPaymentsVehicle{
			ID:             m.ID,
			IDVehicle:      m.IDVehicle,
			Amount:         m.Amount,
			DateAddPayment: m.DateAddPayment,
		}
	}

	return result, nil
}

package customer

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	"github.com/volatiletech/null/v8"
)

type CustomerOutput struct {
	CustomerID   int64
	CustomerName string
	Address      null.String
	Gender       null.String
	CCCD         null.String
	BirthDate    null.Time
	Phone        null.String
	AddressCCCD  null.String
	DayCCCD      null.Time
}

func (rep Repository) GetListCustomer() ([]CustomerOutput, error) {
	users, err := models.Customers().All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	var result = make([]CustomerOutput, len(users))
	for ind, customer := range users {
		result[ind] = CustomerOutput{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

func (rep Repository) GetNewestCustomer() (models.Customer, error) {
	user, err := models.Customers(qm.OrderBy("created_at DESC"), qm.Limit(1)).One(context.Background(), rep.db)
	if err != nil {
		return models.Customer{}, err
	}

	return *user, nil
}

func (rep Repository) GetCustomerByID(id int64) (models.Customer, error) {
	user, err := models.Customers(qm.Where("customer_id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return models.Customer{}, err
	}

	return *user, nil
}

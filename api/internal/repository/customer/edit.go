package customer

import (
	models "Golang/internal/repository/dbmodel"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"context"
)

func (rep Repository) EditCustomer(customerID int64, m models.Customer) (models.Customer,error){
	customer, err := models.FindCustomer(context.Background(),rep.db, customerID)
	if err != nil{
		return models.Customer{}, err
	}

	customer.CustomerName = m.CustomerName
	customer.Address = m.Address
	customer.AddressCCCD = m.AddressCCCD
	customer.BirthDate = m.BirthDate
	customer.DayCCCD = m.DayCCCD
	customer.Phone = m.Phone
	customer.Gender = m.Gender
	customer.CCCD = m.CCCD

	_, err = customer.Update(context.Background(),rep.db, boil.Infer())
	if err != nil{
		return models.Customer{}, err
	}

	return *customer,nil
}
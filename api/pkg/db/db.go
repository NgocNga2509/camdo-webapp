package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)
type DbInstance struct {
	Db *sql.DB
}

var Database DbInstance

func Connect() (*sql.DB, error){
	// `database` host is container which already defined in docker compose
	db, err := sql.Open("postgres", "host=maynghien.ddns.net user=postgres password=pG7%0N6@ dbname=camdo_khaihoan port=5432 sslmode=disable")
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("Database Done !")
	Database = DbInstance{Db: db}

	return db,nil
}

-- Trigger cho bảng "interest_payments_vehicle"
CREATE OR REPLACE FUNCTION delete_revenue_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_vehicle" chưa
    IF EXISTS (SELECT * FROM revenue_vehicle WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_vehicle
        SET amount = amount - paymentAmount
        WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_revenue_vehicle
AFTER DELETE ON interest_payments_vehicle
FOR EACH ROW
EXECUTE FUNCTION delete_revenue_vehicle_function();

-- Trigger cho bảng "interest_payments_vehicle"
CREATE OR REPLACE FUNCTION delete_collect_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount - paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_collect_vehicle
AFTER DELETE ON interest_payments_vehicle
FOR EACH ROW
EXECUTE FUNCTION delete_collect_vehicle_function();

-- Trigger cho bảng "redeem_order_vehicle"
CREATE OR REPLACE FUNCTION delete_collect_redeem_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ lệnh rút bị xóa
    month_val := EXTRACT(MONTH FROM OLD.date_redeem);
    year_val := EXTRACT(YEAR FROM OLD.date_redeem);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount - paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_collect_redeem_vehicle
AFTER DELETE ON redeem_order_vehicle
FOR EACH ROW
EXECUTE FUNCTION delete_collect_redeem_vehicle_function();

-- Trigger cho bảng "order_vehicle"
CREATE OR REPLACE FUNCTION delete_spend_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ lệnh đặt bị xóa
    month_val := EXTRACT(MONTH FROM OLD.order_date);
    year_val := EXTRACT(YEAR FROM OLD.order_date);
    paymentAmount := OLD.price_old;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_vehicle" chưa
    IF EXISTS (SELECT * FROM spend_vehicle WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_vehicle
        SET amount = amount - paymentAmount
        WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_spend_vehicle
AFTER DELETE ON order_vehicle
FOR EACH ROW
EXECUTE FUNCTION delete_spend_vehicle_function();

-- Xóa trigger cho bảng "interest_payments_goal"
DROP TRIGGER IF EXISTS delete_revenue_vehicle_amount ON interest_payments_vehicle;

-- Xóa trigger cho bảng "interest_payments_vehicle"
DROP TRIGGER IF EXISTS delete_collect_vehicle_amount ON interest_payments_vehicle;

-- Xóa trigger cho bảng "redeem_order_vehicle"
DROP TRIGGER IF EXISTS delete_collect_redeem_vehicle_amount ON redeem_order_vehicle;

-- Xóa trigger cho bảng "order_vehicle"
DROP TRIGGER IF EXISTS delete_spend_vehicle_amount ON order_vehicle;

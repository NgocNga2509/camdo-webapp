CREATE TABLE IF  NOT EXISTS public.customers (
  customer_id BIGSERIAL PRIMARY KEY,
  customer_name VARCHAR(255) NOT NULL CONSTRAINT customer_name_check CHECK (customer_name <> ''::varchar),
  cccd VARCHAR(100),
  day_cccd DATE,
  address_cccd VARCHAR(255),
  gender VARCHAR(10),
  address VARCHAR(255),
  birth_date DATE,
  phone VARCHAR(20),
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);


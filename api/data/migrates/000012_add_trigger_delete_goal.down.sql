-- Xóa trigger cho bảng "interest_payments_goal"
DROP TRIGGER IF EXISTS delete_revenue_goal_amount ON interest_payments_goal;

-- Xóa trigger cho bảng "interest_payments_goal"
DROP TRIGGER IF EXISTS delete_collect_goal_amount ON interest_payments_goal;

-- Xóa trigger cho bảng "redeem_order_goal"
DROP TRIGGER IF EXISTS delete_collect_redeem_goal_amount ON redeem_order_goal;

-- Xóa trigger cho bảng "order_goal"
DROP TRIGGER IF EXISTS delete_spend_goal_amount ON order_goal;

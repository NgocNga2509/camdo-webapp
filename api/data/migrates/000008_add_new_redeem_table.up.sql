CREATE TABLE public.redeem_order_goal (
    id  BIGSERIAL PRIMARY KEY,
    amount INT,
    date_redeem DATE,
    order_id BIGINT REFERENCES order_goal(id) ON DELETE CASCADE,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE public.redeem_order_vehicle (
    id  BIGSERIAL PRIMARY KEY,
    amount INT,
    date_redeem DATE,
    order_id     BIGINT REFERENCES order_vehicle(id) ON DELETE CASCADE,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

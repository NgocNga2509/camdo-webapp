-- Trigger cho bảng interest_payments_goal
CREATE OR REPLACE FUNCTION increase_revenue_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_goal" chưa
    IF EXISTS (SELECT * FROM revenue_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO revenue_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_revenue_goal
AFTER INSERT ON interest_payments_goal
FOR EACH ROW
EXECUTE FUNCTION increase_revenue_goal_function();

CREATE OR REPLACE FUNCTION increase_collect_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_collect_goal
AFTER INSERT ON interest_payments_goal
FOR EACH ROW
EXECUTE FUNCTION increase_collect_goal_function();

CREATE OR REPLACE FUNCTION increase_collect_redeem_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.date_redeem);
    year_val := EXTRACT(YEAR FROM NEW.date_redeem);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_collect_redeem_goal
AFTER INSERT ON redeem_order_goal
FOR EACH ROW
EXECUTE FUNCTION increase_collect_redeem_goal_function();

CREATE OR REPLACE FUNCTION increase_spend_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.order_date);
    year_val := EXTRACT(YEAR FROM NEW.order_date);
    paymentAmount := NEW.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_goal" chưa
    IF EXISTS (SELECT * FROM spend_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO spend_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_spend_goal
AFTER INSERT ON order_goal
FOR EACH ROW
EXECUTE FUNCTION increase_spend_goal_function();

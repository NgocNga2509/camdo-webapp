ALTER TABLE public.order_goal
ADD COLUMN stt bigint,
ADD COLUMN code VARCHAR;

ALTER TABLE public.order_vehicle
ADD COLUMN stt bigint,
ADD COLUMN code VARCHAR;
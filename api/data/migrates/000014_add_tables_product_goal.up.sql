CREATE TABLE IF NOT EXISTS public.product_gold (
    id BIGSERIAL PRIMARY KEY,
    id_goal BIGINT REFERENCES order_goal(id) ON DELETE CASCADE,
    weight_goal FLOAT,
    category_goal_id BIGINT REFERENCES category_goal(category_id) ,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.detail_product_gold (
    id BIGSERIAL PRIMARY KEY,
    detail_product_id BIGINT REFERENCES product_gold(id) ON DELETE CASCADE,
    category_id BIGINT REFERENCES category(category_id) ,
    count INT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.add_payments_goal (
    id BIGSERIAL PRIMARY KEY,
    id_gold BIGINT REFERENCES order_goal(id) ON DELETE CASCADE,
    amount INT,
    date_add_payment DATE,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.add_payments_vehicle (
    id BIGSERIAL PRIMARY KEY,
    id_vehicle BIGINT REFERENCES order_vehicle(id) ON DELETE CASCADE,
    amount INT,
    date_add_payment DATE,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
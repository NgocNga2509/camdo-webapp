-- Trigger cho bảng "vehicle"
CREATE OR REPLACE FUNCTION increase_revenue_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_vehicle" chưa
    IF EXISTS (SELECT * FROM revenue_vehicle WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_vehicle
        SET amount = amount + paymentAmount
        WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO revenue_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_revenue_vehicle
AFTER INSERT ON interest_payments_vehicle
FOR EACH ROW
EXECUTE FUNCTION increase_revenue_vehicle_function();

CREATE OR REPLACE FUNCTION increase_collect_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount + paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_collect_vehicle
AFTER INSERT ON interest_payments_vehicle
FOR EACH ROW
EXECUTE FUNCTION increase_collect_vehicle_function();

CREATE OR REPLACE FUNCTION increase_collect_redeem_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.date_redeem);
    year_val := EXTRACT(YEAR FROM NEW.date_redeem);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount + paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_collect_redeem_vehicle
AFTER INSERT ON redeem_order_vehicle
FOR EACH ROW
EXECUTE FUNCTION increase_collect_redeem_vehicle_function();

CREATE OR REPLACE FUNCTION increase_spend_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.order_date);
    year_val := EXTRACT(YEAR FROM NEW.order_date);
    paymentAmount := NEW.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_vehicle" chưa
    IF EXISTS (SELECT * FROM spend_vehicle WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_vehicle
        SET amount = amount + paymentAmount
        WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO spend_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increase_spend_vehicle
AFTER INSERT ON order_vehicle
FOR EACH ROW
EXECUTE FUNCTION increase_spend_vehicle_function();

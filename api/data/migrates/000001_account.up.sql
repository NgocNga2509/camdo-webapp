CREATE TABLE IF NOT EXISTS public.account
(
    id          varchar PRIMARY KEY,
    name        varchar                  NOT NULL CONSTRAINT products_name_check CHECK (name <> ''::varchar),
    username    varchar                  NOT NULL CONSTRAINT products_username_check CHECK (username <> ''::varchar),
    gender      INT                      NOT NULL,
    avatar      varchar                  NOT NULL CONSTRAINT products_avatar_check CHECK (avatar <> ''::varchar),
    password    varchar                  NOT NULL CONSTRAINT products_password_check CHECK (password <> ''::varchar),
    created_at  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);



CREATE TABLE IF NOT EXISTS public.order_goal (
    id              BIGSERIAL PRIMARY KEY,
    note    TEXT,
    miss    INT,
    price_old INT,
    product_name    varchar,
    add_payments          INT,
    is_delete VARCHAR,
    price           INT,
    order_date      date ,
    percent         BIGINT REFERENCES category_percent(percent_id) ,
    customer_id     BIGINT REFERENCES customers(customer_id) ,
    return_date DATE,
    status VARCHAR(20),
    interest_paid_count INT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
CREATE TABLE IF NOT EXISTS public.order_vehicle (
    id              BIGSERIAL PRIMARY KEY,
    product_name    varchar,
    license_plate   varchar,
    add_payments          INT,
    note    TEXT,
    miss INT,
    price_old INT,
    price           INT,
    order_date      date ,
    percent         BIGINT REFERENCES category_percent(percent_id) ,
    return_date DATE,
    is_delete VARCHAR,
    status VARCHAR(20),
    interest_paid_count INT,
    customer_id     BIGINT REFERENCES customers(customer_id) ,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- Trigger cho bảng "interest_payments_vehicle"
CREATE OR REPLACE FUNCTION delete_add_payments_vehicle_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ lệnh đặt bị xóa
    month_val := EXTRACT(MONTH FROM OLD.date_add_payment);
    year_val := EXTRACT(YEAR FROM OLD.date_add_payment);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_vehicle" chưa
    IF EXISTS (SELECT * FROM spend_vehicle WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_vehicle
        SET amount = amount - paymentAmount
        WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_add_payments_vehicle
AFTER DELETE ON add_payments_vehicle
FOR EACH ROW
EXECUTE FUNCTION delete_add_payments_vehicle_function();


-- Trigger cho bảng "interest_payments_vehicle"
CREATE OR REPLACE FUNCTION delete_add_payments_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ lệnh đặt bị xóa
    month_val := EXTRACT(MONTH FROM OLD.date_add_payment);
    year_val := EXTRACT(YEAR FROM OLD.date_add_payment);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_vehicle" chưa
    IF EXISTS (SELECT * FROM spend_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_add_payments_goal
AFTER DELETE ON add_payments_goal
FOR EACH ROW
EXECUTE FUNCTION delete_add_payments_goal_function();

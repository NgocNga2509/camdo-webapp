CREATE OR REPLACE FUNCTION update_interest_paid_goal_count()
RETURNS TRIGGER AS $$
BEGIN
  IF TG_OP = 'INSERT' THEN
    UPDATE order_goal
    SET interest_paid_count = interest_paid_count + 1
    WHERE id = NEW.order_id;
  ELSIF TG_OP = 'DELETE' THEN
    UPDATE order_goal
    SET interest_paid_count = interest_paid_count - 1
    WHERE id = OLD.order_id;
  END IF;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_interest_count_goal_trigger
AFTER INSERT OR DELETE ON interest_payments_goal
FOR EACH ROW
EXECUTE FUNCTION update_interest_paid_goal_count();

CREATE OR REPLACE FUNCTION update_interest_paid_vehicle_count()
RETURNS TRIGGER AS $$
BEGIN
  IF TG_OP = 'INSERT' THEN
    UPDATE order_vehicle
    SET interest_paid_count = interest_paid_count + 1
    WHERE id = NEW.order_id;
  ELSIF TG_OP = 'DELETE' THEN
    UPDATE order_vehicle
    SET interest_paid_count = interest_paid_count - 1
    WHERE id = OLD.order_id;
  END IF;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_interest_count_vehicle_trigger
AFTER INSERT OR DELETE ON interest_payments_vehicle
FOR EACH ROW
EXECUTE FUNCTION update_interest_paid_vehicle_count();

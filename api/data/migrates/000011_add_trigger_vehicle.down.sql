DROP TRIGGER IF EXISTS increase_revenue_vehicle ON interest_payments_goal;
DROP TRIGGER IF EXISTS increase_collect_vehicle ON interest_payments_vehicle;
DROP TRIGGER IF EXISTS increase_collect_redeem_vehicle ON redeem_order_vehicle;
DROP TRIGGER IF EXISTS increase_spend_vehicle ON order_vehicle;
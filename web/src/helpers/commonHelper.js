export function getDataFromURL(url) {
    const arr = url.split("/");
    let data = {};
    if (arr[1] == "ref") {
      data["partnerReferral"] = arr[2];
    } else {
      data["partnerReferral"] = arr[1];
      if (arr[2]) data["redeemCode"] = arr[2];
    }
    return data;
  }
  
  export function getDataFromURLTest(url) {
    const arr = url.split("/");
    let data = {};
    if (arr[4] == "ref") {
      data["partnerReferral"] = arr[5];
    } else {
      data["partnerReferral"] = arr[4];
      if (arr[5]) data["redeemCode"] = arr[5];
    }
    return data;
  }
  
  /**
   * This function is equivalent to PHP's array_column
   * @param array
   * @param column
   */
  export const array_column = (array, column) => array.map((e) => e[column]);
  
  /**
   * Convert default base64String to valid string
   * @param base64String
   */
  export function validBase64String(base64String) {
    return base64String
      .replace("data:image/jpeg;base64,", "")
      .replace("data:image/png;base64,", "");
  }
  
  export const getCurrentRole = () => {
    let roles = localStorage.getItem("role");
    return roles;
  };
  
  export const getCurrentUserRole = (userRole) => {
    let role = localStorage.getItem(userRole);
    return role;
  };
  
  export const getCurrentMerchantPartner = () => {
    let isMerchant = localStorage.getItem("isMerchant");
    return isMerchant;
  };
  
  export const generateId = () => {
    let id = Math.floor(new Date().valueOf() * Math.random())
      .toString()
      .substring(0, 6);
    return `PN${id}`;
  };
  
  export const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };
  
  export const renderNextPre = (_, type, originalElement) => {
    if (type === "prev") {
      return <a>Previous</a>;
    }
  
    if (type === "next") {
      return <a>Next</a>;
    }
    return originalElement;
  };
  
  export const customFormat = (value) => {
    return value.format("MMM D,YYYY");
  };
  
  export const lazyRetry = (componentImport, name) => {
    return new Promise((resolve, reject) => {
      // check if the window has already been refreshed
      const hasRefreshed = JSON.parse(
        window.sessionStorage.getItem(`retry-${name}-refreshed`) || "false"
      );
      // try to import the component
      componentImport()
        .then((component) => {
          window.sessionStorage.setItem("retry-lazy-refreshed", "false"); // success so reset the refresh
          resolve(component);
        })
        .catch((error) => {
          if (!hasRefreshed) {
            // not been refreshed yet
            window.sessionStorage.setItem("retry-lazy-refreshed", "true"); // we are now going to refresh
            return window.location.reload(); // refresh the page
          }
          reject(error); // Default error behaviour as already tried refresh
        });
    });
  };
  
  export const renderStatusTooltip = (key) => {
    if (key === 0) {
      return "Pending";
    } else if (key === 1) {
      return "Active";
    } else if (key === 2) {
      return "Rejected";
    } else if (key === 3) {
      return "Suspended";
    } else {
      return "Deleted";
    }
  };
  
  export const renderPaymentScheme = (key) => {
    if (key === 0) {
      return "Prepaid";
    } else if (key === 1) {
      return "Postpaid";
    } else if (key === 2) {
      return "Consignment";
    } else {
      return "Disable";
    }
  };
  
  export const renderAffiliate = (key) => {
    if (key === true) {
      return "Enable";
    } else if (key === false) {
      return "Disable";
    } else {
      return "";
    }
  };
  
  export const renderGender = (key) => {
    if (key === 0) {
      return "Male";
    } else if (key === 1) {
      return "Female";
    } else {
      return "Other";
    }
  };
  
  export const renderAssigned = (key) => {
    if (key === true) {
      return "Yes";
    } else if (key === false) {
      return "No";
    } else {
      return "";
    }
  };
  
  export const renderStatus = (status) => {
    switch (status) {
      case 0:
        return <span className="text-success">Available</span>;
      case 1:
        return <span className="text-danger">UnAvailable</span>;
      case 3:
        return <span className="text-danger">Assigned</span>;
      case 4:
        return <span className="text-danger">Error</span>;
      case 5:
        return <span className="text-danger">Export</span>;
      default:
        break;
    }
  };
  
  export const renderStatusMember = (key) => {
    if (key === 1) {
      return "Active";
    } else if (key === 3) {
      return "Pending";
    } else {
      return "Suspended";
    }
  };
  
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import BaseURL from '../api/baseURL';

const AuthWrapper = () => {
    const navigate = useNavigate();

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
            // Chuyển hướng người dùng đến trang đăng nhập nếu không có mã token
            return false
        }
    console.log(BaseURL);
    }, []);

    return true;
};

export default AuthWrapper;

import React, { useEffect, useState } from "react";
import BaseURL from "../api/baseURL";

export const FormatCurrency = ({ amount }) => {
  const formatter = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
    minimumFractionDigits: 0,
  });
  return formatter.format(amount);
};

export const FormatDate = ({ date }) => {
  const dateObject = new Date(date);

  const day = dateObject.getDate();
  const month = dateObject.getMonth() + 1;
  const year = dateObject.getFullYear();

  return `${day < 10 ? "0" + day : day}/${month < 10 ? "0" + month : month
    }/${year}`;
};


export const NumberToWords = ({ amount }) => {
  const defaultNumbers = " hai ba bốn năm sáu bảy tám chín";

  const chuHangDonVi = ("1 một" + defaultNumbers).split(" ");
  const chuHangChuc = ("lẻ mười" + defaultNumbers).split(" ");
  const chuHangTram = ("không một" + defaultNumbers).split(" ");

  const convert_block_three = (number) => {
    if (number == "000") return "";
    var _a = number + ""; //Convert biến 'number' thành kiểu string

    //Kiểm tra độ dài của khối
    switch (_a.length) {
      case 0:
        return "";
      case 1:
        return chuHangDonVi[_a];
      case 2:
        return convert_block_two(_a);
      case 3:
        var chuc_dv = "";
        if (_a.slice(1, 3) != "00") {
          chuc_dv = convert_block_two(_a.slice(1, 3));
        }
        var tram = chuHangTram[_a[0]] + " trăm";
        return tram + " " + chuc_dv;
    }
  };

  const convert_block_two = (number) => {
    var dv = chuHangDonVi[number[1]];
    var chuc = chuHangChuc[number[0]];
    var append = "";

    // Nếu chữ số hàng đơn vị là 5
    if (number[0] > 0 && number[1] == 5) {
      dv = "lăm";
    }

    // Nếu số hàng chục lớn hơn 1
    if (number[0] > 1) {
      append = " mươi";

      if (number[1] == 1) {
        dv = " mốt";
      }
    }

    return chuc + "" + append + " " + dv;
  };
  const dvBlock = "1 nghìn triệu tỷ".split(" ");

  const to_vietnamese = (number) => {
    var str = parseInt(number) + "";
    var i = 0;
    var arr = [];
    var index = str.length;
    var result = [];
    var rsString = "";

    if (index == 0 || str == "NaN") {
      return "";
    }

    // Chia chuỗi số thành một mảng từng khối có 3 chữ số
    while (index >= 0) {
      arr.push(str.substring(index, Math.max(index - 3, 0)));
      index -= 3;
    }

    // Lặp từng khối trong mảng trên và convert từng khối đấy ra chữ Việt Nam
    for (i = arr.length - 1; i >= 0; i--) {
      if (arr[i] != "" && arr[i] != "000") {
        result.push(convert_block_three(arr[i]));

        // Thêm đuôi của mỗi khối
        if (dvBlock[i]) {
          result.push(dvBlock[i]);
        }
      }
    }

    // Join mảng kết quả lại thành chuỗi string
    rsString = result.join(" ");

    // Trả về kết quả kèm xóa những ký tự thừa
    return rsString.replace(/[0-9]/g, "").replace(/ /g, " ").replace(/ $/, " ");
  };

  return to_vietnamese(amount);
};

export const FormatAmountIntToFloat = (value) => {
  if (value != "") {
    // Xóa các dấu phân tách hiện có trong giá trị
    const cleanedValue = value.replace(/,/g, "");

    // Chuyển đổi thành số nguyên
    const intValue = parseInt(cleanedValue);

    // Kiểm tra nếu không phải số hợp lệ
    if (isNaN(intValue)) {
      return "";
    }

// Định dạng số thực bằng cách thêm dấu phân tách 3 số
    const floatValue = intValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return floatValue;

  }
};
export const FormatAmountFloatToInt = (value) => {
  if (value != "") {
    // Xóa các dấu phân tách
    const cleanedValue = value.replace(/,/g, "");

    // Chuyển đổi thành số nguyên
    const intValue = parseInt(cleanedValue);

    // Kiểm tra nếu không phải số hợp lệ
    if (isNaN(intValue)) {
      return "";
    }

    return intValue.toString();
  }
};

export const ConvertIDToPercentName = async (id) => {
  try {
    const response = await BaseURL.get(`list-category-percent/${id}`);
    if (response.status === 200) {
      const cateName = response.data.Percent;
      return cateName;
    }
  } catch (error) {
    if (error.response) {
      console.log("Server responded with a non-2xx status");
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      console.log("No response received from the server");
      console.log(error.request);
    } else {
      console.log("Error setting up the request");
      console.log(error.message);
    }
    console.log(error.config);
  }
};

export const ConvertCategory = async (id) => {
  try {
    const response = await BaseURL.get(`list-category/${id}`);
    if (response.status === 200) {
      const cateName = response.data.CategoryName;
      return cateName;
    }
  } catch (error) {
    if (error.response) {
      console.log("Server responded with a non-2xx status");
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      console.log("No response received from the server");
      console.log(error.request);
    } else {
      console.log("Error setting up the request");
      console.log(error.message);
    }
    console.log(error.config);
  }
};
export const FormatNumberFloat = (number) => {
  if (number != "") {
    const cleanedNumber = number.replace(/,/g, ''); // Remove commas from the number
    const parsedNumber = parseFloat(cleanedNumber);

    if (isNaN(parsedNumber)) {
      return "Invalid number";
    }

    const roundedNumber = Math.round(parsedNumber);
    const formattedNumber = roundedNumber.toLocaleString("en-US", { maximumFractionDigits: 0 });

    return formattedNumber;
  }
}
export const ConvertCategoryGoal = async (id) => {
  try {
    const response = await BaseURL.get(`list-category-goal/${id}`);
    if (response.status === 200) {
      const cateName = response.data.CategoryName;
      return cateName
    }
  } catch (error) {
    if (error.response) {
      console.log("Server responded with a non-2xx status");
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      console.log("No response received from the server");
      console.log(error.request);
    } else {
      console.log("Error setting up the request");
      console.log(error.message);
    }
    console.log(error.config);
  }
};
export const CategoryColumn = ({ categoryId, cate }) => {
  const [categoryName, setCategoryName] = useState('');

  useEffect(() => {
    const fetchCategoryName = async () => {
      try {
        const categoryName = cate == 0 ? await ConvertCategory(categoryId) : cate == 1 ? await ConvertCategoryGoal(categoryId) : await ConvertIDToPercentName(categoryId);
        setCategoryName(categoryName);
      } catch (error) {
        // Handle error
        console.log(error);
      }
    };

    fetchCategoryName();
  }, [categoryId]);

  return <span>{categoryName}</span>;
};


import { useEffect, useState } from "react";
import TheSideBar from "./TheSideBar";
import BaseURL from "../api/baseURL";

const TheContent = ({ resetFlag,content, active, setActive, checkSubMenuCustomer, checkSubMenuGoal, checkSubMenuVehicle, isExpand, setIsExpand }) => {
//   const [active, setActive] = useState(1);
  const [numberNotification, setNumberNotification]=useState(0)
  const [numberNotificationVehicle, setNumberNotificationVehicle]=useState(0)

  useEffect(()=>{
    BaseURL.get("payment/get-count-order-goal-over").then((response)=>{
      if(response.status ==200){
        setNumberNotification(response.data)
      }
    })
    BaseURL.get("payment/get-count-order-vehicle-over").then((response)=>{
      if(response.status ==200){
        setNumberNotificationVehicle(response.data)
      }
    })
  },[resetFlag])
  return (
    <body class="">
      <div class="wrapper">
        <TheSideBar isExpand={isExpand} setIsExpand={setIsExpand} active={active} numberNotificationVehicle={numberNotificationVehicle}  numberNotification={numberNotification} setActive={setActive} checkSubMenuCustomer={checkSubMenuCustomer} checkSubMenuGoal={checkSubMenuGoal} checkSubMenuVehicle={checkSubMenuVehicle}/>
        <div>{content}</div>
      </div>
    </body>
  );
};
export default TheContent;

import { useEffect, useState } from "react";
import TheContent from "../../helpers/TheContent";
import {
  Button,
  ConfigProvider,
  DatePicker,
  Input,
  Pagination,
  Popover,
  Select,
  Table,
} from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import { FormatCurrency, FormatDate } from "../../helpers/Help";
import PayVehicle from "../pay/PayVehicle";
import DrawerDetailVehicle from "./DrawerDetailVehicle";
import DrawerEditVehicle from "./DrawerEditVehicle";
import locale from "antd/es/date-picker/locale/vi_VN";
import viVN from "antd/lib/locale/vi_VN";
import { ModalDelete } from "../modal/ModalDelete";
import RedeemOrderVehicle from "../pay/RedeemOrderVehicle";
import AddPaymentVehicle from "../add/AddPaymentVehicle";
import UpdateNote from "./UpdateNote";
import { Spin } from "antd";

const ListOrderVehicle = () => {
  const [loading, setLoading] = useState(true);
  const [percent, setPercent] = useState([]);
  const [active, setActive] = useState(6);
  const [subMenu, setSubMenu] = useState(true);
  const [orderID, setOrderID] = useState("");
  const [data, setData] = useState([]);
  const [popoverContent, setPopoverContent] = useState({});
  const [modalPay, setModalPay] = useState(false);
  const [resetFlag, setResetFlag] = useState(1);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [openModalPay, setOpenModalPay] = useState(false);
  const [openDrawerEdit, setOpenDrawerEdit] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [searchContent, setSearchContent] = useState("");
  const [openModalAddPayment, setOpenModalAddPayment] = useState(false);
  const [expandSidebar, setExpandSidebar] = useState(false);
  const [isUpdateNote, setIsUpdateNote] = useState(false);
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState(0);
  const [customerData, setCustomerData] = useState([]); // Add a state variable to store the customer name
  const [datePickerKey, setDatePickerKey] = useState(0); // Initialize with 0
  const [code, setCode] = useState("");
  const [isSearch, setIsSearch] = useState(false);

  const convertCustomer = (id) => {
    let name = "";
    customerData.map((item) => {
      if (item.CustomerID == id) {
        name = item.CustomerName;
      }
    });
    return name;
  };
  const fetchData = async (id) => {
    try {
      const response = await BaseURL.get(`list-customer/${id}`);
      const customerData = response.data;

      // Xây dựng nội dung popover từ kết quả API
      const content = (
        <div className="content-customer">
          <ul>
            <li>
              <b>Mã:</b> <div>{customerData.customer_id}</div>
            </li>
            <li>
              <b>CCCD:</b> <div>{customerData.cccd}</div>
            </li>
            <li>
              <b>SĐT:</b> <div>{customerData.phone}</div>
            </li>
            <li>
              <b>Địa chỉ:</b>
              <div>{customerData.address}</div>{" "}
            </li>
          </ul>
        </div>
      );

      // Cập nhật nội dung popover
      setPopoverContent({ [id]: content });
    } catch (error) {
      console.error("Error fetching customer data:", error);
    }
  };

  const handleSelectAction = (e, id) => {
    const params = {
      id: id,
      status: "2",
    };
    if (e === 0) {
      setOrderID(id);
      setOpenDrawer(true);
    } else if (e === 1) {
      setOrderID(id);
      setOpenDrawerEdit(true);
    } else if (e === 2) {
      setOrderID(id);
      setOpenModalDelete(true);
    } else if (e === 3) {
      BaseURL.put("payment/update-status-order-vehicle", params).then(
        (response) => {
          setResetFlag(resetFlag + 1);
        }
      );
    }
  };
  const isOverdue = (returnDate) => {
    const currentDate = new Date();
    const returnDateObj = new Date(returnDate);
    const diffInDays = Math.floor(
      (currentDate - returnDateObj) / (1000 * 60 * 60 * 24)
    );
    const diffInDays2 = Math.floor(
      (currentDate - returnDateObj.setMonth(returnDateObj.getMonth() - 1)) /
        (1000 * 60 * 60 * 24)
    );
    if (diffInDays > 0 && diffInDays2 >= 60) {
      return 2;
    } else if (diffInDays > 0) {
      return 1;
    } else {
      return 0;
    }
  };

  const renderReturnDate = (interests, record) => {
    return (
      <div>
        {record.Status == "0" && (
          <ul className="ul-payments-info">
            {record.Payments && record.Payments != null ? (
              <li>
                <div className="li-date">
                  <FormatDate
                    date={
                      record.Payments[record.Payments.length - 1]
                        .NextPaymentDate
                    }
                  />
                  {isOverdue(
                    record.Payments[record.Payments.length - 1].NextPaymentDate
                  ) === 2 ? (
                    <div>Hóa giá</div>
                  ) : isOverdue(
                      record.Payments[record.Payments.length - 1]
                        .NextPaymentDate
                    ) ? (
                    <div>Trễ hạn</div>
                  ) : (
                    <div></div>
                  )}
                </div>
              </li>
            ) : (
              <li>
                <div className="li-date">
                  <FormatDate date={record.ReturnDate} />
                  {isOverdue(record.ReturnDate) === 2 ? (
                    <div>Hóa giá</div>
                  ) : isOverdue(record.ReturnDate) === 1 ? (
                    <div>Trễ hạn</div>
                  ) : (
                    <div></div>
                  )}
                </div>
              </li>
            )}
          </ul>
        )}
      </div>
    );
  };
  const convertStatus = (status) => {
    let stt = "";
    if (status === "0") {
      stt = (
        <span style={{ color: "green", fontWeight: "bold" }}>Còn hiệu lực</span>
      );
    } else if (status === "1") {
      stt = (
        <span style={{ color: "#86518a", fontWeight: "bold" }}>
          Đã chuộc tài sản
        </span>
      );
    } else {
      stt = (
        <span style={{ color: "red", fontWeight: "bold" }}>Hồ sơ hóa giá</span>
      );
    }
    return stt;
  };

  const renderNote = (note, record) => {
    return (
      <div className="note">
        <div>
          {note != null && (
            <div className="note-content">
              <div>{record.Miss == 1 && "Khách mất giấy"}</div>
              {note}
            </div>
          )}
          <div className="note-update">
            <Button
              onClick={() => {
                setOrderID(record.ID);
                setIsUpdateNote(true);
              }}
            >
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </Button>
          </div>
        </div>
      </div>
    );
  };

  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "ID",
      key: "ID",
      render: (id, record) => (
        <div className="text-bold">
          {record.STT}-{record.Code}
        </div>
      ),
    },
    {
      title: "Tên khách hàng",
      dataIndex: "CustomerID",
      key: "CustomerID",
      render: (id) => (
        <div className="info-id-customer">
          <div className="info-id-customer-id">{convertCustomer(id)}</div>
          <div className="info-id-customer-icon">
            <Popover
              content={popoverContent[id]}
              trigger="hover"
              title="Thông tin chi tiết khách hàng:"
            >
              <InfoCircleOutlined onMouseEnter={() => fetchData(id)} />
            </Popover>
          </div>
        </div>
      ),
    },
    {
      title: "Tên xe",
      dataIndex: "ProductName",
      key: "ProductName",
    },
    {
      title: "Biển số",
      dataIndex: "LicensePlate",
      key: "LicensePlate",
      width: 200,
    },
    {
      title: "Giá cầm",
      dataIndex: "Price",
      key: "Price",
      width: 250,
      render: (amount, record) => (
        <div className="add-payment">
          <div className="add-payment-new-amount">
            <FormatCurrency amount={record.Price} />
          </div>
          {record.AddPayment != null && (
            <div className="add-payment-old-amount">
              Tiền gốc: <FormatCurrency amount={record.PriceOld} />
            </div>
          )}
          {record.AddPayment &&
            record.AddPayment.map((item) => (
              <div>
                Cầm thêm <FormatCurrency amount={item.Amount} /> ngày{" "}
                <FormatDate date={item.DateAddPayment} />
              </div>
            ))}
        </div>
      ),
    },
    {
      title: "Lãi suất",
      dataIndex: "Percent",
      key: "Percent",
      render: (percent) => convertPercent(percent),
    },
    {
      title: "Trạng thái hồ sơ",
      dataIndex: "Status",
      key: "Status",
      width: 250, // Độ rộng của cột
      render: (stt) => (
        <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
      ),
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },

    {
      title: "Thông tin đóng lãi",
      dataIndex: "interestAmount",
      key: "interestAmount",
      render: (item, record) => (
        <div>
          {record.Status == "0" && (
            <ul className="ul-payments-info">
              {record.Payments && record.Payments != null ? (
                record.Payments.map((interest) => (
                  <li key={interest.ID}>
                    <div className="li-date">
                      <FormatDate date={interest.PaymentDate} />
                    </div>
                    <div className="li-amount">
                      <FormatCurrency amount={interest.Amount} />{" "}
                    </div>
                  </li>
                ))
              ) : (
                <li>Chưa đóng lãi</li>
              )}
            </ul>
          )}
        </div>
      ),
    },
    {
      title: "Ngày tới hạn",
      dataIndex: "interestAmount",
      key: "interestAmount",
      render: (interests, record) => renderReturnDate(interests, record),
    },
    {
      title: "Ghi chú",
      dataIndex: "Note",
      key: "Note",
      render: (note, record) => renderNote(note, record),
    },
    {
      title: "",
      dataIndex: "ID",
      key: "ID",
      render: (id, record) => (
        <div className="button-action">
          {record.Status != "1" && (
            <>
              <Button
                className="button-action-detail"
                onClick={() => {
                  setOrderID(id);
                  setModalPay(true);
                }}
              >
                Đóng lãi
              </Button>
              <Button
                className="button-action-payment"
                onClick={() => {
                  setOrderID(id);
                  setOpenModalPay(true);
                }}
              >
                Thanh toán
              </Button>
              <Button
                className="button-action-payment"
                onClick={() => {
                  setOrderID(id);
                  setOpenModalAddPayment(true);
                }}
              >
                Cầm thêm
              </Button>
            </>
          )}
          <Select
            defaultValue="Thao tác"
            className="select-search-action"
            onChange={(e) => handleSelectAction(e, id)}
          >
            <Select.Option value={0}>Chi tiết</Select.Option>
            <Select.Option value={1}>Sửa</Select.Option>
            <Select.Option value={2}>Xóa</Select.Option>
            <Select.Option value={3}>Hóa giá</Select.Option>
          </Select>
        </div>
      ),
    },
  ];

  const handleDelete = (id) => {
    BaseURL.delete(`delete-order-vehicle/${id}`).then((response) => {
      if (response.status === 200) {
        setResetFlag(resetFlag + 1);
        setOpenModalDelete(false);
      }
    });
  };

  const dataSource =
    data &&
    data.map((item, index) => ({
      key: index,
      ID: item.order.ID,
      CustomerID: item.order.CustomerID,
      ProductName: item.order.ProductName,
      LicensePlate: item.order.LicensePlate,
      EstimatedPrice: item.order.EstimatedPrice,
      AddPayment: item.add_payments,
      Payments: item.payments,
      Price: item.order.Price,
      PriceOld: item.order.PriceOld,
      Miss: item.order.Miss,
      ReturnDate: item.order.ReturnDate,
      Status: item.order.Status,
      Percent: item.order.Percent,
      OrderDate: item.order.OrderDate,
      Code: item.order.Code,
      STT: item.order.STT,
      interestAmount: item.order.interestAmount,
      Note: item.order.Note,
    }));

  const filterMiss = (data) => {
    if (filteredData === 1) {
      return data && data.filter((item) => item && item.Miss === 1);
    } else {
      return data;
    }
  };

  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  const pageSize = 8; // Kích thước trang
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData =
    dataSource &&
    dataSource.slice((currentPage - 1) * pageSize, currentPage * pageSize);

  const fetchDataSearch = async () => {
    setLoading(true);
    try {
      const response = await BaseURL.post("search-order-vehicle-stt", {
        stt: search == "" ? 0 : parseInt(search),
        code: code,
        miss: parseInt(filteredData),
      });

      const dataFromApi = response.data;
      setData(dataFromApi);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const fetchDataPage = async () => {
    setLoading(true);
    try {
      const response = await BaseURL.get("get-order-vehicle");

      const dataFromApi = response.data;
      setData(dataFromApi);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleChangeFilter = (value) => {
    setFilteredData(value);
  };
  console.log(filteredData);
  useEffect(() => {
    isSearch == false && filteredData == 0
      ? fetchDataPage()
      : fetchDataSearch();
    setCurrentPage(1);
  }, [resetFlag, isSearch, filteredData]);

  useEffect(() => {
    BaseURL.get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setPercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL.get("list-customer")
      .then((response) => {
        if (response.status === 200) {
          console.log(response.data);
          setCustomerData(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);

  const convertPercent = (cate) => {
    let name = "";
    percent.map((item) => {
      if (item.PercentID == cate) {
        name = item.Percent;
      }
    });
    return name;
  };

  const getRowClassName = (record, index) => {
    return index % 2 === 0 ? "even-row" : "odd-row";
  };

  const handleChangeMonth = (dateString) => {
    if (dateString == null) {
      setCode("");
    } else {
      const date = new Date(dateString);
      const year = date.getFullYear().toString().slice(-2); // Lấy hai chữ số cuối của năm
      const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Đảm bảo tháng luôn có hai chữ số
      const code = month + year;
      setCode(code);
    }
  };
  console.log(code);
  const handleReset = () => {
    setSearch("");
    setCode("");
    setFilteredData(0);
    setIsSearch(false);
    setDatePickerKey(datePickerKey + 1);
    setResetFlag(resetFlag + 1);
  };

  const content = (
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <Button
                type="button"
                class="navbar-toggler"
                onClick={() => setExpandSidebar(true)}
              >
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </Button>
            </div>
            <a class="navbar-brand" href="javascript:;">
              QUẢN LÝ ĐƠN XE
            </a>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                {/* <h4 class="card-title">Danh sách khách hàng</h4> */}
                <div className="d-flex-column search-customer">
                  <div className="d-flex">
                    <div className="picker-list-order-date">
                      <ConfigProvider locale={viVN}>
                        <DatePicker
                          key={datePickerKey}
                          picker="month"
                          onChange={handleChangeMonth}
                          format={"MM/YYYY"}
                          locale={locale}
                        />
                      </ConfigProvider>
                    </div>
                    <div className="d-flex">
                      <Input
                        type="text"
                        //value=""
                        className="form-control"
                        placeholder="Tìm kiếm..."
                        value={search}
                        onInput={(e) => setSearch(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.key === "Enter") {
                            setIsSearch(true);
                            fetchDataSearch();
                          }
                        }}
                      />
                      <div
                        className="input-group-append"
                        onClick={() => {
                          setIsSearch(true);
                          fetchDataSearch();
                        }}
                      >
                        <div className="input-group-text">
                          <i class="nc-icon nc-zoom-split"></i>
                        </div>
                      </div>
                      <div className="input-group-append reset">
                        <div
                          className="input-group-text"
                          onClick={() => {
                            handleReset();
                          }}
                        >
                          <i class="fa fa-refresh" aria-hidden="true"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Select
                    className="select-search"
                    defaultValue={0}
                    onChange={handleChangeFilter}
                    value={filteredData}
                  >
                    <Select.Option value={0}>Tất cả</Select.Option>
                    <Select.Option value={1}>Khách mất giấy</Select.Option>
                  </Select>
                </div>
              </div>
              <div class="card-body">
                {loading ? (
                  <Spin size="large" /> // Hiển thị loading khi loading = true
                ) : (
                  <div class="table-responsive">
                    <Table
                      columns={columns}
                      dataSource={currentPageData}
                      pagination={false}
                      rowClassName={getRowClassName}
                    />
                    <Pagination
                      showSizeChanger={false}
                      className="pagination-category"
                      current={currentPage}
                      pageSize={pageSize}
                      total={totalItems}
                      onChange={handlePageChange}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  return (
    <>
      <TheContent
        content={content}
        active={active}
        setActive={setActive}
        checkSubMenuVehicle={subMenu}
        resetFlag={resetFlag}
        isExpand={expandSidebar}
        setIsExpand={setExpandSidebar}
      />
      {modalPay && (
        <PayVehicle
          isOpen={modalPay}
          setIsOpen={setModalPay}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openDrawer && (
        <DrawerDetailVehicle
          open={openDrawer}
          setOpen={setOpenDrawer}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openDrawerEdit && (
        <DrawerEditVehicle
          isOpen={openDrawerEdit}
          setIsOpen={setOpenDrawerEdit}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalAddPayment && (
        <AddPaymentVehicle
          isOpen={openModalAddPayment}
          setIsOpen={setOpenModalAddPayment}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalPay && (
        <RedeemOrderVehicle
          isOpen={openModalPay}
          setIsOpen={setOpenModalPay}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalDelete && (
        <ModalDelete
          isOpen={openModalDelete}
          setIsOpen={setOpenModalDelete}
          handleDelete={handleDelete}
          id={orderID}
        />
      )}
      {isUpdateNote && (
        <UpdateNote
          isOpen={isUpdateNote}
          setIsOpen={setIsUpdateNote}
          id={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
    </>
  );
};
export default ListOrderVehicle;

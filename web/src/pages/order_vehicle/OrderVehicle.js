import { useEffect, useState } from "react";
import TheContent from "../../helpers/TheContent";
import { Button, ConfigProvider, DatePicker, Form, Input, Select } from "antd";
import TextArea from "antd/es/input/TextArea";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import "moment/locale/vi";
import dayjs from "dayjs";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
import ModalCustomer from "../customer/ModalCustomer";
import ModalNewCustomer from "../customer/ModalNewCustomer";
import { ToastContainer, toast } from "react-toastify";
import {
  FormatAmountFloatToInt,
  FormatAmountIntToFloat,
  FormatDate,
} from "../../helpers/Help";
import PrintOrder from "../modal/PrintOrder";

const OrderVehicle = () => {
  const [subMenu, setSubMenu] = useState(true);
  const [active, setActive] = useState(5);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [dataCustomer, setDataCustomer] = useState([]);
  const [dataNewCustomer, setDataNewCustomer] = useState([]);
  const [isSelect, setIsSelect] = useState(false);
  const [isSelectNew, setIsSelectNew] = useState(0);
  const [newCustomer, setNewCustomer] = useState(false);
  const isSubmit = isSelect || isSelectNew ? "true" : "fasle";
  const [date, setDate] = useState(moment().locale("vi"));
  const [percent, setPercent] = useState(2);
  const [message, setMessage] = useState("");
  const [dataCatePercent, setDataCatePercent] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [isSuccess, setIsSuccess] = useState(false);
  const [expandSidebar, setExpandSidebar] = useState(false)

  const [formData, setFormData] = useState({
    product_name: "",
    price: 0,
    customer_id: "",
    license_plate: "",
    estimated_price: "",
    order_date: moment(),
    return_date: moment().add(1, "month"),
    percent: "",
    status: "0",
    interest_paid_count: 0,
    note: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const originalDate = new Date();
  // Lấy các thông tin ngày, tháng, năm từ ngày ban đầu
  const day = originalDate.getDate();
  const month = originalDate.getMonth() + 1; // Tháng trong JavaScript bắt đầu từ 0, nên cần cộng thêm 1
  const year = originalDate.getFullYear();

  // Định dạng lại thành dd/mm/yyyy
  const formattedDate = `${year}-${month < 10 ? "0" + month : month}-${day < 10 ? "0" + day : day
    }`;

  const handleSelectPercent = (e) => {
    setFormData({ ...formData, percent: e });

  };
  const convertNextMonth = (day) => {
    const date = dayjs(day);
    // Lấy ngày sau 1 tháng
    const nextMonth = date.add(1, "month");

    // Lấy thông tin ngày, tháng, năm từ ngày sau 1 tháng
    const nextMonthDay = nextMonth.date();
    const nextMonthMonth = nextMonth.month() + 1; // Tháng trong Day.js bắt đầu từ 0, nên cần cộng thêm 1
    const nextMonthYear = nextMonth.year();
    // Định dạng lại thành dd/mm/yyyy
    const nextMonthFormatted = `${nextMonthDay < 10 ? "0" + nextMonthDay : nextMonthDay
      }/${nextMonthMonth < 10 ? "0" + nextMonthMonth : nextMonthMonth
      }/${nextMonthYear}`;

    const dateS = dayjs(nextMonthFormatted, "DD/MM/YYYY");

    const isoString = dateS.toISOString();

    return isoString;
  };

  const handleChangeDate = (date, dateString) => {
    const dateS = dayjs(date);
    const nextDay = dateS.add(1, "day");
    setSelectedDate(nextDay);
  };
  const [amount, setAmount] = useState("");
  const handleAmountChange = (event) => {
    const value = event.target.value;
    const formattedValue = FormatAmountIntToFloat(value);
    setAmount(formattedValue);
    setFormData({ ...formData, price: value });
  };

  const handleSubmit = () => {
    const isPriceValid = !isNaN(FormatAmountFloatToInt(formData.price))
    const isInterestRateValid = !isNaN(parseFloat(formData.percent));
    const isProductNameValid = formData.product_name.trim() !== ''; // Validate product_name

    if (
      !isInterestRateValid ||
      !isPriceValid ||
      !isProductNameValid || // Add this validation
      isSubmit !== 'true'
    ) {
      setMessage('Vui lòng kiểm tra thông tin nhập vào');
      return; // Prevent form submission
    }

    const params = {
      id: formData.id,
      note: formData.note,
      product_name: formData.product_name,
      estimated_price: parseInt(
        FormatAmountFloatToInt(formData.estimated_price)
      ),
      price: parseInt(FormatAmountFloatToInt(formData.price)),
      order_date: selectedDate == null ? formData.order_date : selectedDate,
      return_date:
        selectedDate == null
          ? formData.return_date
          : convertNextMonth(selectedDate),
      percent: parseFloat(formData.percent),
      license_plate: formData.license_plate,
      customer_id: isSelect
        ? dataCustomer.CustomerID
        : dataNewCustomer.customer_id,
      status: formData.status,
      interest_paid_count: 0,
    };
    BaseURL
      .post("create-order-vehicle", params)
      .then((response) => {
        if (response.status === 200) {
          setIsSuccess(true);
        }
      })
      .catch((err) => toast("Thêm đơn thất bại!"));
  };

  useEffect(() => {
    BaseURL
      .get("get-newest-customer")
      .then((response) => {
        if (response.status === 200) {
          setDataNewCustomer(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [isSelectNew]);

  useEffect(() => {
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setDataCatePercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);

  const content = (
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </Button>
            </div>
            <a class="navbar-brand" href="javascript:;">
              Thêm đơn
            </a>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="row">
          <div class="col-md-10">
            <div class="card ">
              <div class="card-header ">
                <div className="button-customer">
                  {/* <Button
                      className="button-customer-list"
                      onClick={() => {
                        setIsOpenModal(true);
                        setIsSelectNew(false);
                      }}
                    >
                      Danh sách khách hàng
                    </Button> */}
                  <Button
                    className="button-customer-new"
                    onClick={() => {
                      setNewCustomer(true);
                      setIsSelect(false);
                    }}
                  >
                    Khách hàng mới
                  </Button>
                </div>
                <Form
                  className="form-add-pawn"
                  autoSave="off"
                  autoComplete="off"
                >
                  <Form.Item className="form-add-pawn-item" label="Tên xe">
                    <Input
                      className="form-input-pawn"
                      name="product_name"
                      onChange={handleChange}
                    />
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Biển số xe">
                    <Input
                      className="form-input-pawn"
                      name="license_plate"
                      onChange={handleChange}
                    />
                  </Form.Item>
                  {/* <Form.Item
                    className="form-add-pawn-item"
                    label="Giá ước tính"
                  >
                    <Input
                      className="form-input-pawn"
                      name="estimated_price"
                      value={amountEsti}
                      onChange={handleAmountEstiChange}
                      suffix="VND"
                    />
                  </Form.Item> */}
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Giá cầm"
                  >
                    <Input
                      className="form-input-pawn"
                      name="price"
                      value={amount && amount}
                      onChange={handleAmountChange}
                      suffix="VND"
                    />
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Lãi suất">
                    <Select
                      placeholder="Chọn lãi xuất"
                      className="form-input-select"
                      onChange={handleSelectPercent}
                    >
                      {dataCatePercent.map((item) => (
                        <Select.Option value={item.PercentID}>
                          {item.Percent}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Ngày cầm">
                    <ConfigProvider locale={viVN}>
                      <DatePicker
                        className="form-input-pawn"
                        defaultValue={dayjs(formattedDate, "YYYY-MM-DD")}
                        onChange={handleChangeDate}
                        locale={locale}
                      />
                    </ConfigProvider>
                  </Form.Item>
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Ghi chú"
                    name="note"
                  >
                    <TextArea
                      name="note"
                      onChange={handleChange}
                    />
                  </Form.Item>
                  <hr></hr>

                  {isSelect && (
                    <div className="customer-old">
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Tên khách hàng:
                        </div>
                        <div>{dataCustomer.CustomerName}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Ngày tháng năm sinh:
                        </div>
                        <div>
                          {dataCustomer.BirthDate && <FormatDate date={dataCustomer.BirthDate} />}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">Giới tính:</div>
                        <div>{dataCustomer.Gender == "" ? "" : dataCustomer.Gender == "0" ? "Nữ" : "Nam"}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">CCCD:</div>
                        <div>{dataCustomer.CCCD}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">SĐT:</div>
                        <div>{dataCustomer.Phone}</div>
                      </div>
                    </div>
                  )}
                  {isSelectNew > 0 && (
                    <div className="customer-old">
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Tên khách hàng:
                        </div>
                        <div>{dataNewCustomer.customer_name}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Ngày tháng năm sinh:
                        </div>
                        <div>{dataNewCustomer.birth_date && <FormatDate date={dataNewCustomer.birth_date} />}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">Giới tính:</div>
                        <div>
                          {dataNewCustomer.gender == "" ? "" : dataNewCustomer.gender == "0" ? "Nữ" : "Nam"}
                        </div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">CCCD:</div>
                        <div>{dataNewCustomer.cccd}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">SĐT:</div>
                        <div>{dataNewCustomer.phone}</div>
                      </div>
                    </div>
                  )}
                  <div className="message-fail-submit">{message}</div>
                  <div className="save-form-pawn">
                    <div class="update ml-auto mr-auto">
                      <button
                        onClick={() => {
                          handleSubmit();
                        }}
                        type="submit"
                        class="btn btn-primary btn-round"
                      >
                        Lưu đơn
                      </button>
                    </div>
                  </div>
                </Form>
              </div>
              {/* <div class="card-footer ">
            <hr />
            <div class="stats">
              <i class="fa fa-history"></i> Updated 3 minutes ago
            </div>
          </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  return (
    <>
      <TheContent
        content={content}
        active={active}
        setActive={setActive}
        checkSubMenuVehicle={subMenu}
        isExpand={expandSidebar}
        setIsExpand={setExpandSidebar}
      />
      {isOpenModal && (
        <ModalCustomer
          setDataCustomer={setDataCustomer}
          isOpen={isOpenModal}
          setIsOpen={setIsOpenModal}
          setIsSelect={setIsSelect}
          setNewCustomer={setNewCustomer}
          setMessage={setMessage}
        />
      )}
      {newCustomer && (
        <ModalNewCustomer
          isOpenNewCus={newCustomer}
          setIsOpenNewCus={setNewCustomer}
          setIsSelect={setIsSelectNew}
          setMessage={setMessage}
          isSelect={isSelectNew}
        />
      )}
      {isSuccess && (
        <PrintOrder isOpen={isSuccess} setIsOpen={setIsSuccess} />
      )}
      <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
  );
};
export default OrderVehicle;

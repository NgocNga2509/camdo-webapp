import { Button, ConfigProvider, DatePicker, Drawer, Form, Input, Select } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import {
  FormatAmountFloatToInt,
  FormatAmountIntToFloat,
  FormatDate,
} from "../../helpers/Help";
import dayjs from "dayjs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
const DrawerEditOrder = ({
  isOpen,
  setIsOpen,
  orderID,
  resetFlag,
  setResetFlag,
}) => {
  const [data, setData] = useState([]);
  const [products, setProducts] = useState([]);

  const [dataCate, setDataCate] = useState([]);
  const [dataCateGoal, setDataCateGoal] = useState([]);
  const [dataCatePercent, setDataCatePercent] = useState([]);

  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedDateEnd, setSelectedDateEnd] = useState(null);

  const [formData, setFormData] = useState({
    product_name: "",
    estimated_price: "",
    price: "",
    weight_goal: "",
    order_date: "",
    return_date: "",
    status: "",
    interest_paid_count: "",
    category_id: "",
    category_goal_id: "",
    customer_id: "",
    percent: "",
    code: "",
    stt: "",
  });

  const onClose = () => {
    setResetFlag(resetFlag + 1)
    setIsOpen(false);
  };
  useEffect(() => {
    BaseURL
      .get(`get-order-goal/${orderID}`)
      .then((response) => {
        setData(response.data.order);
        setProducts(response.data.products)
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [orderID]);

  const handleChangeDate = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDate(nextDay);
      setFormData({ ...formData, order_date: dateS });
    }
  };

  const handleChangeDateEnd = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDateEnd(nextDay);
      setFormData({ ...formData, return_date: dateS });
    }
  };
  const formatNumber = (number) => {
    if (typeof number === "number") {
      const formattedNumber = number
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return formattedNumber;
    } else {
      return 0;
    }
  };

  const handleAmountChange = (event) => {
    const value = event.target.value;
    const formattedValue = FormatAmountIntToFloat(value);
    setFormData({ ...formData, price: formattedValue });
  };

  useEffect(() => {
    BaseURL
      .get("list-category")
      .then((response) => {
        if (response.status === 200) {
          setDataCate(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-goal")
      .then((response) => {
        if (response.status === 200) {
          setDataCateGoal(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setDataCatePercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [resetFlag]);

  useEffect(() => {
    if (data) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        product_name: data.product_name,
        estimated_price: formatNumber(data.EstimatedPrice),
        price: formatNumber(data.Price),
        weight_goal: data.WeightGoal,
        // order_date: dayjs(data.order_date).format("YYYY-MM-DD"),
        // return_date: dayjs(data.return_date).format("YYYY-MM-DD"),
        order_date: data.OrderDate,
        return_date: data.ReturnDate,
        status: data.Status,
        interest_paid_count: data.InterestPaidCount,
        category_id: data.category_id,
        category_goal_id: data.category_goal_id,
        customer_id: data.CustomerID,
        percent: data.Percent,
      }));
    }
  }, [data]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSelectCate = (e) => {
    setFormData({ ...formData, category_id: e });
  };

  const handleSelectCateGoal = (e) => {
    setFormData({ ...formData, category_goal_id: e });
  };
  const handleSelectStatus = (e) => {
    setFormData({ ...formData, status: e });
  };
  const handleSelectPercent = (e) => {
    setFormData({ ...formData, percent: e });
  };


  const handleSubmit = () => {
    const params = {
      product_name: formData.product_name,
      estimated_price: parseInt(
        FormatAmountFloatToInt(formData.estimated_price)
      ),
      price: parseInt(FormatAmountFloatToInt(formData.price)),
      order_date: selectedDate == null ? formData.order_date : selectedDate,
      return_date:
        selectedDateEnd == null ? formData.return_date : selectedDateEnd,
      weight_goal: parseFloat(formData.weight_goal),
      status: formData.status,
      interest_paid_count: 0,
      percent: parseFloat(formData.percent),
      customer_id: formData.customer_id,
      category_id: parseInt(formData.category_id),
      category_goal_id: parseInt(formData.category_goal_id),
    };
    BaseURL
      .put(`edit-order-goal/${orderID}`, params)
      .then((response) => {
        if (response.status === 200) {
          setIsOpen(false);
          setResetFlag(resetFlag + 1);
          toast("Cập nhật thành công!");
        }
      })
      .catch((err) => toast("Cập nhật thất bại!"));
  };
  const onSave = () => { };

  return (
    <>
      <Drawer
        zIndex={1030}
        className="drawer-detail"
        title={
          <div className="drawer-detail-title">
            SỬA HỒ SƠ<div className="drawer-detail-id">(Mã đơn: {data.STT + "-" + data.Code})</div>
          </div>
        }
        placement="right"
        onClose={onClose}
        open={isOpen}
      >
        <Form className="form-edit-order" autoSave="off" autoComplete="off">
          <Form.Item className="form-edit-order-label" label="Mã khách hàng">
            <Input
              className="form-edit-order-input"
              value={formData.customer_id}
              disabled
            />
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Sản phẩm">
            <li>
              {products && products.map((item, index) => (
                <div key={index}>
                  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
                  (
                  {item.DetailProducts &&
                    item.DetailProducts.map((detail, index) => {
                      // Make sure to return the JSX element here
                      return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
                    })}
                  ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
                </div>
              ))}
            </li>
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Trạng thái">
            <Select
              className="form-edit-order-select"
              value={formData.status}
              onChange={handleSelectStatus}
              name="status"
            >
              <Select.Option value="0">Còn hiệu lực</Select.Option>
              <Select.Option value="1">Đã xong</Select.Option>
              <Select.Option value="2">Đã hóa giá</Select.Option>
            </Select>
          </Form.Item>
          {/* <Form.Item className="form-edit-order-label" label="Loại hàng">
            <Select
              className="form-edit-order-select"
              onChange={handleSelectCate}
              value={formData.category_id}
              name="category_id"
            >
              {dataCate.map((item) => (
                <Select.Option value={item.CategoryID}>
                  {item.CategoryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Loại vàng">
            <Select
              className="form-edit-order-select"
              onChange={handleSelectCateGoal}
              value={formData.category_goal_id}
              name="category_goal_id"
            >
              {dataCateGoal.map((item) => (
                <Select.Option value={item.CategoryID}>
                  {item.CategoryName}
                </Select.Option>
              ))}
            </Select>
          </Form.Item> */}
          {/* <Form.Item className="form-edit-order-label" label="Cân nặng">
            <Input
              className="form-edit-order-input"
              name="weight_goal"
              value={formData.weight_goal}
              onChange={handleChange}
            />
          </Form.Item> */}
          <Form.Item className="form-edit-order-label" label="Số tiền cầm cố">
            <Input
              className="form-edit-order-input"
              name="price"
              value={formData.price}
              onChange={handleAmountChange}
              suffix="VND"
            />
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Lãi suất">
            <Select
              className="form-edit-order-select"
              onChange={handleSelectPercent}
              value={formData.percent}
              name="category_goal_id"
            >
              {dataCatePercent.map((item) => (
                <Select.Option value={item.PercentID}>
                  {item.Percent}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Ngày cầm">
            <ConfigProvider locale={viVN}>
              <DatePicker
                className="form-edit-order-input"
                value={dayjs(formData.order_date, "YYYY-MM-DD")}
                name="order_date"
                onChange={handleChangeDate}
                locale={locale}
              />
            </ConfigProvider>
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Ngày hết hạn">
            <ConfigProvider locale={viVN}>
              <DatePicker
                className="form-edit-order-input"
                value={dayjs(formData.return_date, "YYYY-MM-DD")}
                name="return_date"
                onChange={handleChangeDateEnd}
                locale={locale}
              />
            </ConfigProvider>
          </Form.Item>
          <div className="form-edit-order-save">
            <Button className="btn-cancel" onClick={() => onSave()}>
              Huỷ
            </Button>
            <Button className="btn-ok" onClick={() => handleSubmit()}>
              Lưu thay đổi
            </Button>
          </div>
        </Form>
      </Drawer>
      <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
  );
};
export default DrawerEditOrder;

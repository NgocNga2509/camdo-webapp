import { Button, Drawer } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import {
  FormatCurrency,
} from "../../helpers/Help";
import ModalOrderPrintGold from "../modal/ModalOrderPrintGold";

const DrawerDetail = ({ setOpen, open, orderID, resetFlag, setResetFlag }) => {
  const [dataPayment, setDataPayment] = useState([]);
  const [dataOrder, setDataOrder] = useState([]);
  const [dataCustomer, setDataCustomer] = useState([]);
  const [dataRedeem, setDataRedeem] = useState([]);
  const [isDataRedeem, setIsDataRedeem] = useState(false);

  const [idCustomer, setIdCustomer] = useState("");
  const [isOpenModal, setIsOpenModal] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
    setResetFlag(resetFlag + 1)
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        if (orderID) {
          const paymentResponse = await BaseURL.get(`get-payments-goal/${orderID}`);
          setDataPayment(paymentResponse.data);

          const orderResponse = await BaseURL.get(`get-order-goal/${orderID}`);
          setDataOrder(orderResponse.data);
          setIdCustomer(orderResponse.data.order.CustomerID);

          const redeemResponse = await BaseURL.get(`payment/list-redeem-order-goal/${orderID}`);
          if (redeemResponse.status === 200) {
            setDataRedeem(redeemResponse.data);
            setIsDataRedeem(true);
          }
        }
      } catch (error) {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      }
    };

    fetchData();
  }, [orderID]);

  useEffect(() => {
    const fetchCustomerData = async () => {
      try {
        if (dataOrder && dataOrder.order && dataOrder.order.CustomerID) {
          const customerResponse = await BaseURL.get(`list-customer/${dataOrder.order.CustomerID}`);
          setDataCustomer(customerResponse.data);
        }
      } catch (error) {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      }
    };

    fetchCustomerData();
  }, [dataOrder]);


  const formatDate = (date) => {
    const dateObject = new Date(date);

    const day = dateObject.getDate();
    const month = dateObject.getMonth() + 1;
    const year = dateObject.getFullYear();

    return `${day < 10 ? "0" + day : day}/${month < 10 ? "0" + month : month
      }/${year}`;
  };
  return (
    <>
      <Button type="primary" onClick={showDrawer}>
        Open
      </Button>
      <Drawer
        zIndex={1030}
        className="drawer-detail"
        title={
          <div className="drawer-detail-title">
            CHI TIẾT HỒ SƠ
            <div className="drawer-detail-id">(Mã đơn: {dataOrder?.order?.STT + "-" + dataOrder?.order?.Code})</div>
          </div>
        }
        placement="right"
        onClose={onClose}
        open={open}
        footer={
          <div>
            {" "}
            <Button
              className="btn-detail-print"
              onClick={() => setIsOpenModal(true)}
            >
              In hợp đồng cầm cố
            </Button>
          </div>
        }
      >
        <div className="drawer-div">
          <div className="drawer-title">Thông tin đóng lãi: </div>
          {dataPayment.map((item, index) => (
            <div className="drawer-label-main">
              <div className="drawer-label-main-amount">
                Đóng lãi lần {index + 1}
              </div>
              <div>
                Số tiền: <FormatCurrency amount={item.amount} />
              </div>
              <div>Ngày đóng: {formatDate(item.payment_date)}</div>
            </div>
          ))}
          {isDataRedeem &&
            <div className="drawer-label-main">
              <div className="drawer-label-main-amount" style={{ color: "red" }}>ĐÃ THANH TOÁN</div>
              <div>
                Số tiền: <FormatCurrency amount={dataRedeem.amount} />
              </div>
              <div>Ngày thanh toán: {formatDate(dataRedeem.date_redeem)}</div>
            </div>
          }
        </div>
        <hr />
        <div className="drawer-div">
          <div className="drawer-title">Chi tiết hồ sơ: </div>
          <div className="drawer-label-main">
            <ul>
              <li>
                <b>Tên sản phẩm:</b>{" "}
                {dataOrder.products && dataOrder.products.map((item, index) => (
                  <div key={index}>
                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
                    (
                    {item.DetailProducts &&
                      item.DetailProducts.map((detail, index) => {
                        // Make sure to return the JSX element here
                        return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
                      })}
                    ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
                  </div>
                ))}
              </li>
              <li>
                <b>Ngày bắt đầu:</b> {formatDate(dataOrder.order && dataOrder.order.OrderDate)}
              </li>
              <li>
                <b>Ngày hết hạn:</b> {formatDate(dataOrder.order && dataOrder.order.ReturnDate)}
              </li>
              <li>
                <b>Giá trị cầm: </b>
                <FormatCurrency amount={dataOrder.order && dataOrder.order.Price} />
              </li>
            </ul>
          </div>
        </div>
        <hr />
        <div className="drawer-div">
          <div className="drawer-title">Thông tin khách hàng </div>
          <div className="drawer-label-main">
            {/* <div  className="drawer-label-main-amount">Đóng lãi lần {index+1}</div> */}
            <ul>
              <li>
                <b>Mã khách hàng:</b> {dataOrder.order && dataOrder.order.CustomerID}
              </li>
              <li>
                <b>Tên khách hàng:</b> {dataCustomer.customer_name}
              </li>
              <li>
                <b>Ngày tháng năm sinh: </b>{" "}
                {dataCustomer.birth_date && formatDate(dataCustomer.birth_date)}
              </li>
              <li>
                <b>Số điện thoại: </b>
                {dataCustomer.phone}
              </li>
              <li>
                <b>CCCD:</b> {dataCustomer.cccd}
              </li>
              <li>
                <b>Hộ khẩu thường trú:</b> {dataCustomer.address}
              </li>
            </ul>
          </div>
        </div>
      </Drawer>
      {isOpenModal && (
        <ModalOrderPrintGold
          isOpen={isOpenModal}
          orderID={orderID}
          setIsOpen={setIsOpenModal}
        />
      )}
    </>
  );
};
export default DrawerDetail;

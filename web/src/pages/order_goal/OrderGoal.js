import { useEffect, useState } from "react";
import TheContent from "../../helpers/TheContent";
import {
  Button,
  ConfigProvider,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Select,
} from "antd";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import ModalCustomer from "../customer/ModalCustomer";
import ModalNewCustomer from "../customer/ModalNewCustomer";
import { ToastContainer, toast } from "react-toastify";
import dayjs from "dayjs";
import { FormatAmountFloatToInt, FormatAmountIntToFloat, FormatDate } from "../../helpers/Help";
import PrintOrderGold from "../modal/PrintOrderGold";
import TextArea from "antd/es/input/TextArea";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
const OrderGoal = () => {
  const [subMenu, setSubMenu] = useState(true);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [dataCustomer, setDataCustomer] = useState([]);
  const [dataNewCustomer, setDataNewCustomer] = useState([]);
  const [isSelect, setIsSelect] = useState(false);
  const [isSelectNew, setIsSelectNew] = useState(0);
  const [active, setActive] = useState(3);
  const [newCustomer, setNewCustomer] = useState(false);
  const isSubmit = isSelect || isSelectNew ? "true" : "fasle";
  const [dataCate, setDataCate] = useState([]);
  const [dataCateGoal, setDataCateGoal] = useState([]);
  const [dataCatePercent, setDataCatePercent] = useState([]);
  const [isSuccess, setIsSuccess] = useState(false);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [selectedDetailProducts, setSelectedDetailProducts] = useState([]);


  const [message, setMessage] = useState("");
  const [selectedDate, setSelectedDate] = useState(null);
  const originalDate = new Date();
  // Lấy các thông tin ngày, tháng, năm từ ngày ban đầu
  const day = originalDate.getDate();
  const month = originalDate.getMonth() + 1; // Tháng trong JavaScript bắt đầu từ 0, nên cần cộng thêm 1
  const year = originalDate.getFullYear();
  const [expandSidebar, setExpandSidebar] = useState(false)
  // Định dạng lại thành dd/mm/yyyy
  const formattedDate = `${year}-${month < 10 ? "0" + month : month}-${day < 10 ? "0" + day : day}`;

  const handleChangeDate = (date, dateString) => {
    const dateS = dayjs(date);
    const nextDay = dateS.add(1, "day");
    setSelectedDate(nextDay);
  };

  const convertNextMonth = (day) => {
    const date = dayjs(day);
    // Lấy ngày sau 1 tháng
    const nextMonth = date.add(1, "month");

    // Lấy thông tin ngày, tháng, năm từ ngày sau 1 tháng
    const nextMonthDay = nextMonth.date();
    const nextMonthMonth = nextMonth.month() + 1; // Tháng trong Day.js bắt đầu từ 0, nên cần cộng thêm 1
    const nextMonthYear = nextMonth.year();
    // Định dạng lại thành dd/mm/yyyy
    const nextMonthFormatted = `${nextMonthDay < 10 ? "0" + nextMonthDay : nextMonthDay
      }/${nextMonthMonth < 10 ? "0" + nextMonthMonth : nextMonthMonth
      }/${nextMonthYear}`;

    console.log(nextMonthFormatted);
    const dateS = dayjs(nextMonthFormatted, "DD/MM/YYYY");

    const isoString = dateS.toISOString();

    return isoString;
  };

  const [formData, setFormData] = useState({
    product_name: "",
    estimated_price: "",
    price: 0,
    weight_goal: "",
    order_date: moment(),
    return_date: moment().add(1, "month"),
    status: "0",
    interest_paid_count: 0,
    category_id: "",
    category_goal_id: "",
    percent: "",
    customer_id: "",
    note: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSelectPercent = (e) => {
    setFormData({ ...formData, percent: e });
  };

  const handleSubmit = () => {
    const productsValid =selectedProducts && selectedProducts.every(
      product => product.category_goal_id !== '' && product.weight_goal !== ''
    );

    
    // Validate price
    const isPriceValid = !isNaN(FormatAmountFloatToInt(formData.price));
    const isInterestRateValid = !isNaN(parseFloat(formData.percent));
    const allDetailsValid =selectedProducts && selectedProducts.every((_, index) =>
      selectedDetailProducts[index]?.every(
        detailProduct => detailProduct.category_id !== '' && detailProduct.count > 0
      )
    );

    if (!isInterestRateValid || !allDetailsValid || !productsValid || !isPriceValid || isSubmit != 'true' || selectedProducts.length === 0) {
      setMessage('Vui lòng kiểm tra thông tin nhập vào');
      return; // Prevent form submission
    }

    setMessage('');
    const products = selectedProducts.map((product, index) => ({
      product: {
        id_goal: product.gold,
        category_goal_id: product.category_goal_id,
        weight_goal: parseFloat(product.weight_goal),
      },
      detail_products:
        selectedDetailProducts &&
        selectedDetailProducts.length > 0 &&
        selectedDetailProducts[index].map((detailProduct) => ({
          category_id: detailProduct.category_id,
          count: detailProduct.count,
        })),
    }));
    const params = {
      order: {
        note: formData.note,
        product_name: formData.product_name,
        estimated_price: parseInt(FormatAmountFloatToInt(formData.estimated_price)),
        price: parseInt(FormatAmountFloatToInt(formData.price)),
        order_date: selectedDate == null ? formData.order_date : selectedDate,
        return_date:
          selectedDate == null
            ? formData.return_date
            : convertNextMonth(selectedDate),
        weight_goal: parseFloat(formData.weight_goal),
        status: formData.status,
        interest_paid_count: 0,
        percent: parseFloat(formData.percent),
        customer_id: isSelect
          ? dataCustomer.CustomerID
          : dataNewCustomer.customer_id,
      },
      products: products
    }
    BaseURL
      .post("create-order", params)
      .then((response) => {
        if (response.status === 200) {
          setIsSuccess(true);
        }
      })
      .catch((err) => toast("Thêm đơn thất bại!"));
  };
  const [amount, setAmount] = useState("");
  const handleAmountChange = (event) => {
    const value = event.target.value;
    console.log(value);
    const formattedValue = FormatAmountIntToFloat(value);
    setAmount(formattedValue);
    setFormData({ ...formData, price: value });
  };

  useEffect(() => {
    BaseURL
      .get("get-newest-customer")
      .then((response) => {
        if (response.status === 200) {
          setDataNewCustomer(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [isSelectNew]);

  useEffect(() => {
    BaseURL
      .get("list-category")
      .then((response) => {
        if (response.status === 200) {
          setDataCate(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-goal")
      .then((response) => {
        if (response.status === 200) {
          setDataCateGoal(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setDataCatePercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);

  const handleMinusCategoryDetail = (index, productIndex) => {
    setSelectedDetailProducts((prevDetailProducts) => {
      const updatedDetailProducts = [...prevDetailProducts];
      if (
        updatedDetailProducts[productIndex] &&
        updatedDetailProducts[productIndex][index]
      ) {
        updatedDetailProducts[productIndex] = updatedDetailProducts[productIndex].filter(
          (item, idx) => idx !== index
        );
      }
      return updatedDetailProducts;
    });
  };

  const handleAddProduct = () => {
    setSelectedProducts((prevProducts) => [
      ...prevProducts,
      { category_goal_id: '', weight_goal: '' },
    ]);
  };
  const handleMinusProduct = (index) => {
    setSelectedProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (index >= 0 && index < updatedProducts.length) {
        updatedProducts.splice(index, 1);
      }
      return updatedProducts;
    });
    // Xóa mảng loại hàng liên quan khi xóa loại vàng
    setSelectedDetailProducts((prevDetailProducts) => {
      const updatedDetailProducts = [...prevDetailProducts];
      if (index >= 0 && index < updatedDetailProducts.length) {
        updatedDetailProducts.splice(index, 1);
      }
      return updatedDetailProducts;
    });
  };

  const handleGoldChange = (value, index) => {
    setSelectedProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (updatedProducts[index]) {
        updatedProducts[index].category_goal_id = value;
      }
      return updatedProducts;
    });
  };
  const handleWeightGoldChange = (value, index) => {
    setSelectedProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (updatedProducts[index]) {
        updatedProducts[index].weight_goal = value;
      }
      return updatedProducts;
    });
  };

  const handleCategoryChange = (value, index, productIndex) => {
    setSelectedDetailProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (
        updatedProducts[productIndex] &&
        updatedProducts[productIndex][index]
      ) {
        updatedProducts[productIndex][index] = {
          ...updatedProducts[productIndex][index],
          category_id: value,
        };
      }
      return updatedProducts;
    });
  };

  const handleCountChange = (value, index, productIndex) => {
    setSelectedDetailProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (
        updatedProducts[productIndex] &&
        updatedProducts[productIndex][index]
      ) {
        updatedProducts[productIndex][index] = {
          ...updatedProducts[productIndex][index],
          count: value,
        };
      }
      return updatedProducts;
    });
  };
  const handleAddCategory = (productIndex) => {
    setSelectedDetailProducts((prevDetailProducts) => {
      const updatedDetailProducts = [...prevDetailProducts];
      updatedDetailProducts[productIndex] = [
        ...(updatedDetailProducts[productIndex] || []),
        { category_id: '', count: 1 },
      ];
      return updatedDetailProducts;
    });
  };

  const content = (
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </Button>
            </div>
            <a class="navbar-brand" href="javascript:;">
              THÊM ĐƠN VÀNG
            </a>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="row">
          <div class="col-md-10">
            <div class="card">
              <div class="card-header">
              <div className="button-customer">
                    {/* <Button
                      className="button-customer-list"
                      onClick={() => {
                        setIsOpenModal(true);
                        setIsSelectNew(false);
                      }}
                    >
                      Danh sách khách hàng
                    </Button> */}
                    <Button
                      className="button-customer-new"
                      onClick={() => {
                        setNewCustomer(true);
                        setIsSelect(false);
                      }}
                    >
                      Khách hàng mới
                    </Button>
                  </div>
                <Form className="form-add-pawn" autoSave="off" autoComplete="off">
                  {selectedProducts.map((product, productIndex) => (
                    <>
                      <div className="list-products" key={productIndex}>
                        <Form.Item className="form-add-pawn-item" label="Loại vàng">
                          <div className="category-order">
                            <Select
                              placeholder="Chọn loại"
                              className="form-input-select"
                              onChange={(value) => handleGoldChange(value, productIndex)}
                              value={product.category_goal_id}
                            >
                              {dataCateGoal.map((item) => (
                                <Select.Option value={item.CategoryID} key={item.CategoryID}>
                                  {item.CategoryName}
                                </Select.Option>
                              ))}
                            </Select>

                          </div>
                        </Form.Item>
                        <Form.Item className="form-add-pawn-item" label="Thêm loại hàng">
                          <Button
                            className="button-order-category"
                            onClick={() => handleAddCategory(productIndex)}
                          >
                            <i class="fa fa-plus" aria-hidden="true"></i>
                          </Button>
                        </Form.Item>
                        {selectedDetailProducts[productIndex]?.map((product, index) => (
                          <div className="list-products" key={index}>
                            <Form.Item className="form-add-pawn-item" label="Loại hàng">
                              <div className="category-order">
                                <Select
                                  className="form-input-select"
                                  onChange={(value) => handleCategoryChange(value, index, productIndex)}
                                  value={product.category_id}
                                  placeholder="Chọn loại"
                                >
                                  {dataCate.map((item) => (
                                    <Select.Option value={item.CategoryID} key={item.CategoryID}>
                                      {item.CategoryName}
                                    </Select.Option>
                                  ))}
                                </Select>
                                <InputNumber
                                  min={1}
                                  max={9999}
                                  defaultValue={1}
                                  className="form-input-number"
                                  value={product.count}
                                  onChange={(value) => handleCountChange(value, index, productIndex)}
                                />
                                <Button
                                  className="button-order-category"
                                  onClick={() => handleMinusCategoryDetail(index, productIndex)}
                                >
                                  <i class="fa fa-minus" aria-hidden="true"></i>
                                </Button>
                              </div>
                            </Form.Item>
                          </div>
                        ))}
                        <Form.Item className="form-add-pawn-item" label="Cân nặng">
                          <Input
                            className="form-input-pawn-weight"
                            value={product.weight_goal}
                            onChange={(e) => handleWeightGoldChange(e.target.value, productIndex)}
                            suffix="Chỉ"
                          />
                        </Form.Item>

                        <Button
                          className="button-remove"
                          onClick={() => {
                            handleMinusProduct(productIndex);
                          }}
                        >
                          <i class="fa fa-times" aria-hidden="true"></i>
                        </Button>
                      </div>
                    </>
                  ))}
                  <Form.Item className="form-add-pawn-item" label="Thêm loại vàng">
                    <Button
                      className="button-order-category"
                      onClick={handleAddProduct}
                    >
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </Button>

                  </Form.Item>

                  <Form.Item className="form-add-pawn-item" label="Giá cầm">
                    <Input
                      className="form-input-pawn"
                      name="price"
                      value={amount}
                      onChange={handleAmountChange}
                      suffix="VND"
                    />
                  </Form.Item>

                  <Form.Item className="form-add-pawn-item" label="Lãi suất">
                    <Select
                      placeholder="Chọn lãi xuất"
                      className="form-input-select"
                      onChange={handleSelectPercent}
                    >
                      {dataCatePercent.map((item) => (
                        <Select.Option value={item.PercentID}>
                          {item.Percent}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Ngày cầm"
                    name="date"
                  >
                    <ConfigProvider locale={viVN}>
                      <DatePicker
                        className="form-input-pawn"
                        defaultValue={dayjs(formattedDate, "YYYY-MM-DD")}
                        onChange={handleChangeDate}
                        locale={locale}
                      />
                    </ConfigProvider>
                  </Form.Item>
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Ghi chú"
                    name="note"
                  >
                    <TextArea
                      name="note"
                      onChange={handleChange}
                    />
                  </Form.Item>

                  {isSelect && (
                    <div className="customer-old">
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Tên khách hàng:
                        </div>
                        <div>{dataCustomer.CustomerName}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Ngày tháng năm sinh:
                        </div>
                        <div>
                          {dataCustomer.BirthDate && <FormatDate date={dataCustomer.BirthDate} />}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">Giới tính:</div>
                        <div>{dataCustomer.Gender == "" ? "" : dataCustomer.Gender == "0" ? "Nữ" : "Nam"}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">CCCD:</div>
                        <div>{dataCustomer.CCCD}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">SĐT:</div>
                        <div>{dataCustomer.Phone}</div>
                      </div>
                    </div>
                  )}
                  {isSelectNew > 0 && (
                    <div className="customer-old">
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Tên khách hàng:
                        </div>
                        <div>{dataNewCustomer.customer_name}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">
                          Ngày tháng năm sinh:
                        </div>
                        <div>{dataNewCustomer.birth_date && <FormatDate date={dataNewCustomer.birth_date} />}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">Giới tính:</div>
                        <div>
                          {dataNewCustomer.gender == "" ? "": dataNewCustomer.gender == "0" ? "Nữ" : "Nam"}
                        </div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">CCCD:</div>
                        <div>{dataNewCustomer.cccd}</div>
                      </div>
                      <div className="info-customer">
                        <div className="info-customer-label">SĐT:</div>
                        <div>{dataNewCustomer.phone}</div>
                      </div>
                    </div>
                  )}
                  <div className="message-fail-submit">{message}</div>
                  <div className="save-form-pawn">
                    <div class="update ml-auto mr-auto">
                      <button
                        onClick={() => {
                          handleSubmit();
                        }}
                        type="submit"
                        class="btn btn-primary btn-round"
                      >
                        Lưu đơn
                      </button>
                    </div>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  return (
    <>
      <TheContent
        content={content}
        active={active}
        setActive={setActive}
        checkSubMenuGoal={subMenu}
        isExpand={expandSidebar}
        setIsExpand={setExpandSidebar}
      />
      {isOpenModal && (
        <ModalCustomer
          setDataCustomer={setDataCustomer}
          isOpen={isOpenModal}
          setIsOpen={setIsOpenModal}
          setIsSelect={setIsSelect}
          setNewCustomer={setNewCustomer}
          setMessage={setMessage}
        />
      )}
      {newCustomer && (
        <ModalNewCustomer
          isOpenNewCus={newCustomer}
          setIsOpenNewCus={setNewCustomer}
          setIsSelect={setIsSelectNew}
          isSelect={isSelectNew}
          setMessage={setMessage}
        />
      )}
      {isSuccess && (
        <PrintOrderGold isOpen={isSuccess} setIsOpen={setIsSuccess} />
      )}

      <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
  );
};
export default OrderGoal;

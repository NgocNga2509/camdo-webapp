import { useEffect, useState, useRef } from "react";
import TheContent from "../../helpers/TheContent";
import {
  Button,
  ConfigProvider,
  DatePicker,
  Input,
  Pagination,
  Popover,
  Select,
  Table,
} from "antd";
import { Spin } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import BaseURL from "../../api/baseURL";
import locale from "antd/es/date-picker/locale/vi_VN";
import viVN from "antd/lib/locale/vi_VN";
import moment from "moment";
import PayGoal from "../pay/PayGoal";
import DrawerDetail from "./DrawerDetail";
import DrawerEditOrder from "./DrawerEditOrder";
import { ModalDelete } from "../modal/ModalDelete";
import { FormatCurrency, FormatDate } from "../../helpers/Help";
import RedeemOrderGoal from "../pay/RedeemOrderGoal";
import AddPaymentGoal from "../add/AddPaymentGoal";
import UpdateNote from "./UpdateNote";

const ListOrderGoal = () => {
  const [loading, setLoading] = useState(true);
  const [active, setActive] = useState(4);
  const [subMenu, setSubMenu] = useState(true);
  const [orderID, setOrderID] = useState("");
  const [data, setData] = useState([]);
  const [popoverContent, setPopoverContent] = useState({});
  const [modalPay, setModalPay] = useState(false);
  const [resetFlag, setResetFlag] = useState(1);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [openDrawerEdit, setOpenDrawerEdit] = useState(false);
  const [openModalPay, setOpenModalPay] = useState(false);
  const [openModalAddPayment, setOpenModalAddPayment] = useState(false);
  const [expandSidebar, setExpandSidebar] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [isUpdateNote, setIsUpdateNote] = useState(false);
  const [percent, setPercent] = useState([]);
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState(0);
  const [customerData, setCustomerData] = useState([]); // Add a state variable to store the customer name
  const [code, setCode] = useState("");
  const [isSearch, setIsSearch] = useState(false);
  const [datePickerKey, setDatePickerKey] = useState(0); // Initialize with 0

  const convertCustomer = (id) => {
    let name = "";
    customerData.map((item) => {
      if (item.CustomerID == id) {
        name = item.CustomerName;
      }
    });
    return name;
  };
  const fetchData = async (id) => {
    try {
      const response = await BaseURL.get(`list-customer/${id}`);
      const customerData = response.data;

      // Xây dựng nội dung popover từ kết quả API
      const content = (
        <div className="content-customer">
          <ul>
            <li>
              <b>Mã:</b> <div>{customerData.customer_id}</div>
            </li>
            <li>
              <b>CCCD:</b> <div>{customerData.cccd}</div>
            </li>
            <li>
              <b>SĐT:</b> <div>{customerData.phone}</div>
            </li>
            <li>
              <b>Địa chỉ:</b>
              <div>{customerData.address}</div>{" "}
            </li>
          </ul>
        </div>
      );

      // Cập nhật nội dung popover
      setPopoverContent({ [id]: content });
    } catch (error) {
      console.error("Error fetching customer data:", error);
    }
  };

  const isOverdue = (returnDate) => {
    const currentDate = new Date();
    const returnDateObj = new Date(returnDate);
    const diffInDays = Math.floor(
      (currentDate - returnDateObj) / (1000 * 60 * 60 * 24)
    );
    const diffInDays2 = Math.floor(
      (currentDate - returnDateObj.setMonth(returnDateObj.getMonth() - 1)) /
        (1000 * 60 * 60 * 24)
    );
    if (diffInDays > 0 && diffInDays2 >= 60) {
      return 2;
    } else if (diffInDays > 0) {
      return 1;
    } else {
      return 0;
    }
  };

  useEffect(() => {
    BaseURL.get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setPercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL.get("list-customer")
      .then((response) => {
        if (response.status === 200) {
          setCustomerData(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);

  const handleSelectAction = (e, id) => {
    const params = {
      id: id,
      status: "2",
    };
    if (e === 0) {
      setOrderID(id);
      setOpenDrawer(true);
    } else if (e === 1) {
      setOrderID(id);
      setOpenDrawerEdit(true);
    } else if (e === 2) {
      setOrderID(id);
      setOpenModalDelete(true);
    } else if (e === 3) {
      BaseURL.put("payment/update-status-order-goal", params).then(
        (response) => {
          setResetFlag(resetFlag + 1);
        }
      );
    }
  };

  const convertPercent = (cate) => {
    let name = "";
    percent.map((item) => {
      if (item.PercentID == cate) {
        name = item.Percent;
      }
    });
    return name;
  };

  const convertStatus = (status) => {
    let stt = "";
    if (status === "0") {
      stt = (
        <span style={{ color: "green", fontWeight: "bold" }}>
          Còn hiệu lực{" "}
        </span>
      );
    } else if (status === "1") {
      stt = (
        <span style={{ color: "#86518a", fontWeight: "bold" }}>
          Đã chuộc tài sản
        </span>
      );
    } else {
      stt = (
        <span style={{ color: "red", fontWeight: "bold" }}>Hồ sơ hóa giá</span>
      );
    }
    return stt;
  };
  const handleDelete = (id) => {
    BaseURL.delete(`delete-order-goal/${id}`).then((response) => {
      if (response.status === 200) {
        setResetFlag(resetFlag + 1);
        setOpenModalDelete(false);
      }
    });
  };

  const renderReturnDate = (interests, record) => {
    return (
      <div>
        {record.Status == "0" && (
          <ul className="ul-payments-info">
            {record.Payments && record.Payments != null ? (
              <li>
                <div className="li-date">
                  <FormatDate
                    date={
                      record.Payments[record.Payments.length - 1]
                        .NextPaymentDate
                    }
                  />
                  {isOverdue(
                    record.Payments[record.Payments.length - 1].NextPaymentDate
                  ) === 2 ? (
                    <div>Hóa giá</div>
                  ) : isOverdue(
                      record.Payments[record.Payments.length - 1]
                        .NextPaymentDate
                    ) ? (
                    <div>Trễ hạn</div>
                  ) : (
                    <div></div>
                  )}
                </div>
              </li>
            ) : (
              <li>
                <div className="li-date">
                  <FormatDate date={record.ReturnDate} />
                  {isOverdue(record.ReturnDate) === 2 ? (
                    <div>Hóa giá</div>
                  ) : isOverdue(record.ReturnDate) === 1 ? (
                    <div>Trễ hạn</div>
                  ) : (
                    <div></div>
                  )}
                </div>
              </li>
            )}
          </ul>
        )}
      </div>
    );
  };
  const renderNote = (note, record) => {
    return (
      <div className="note">
        <div>
          {note != null && (
            <div className="note-content">
              <div>{record.Miss == 1 && "Khách mất giấy"}</div>
              {note}
            </div>
          )}
          <div className="note-update">
            <Button
              onClick={() => {
                setOrderID(record.ID);
                setIsUpdateNote(true);
              }}
            >
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </Button>
          </div>
        </div>
      </div>
    );
  };
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "ID",
      key: "ID",
      width: 200,
      render: (id, record) => (
        <div className="text-bold">
          {record.STT}-{record.Code}
        </div>
      ),
    },
    {
      title: "Tên khách hàng",
      dataIndex: "CustomerID",
      key: "CustomerID",
      render: (id) => {
        return (
          <div className="info-id-customer">
            <div className="info-id-customer-id">{convertCustomer(id)}</div>
            <div className="info-id-customer-icon">
              <Popover
                content={popoverContent[id]}
                trigger="hover"
                title="Thông tin chi tiết khách hàng:"
              >
                <InfoCircleOutlined onMouseEnter={() => fetchData(id)} />
              </Popover>
            </div>
          </div>
        );
      },
    },
    {
      title: "Thông tin sản phẩm",
      dataIndex: "CategoryID",
      key: "CategoryID",
      width: 400,
      // responsive: ["xl", "lg", "md", "sm"],
      render: (e) =>
        e &&
        e.map((item, index) => (
          <div key={index}>
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> (
            {item.DetailProducts &&
              item.DetailProducts.map((detail, index) => {
                // Make sure to return the JSX element here
                return (
                  <span key={index}>
                    {detail.Count} {detail.CategoryName}
                    {index < item.DetailProducts.length - 1 && " + "}
                  </span>
                );
              })}
            ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
          </div>
        )),
    },
    {
      title: "Giá cầm",
      dataIndex: "Price",
      key: "Price",
      width: 250,
      render: (amount, record) => (
        <div className="add-payment">
          <div className="add-payment-new-amount">
            <FormatCurrency amount={record.Price} />
          </div>
          {record.AddPayment != null && (
            <div className="add-payment-old-amount">
              Tiền gốc: <FormatCurrency amount={record.PriceOld} />
            </div>
          )}
          {record.AddPayment &&
            record.AddPayment.map((item) => (
              <div>
                Cầm thêm <FormatCurrency amount={item.Amount} /> ngày{" "}
                <FormatDate date={item.DateAddPayment} />
              </div>
            ))}
        </div>
      ),
    },
    {
      title: "Lãi suất",
      dataIndex: "Percent",
      key: "Percent",
      render: (percent) => convertPercent(percent),
    },
    {
      title: "Trạng thái hồ sơ",
      dataIndex: "Status",
      key: "Status",
      width: 250,
      render: (stt) => (
        <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
      ),
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Thông tin đóng lãi",
      dataIndex: "Payments",
      key: "Payments",
      render: (item, record) => (
        <div>
          {record.Status == "0" && (
            <ul className="ul-payments-info">
              {record.Payments && record.Payments != null ? (
                record.Payments.map((interest) => (
                  <li key={interest.ID}>
                    <div className="li-date">
                      <FormatDate date={interest.PaymentDate} />
                    </div>
                    <div className="li-amount">
                      <FormatCurrency amount={interest.Amount} />{" "}
                    </div>
                  </li>
                ))
              ) : (
                <li>Chưa đóng lãi</li>
              )}
            </ul>
          )}
        </div>
      ),
    },
    {
      title: "Ngày tới hạn",
      dataIndex: "Payments",
      key: "Payments",
      render: (item, record) => renderReturnDate(item, record),
    },
    {
      title: "Ghi chú",
      dataIndex: "Note",
      key: "Note",
      width: 200,
      render: (note, record) => renderNote(note, record),
    },
    {
      title: "",
      dataIndex: "ID",
      key: "ID",
      render: (id, record) => (
        <div className="button-action">
          {record.Status != "1" && (
            <>
              <Button
                className="button-action-detail"
                onClick={() => {
                  setOrderID(id);
                  setModalPay(true);
                }}
              >
                Đóng lãi
              </Button>

              <Button
                className="button-action-payment"
                onClick={() => {
                  setOrderID(id);
                  setOpenModalPay(true);
                }}
              >
                Thanh toán
              </Button>
              <Button
                className="button-action-payment"
                onClick={() => {
                  setOrderID(id);
                  setOpenModalAddPayment(true);
                }}
              >
                Cầm thêm
              </Button>
            </>
          )}
          <Select
            defaultValue="Thao tác"
            className="select-search-action"
            onChange={(e) => handleSelectAction(e, id)}
          >
            <Select.Option value={0}>Chi tiết</Select.Option>
            <Select.Option value={1}>Sửa</Select.Option>
            <Select.Option value={2}>Xóa</Select.Option>
            <Select.Option value={3}>Hóa giá</Select.Option>
          </Select>
        </div>
      ),
    },
  ];

  const dataSource =
    data &&
    data.map((item, index) => ({
      key: index,
      ID: item.order.ID,
      CustomerID: item.order.CustomerID,
      ProductName: item.order.ProductName,
      CategoryID: item.products && item.products,
      AddPayment: item.add_payments,
      Payments: item.payments,
      EstimatedPrice: item.order.EstimatedPrice,
      Price: item.order.Price,
      PriceOld: item.order.PriceOld,
      ReturnDate: item.order.ReturnDate,
      Note: item.order.Note,
      Miss: item.order.Miss,
      WeightGoal: item.order.WeightGoal,
      Status: item.order.Status,
      Percent: item.order.Percent,
      OrderDate: item.order.OrderDate,
      Code: item.order.Code,
      STT: item.order.STT,
      interestAmount: item.interestAmount,
    }));

  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  const pageSize = 8; // Kích thước trang
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const handleChangeFilter = (value) => {
    setFilteredData(value);
  };

  const filterMiss = (data) => {
    if (filteredData === 1) {
      return data.filter((item) => item && item.Miss === 1);
    } else {
      return data;
    }
  };

  const currentPageData =
    dataSource &&
    dataSource.slice((currentPage - 1) * pageSize, currentPage * pageSize);

  const fetchDataSearch = async () => {
    setLoading(true);
    try {
      const response = await BaseURL.post("search-order-goal-stt", {
        stt: parseInt(search),
        code: code,
        miss: parseInt(filteredData),
      });
      console.log(response);
      const dataFromApi = response.data;
      setData(dataFromApi);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const fetchDataPage = async () => {
    setLoading(true);
    try {
      const response = await BaseURL.get("get-order-goal-add-products");

      const dataFromApi = response.data;
      setData(dataFromApi);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const getRowClassName = (record, index) => {
    return index % 2 === 0 ? "even-row" : "odd-row";
  };

  useEffect(() => {
    isSearch == false && filteredData == 0 ? fetchDataPage() : fetchDataSearch();
    setCurrentPage(1);
  }, [resetFlag, isSearch, filteredData]);
  // useEffect(() => {
  //   fetchDataPage()
  // }, [resetFlag, filteredData]);
  const handleChangeMonth = (dateString) => {
    if (dateString == null) {
      setCode("");
    } else {
      const date = new Date(dateString);
      const year = date.getFullYear().toString().slice(-2); // Lấy hai chữ số cuối của năm
      const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Đảm bảo tháng luôn có hai chữ số
      const code = month + year;
      setCode(code);
    }
  };

  const handleReset = () => {
    setSearch("");
    setCode("");
    setFilteredData(0);
    setIsSearch(false);
    setDatePickerKey(datePickerKey + 1);
    setResetFlag(resetFlag + 1);
  };

  const content = (
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <Button
                type="button"
                class="navbar-toggler"
                onClick={() => setExpandSidebar(true)}
              >
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </Button>
            </div>
            <a class="navbar-brand" href="javascript:;">
              QUẢN LÝ ĐƠN
            </a>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div className="d-flex-column search-customer">
                  <div className="d-flex">
                    <div className="picker-list-order-date">
                      <ConfigProvider locale={viVN}>
                        <DatePicker
                          key={datePickerKey}
                          picker="month"
                          onChange={handleChangeMonth}
                          format={"MM/YYYY"}
                          locale={locale}
                        />
                      </ConfigProvider>
                    </div>
                    <div className="d-flex">
                      <Input
                        type="text"
                        className="form-control"
                        placeholder="Tìm kiếm..."
                        value={search}
                        onInput={(e) => setSearch(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.key === "Enter") {
                            setIsSearch(true);
                            fetchDataSearch();
                          }
                        }}
                      />
                      <div className="input-group-append">
                        <div
                          className="input-group-text"
                          onClick={() => {
                            setIsSearch(true);
                            fetchDataSearch();
                          }}
                        >
                          <i class="nc-icon nc-zoom-split"></i>
                        </div>
                      </div>
                      <div className="input-group-append reset">
                        <div
                          className="input-group-text"
                          onClick={() => {
                            handleReset();
                          }}
                        >
                          <i class="fa fa-refresh" aria-hidden="true"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Select
                    className="select-search"
                    defaultValue={0}
                    onChange={handleChangeFilter}
                    value={filteredData}
                  >
                    <Select.Option value={0}>Tất cả</Select.Option>
                    <Select.Option value={1}>Khách mất giấy</Select.Option>
                  </Select>
                </div>
              </div>
              <div class="card-body">
                {loading ? (
                  <Spin size="large" /> // Hiển thị loading khi loading = true
                ) : (
                  <div class="table-responsive">
                    <Table
                      columns={columns}
                      dataSource={currentPageData}
                      pagination={false}
                      rowClassName={getRowClassName}
                    />
                    <Pagination
                      showSizeChanger={false}
                      className="pagination-category"
                      current={currentPage}
                      pageSize={pageSize}
                      total={totalItems}
                      onChange={handlePageChange}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  return (
    <>
      <TheContent
        content={content}
        active={active}
        setActive={setActive}
        checkSubMenuGoal={subMenu}
        resetFlag={resetFlag}
        isExpand={expandSidebar}
        setIsExpand={setExpandSidebar}
      />
      {modalPay && (
        <PayGoal
          isOpen={modalPay}
          setIsOpen={setModalPay}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalAddPayment && (
        <AddPaymentGoal
          isOpen={openModalAddPayment}
          setIsOpen={setOpenModalAddPayment}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalPay && (
        <RedeemOrderGoal
          isOpen={openModalPay}
          setIsOpen={setOpenModalPay}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openDrawer && (
        <DrawerDetail
          open={openDrawer}
          setOpen={setOpenDrawer}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openDrawerEdit && (
        <DrawerEditOrder
          isOpen={openDrawerEdit}
          setIsOpen={setOpenDrawerEdit}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalDelete && (
        <ModalDelete
          isOpen={openModalDelete}
          setIsOpen={setOpenModalDelete}
          handleDelete={handleDelete}
          id={orderID}
        />
      )}
      {isUpdateNote && (
        <UpdateNote
          isOpen={isUpdateNote}
          setIsOpen={setIsUpdateNote}
          id={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
    </>
  );
};
export default ListOrderGoal;

import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";
import { Spin } from "antd";

const CollectTable = ({ month, year, isTable, setIsTable, isPrint }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    const params = {
      month: month,
      year: year,
    };
    BaseURL
      .post(
        isTable == 0
          ? "payment/get-by-month-and-year"
          : "redeem/get-by-month-and-year",
        params
      )
      .then((response) => {
        setData(response.data);
        setLoading(false);
      });
  }, [month, year, isTable]);

  const dataSource =
    isTable == 0
      ? data && data.map((item, index) => ({
        key: index,
        ID: item.payments && item.payments.payment_id,
        Amount: item.payments && item.payments.amount,
        OrderID: item.payments && item.payments.order_id,
        PaymentDate: item.payments && item.payments.payment_date,
        Products: item.payments && item.products,
        OrderDate: item.order && item.order.OrderDate,
        Code: item.order && item.order.Code,
        STT: item.order && item.order.STT,
      }))
      : data && data.map((item, index) => ({
        key: index,
        ID: item.redeem && item.redeem.id,
        Amount: item.redeem && item.redeem.amount,
        OrderID: item.redeem && item.redeem.order_id,
        PaymentDate: item.redeem && item.redeem.date_redeem,
        Products: item.redeem && item.products,
        OrderDate: item.order && item.order.OrderDate,
        Code: item.order && item.order.Code,
        STT: item.order && item.order.STT,
      }));
  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const pageSize = isPrint == 1 ? totalItems : 3; // Kích thước trang

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource && dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "OrderID",
      key: "OrderID",
      render: (id, record) => <div className="text-bold">{record.STT}-{record.Code}</div>
    },
    {
      title: "Sản phẩm",
      dataIndex: "Products",
      key: "Products",
      render: (product) =>
        product &&
        product.map((item, index) => (
          <div key={index}>
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
            (
            {item.DetailProducts &&
              item.DetailProducts.map((detail, index) => {
                // Make sure to return the JSX element here
                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
              })}
            ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
          </div>
        ))
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Ngày đóng lãi/thanh toán",
      dataIndex: "PaymentDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Số tiền",
      dataIndex: "Amount",
      key: "Amount",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
  ];

  return (
    <div className="table-container">
      {loading ? (
        <Spin size="large" /> // Hiển thị loading khi loading = true
      ) : (
        <>
          < Table
            dataSource={currentPageData}
            columns={columns}
            pagination={false}
          />
          {isPrint != 1 &&
            <Pagination showSizeChanger={false} 
              className="pagination-category"
              current={currentPage}
              pageSize={pageSize}
              total={totalItems}
              onChange={handlePageChange}
            />
          }
        </>
      )}
    </div>
  );
};

export default CollectTable;

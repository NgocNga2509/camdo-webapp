import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";

const AddPaymentTable = ({ month, year, isTable, setIsTable, isPrint }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const params = {
      month: month,
      year: year,
    };
    BaseURL
      .post(
        isTable == 0
          ? "payment/get-by-month-and-year"
          : "redeem/get-by-month-and-year",
        params
      )
      .then((response) => {
        setData(response.data);
      });
  }, [month, year, isTable]);

  const dataSource =
    isTable == 0
      ? data && data.map((item, index) => ({
        key: index,
        ID: item.payment_id,
        Amount: item.amount,
        OrderID: item.order_id,
        PaymentDate: item.payment_date,
      }))
      : data && data.map((item, index) => ({
        key: index,
        ID: item.payment_id,
        Amount: item.amount,
        OrderID: item.order_id,
        PaymentDate: item.date_redeem,
      }));
  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const pageSize = isPrint == 1 ? totalItems : 3; // Kích thước trang

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource && dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "OrderID",
      key: "OrderID",
    },
    {
      title: "Ngày đóng lãi/thanh toán",
      dataIndex: "PaymentDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Số tiền",
      dataIndex: "Amount",
      key: "Amount",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
  ];

  return (
    <div className="table-container">
      <Table
        dataSource={currentPageData}
        columns={columns}
        pagination={false}
      />
      {isPrint != 1 &&
        <Pagination showSizeChanger={false} 
          className="pagination-category"
          current={currentPage}
          pageSize={pageSize}
          total={totalItems}
          onChange={handlePageChange}
        />
      }
    </div>
  );
};

export default AddPaymentTable;

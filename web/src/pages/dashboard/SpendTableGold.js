import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";
import { Spin } from "antd";

const SpendTableGold = ({ month, year, isPrint }) => {
    const [combinedData, setCombinedData] = useState([]);
    const [orderData, setOrderData] = useState([]);
    const [paymentData, setPaymentData] = useState([]);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        setLoading(true)
        const fetchOrderData = async () => {
            try {
                const params = {
                    month: month,
                    year: year,
                };
                const orderDataResponse = await BaseURL.post("get-order-goal-by-month-and-year", params);
                setOrderData(orderDataResponse.data);
                setLoading(false)
            } catch (error) {
                console.error("Error fetching order data:", error);
            }
        };

        const fetchPaymentData = async () => {
            try {
                const params = {
                    month: month,
                    year: year,
                };
                const paymentDataResponse = await BaseURL.post("add-payment/get-by-month-and-year", params);
                setPaymentData(paymentDataResponse.data);
                setLoading(false)
            } catch (error) {
                console.error("Error fetching payment data:", error);
            }
        };

        fetchOrderData();
        fetchPaymentData();
    }, [month, year]);

    useEffect(() => {
        const combineData = (orderData, paymentData) => {
            const combinedData = [];
            const orderIdsSet = new Set(orderData && orderData.map((order) => order.order.ID));

            orderData && orderData.forEach((order) => {
                const matchingPayment = paymentData && paymentData.find((payment) => payment.add_payment.id_gold === order.order.ID);
                if (matchingPayment) {
                    combinedData.push({
                        id: order.order.ID,
                        price: order.order.Price,
                        date: order.order.OrderDate,
                        code: order.order.Code,
                        stt: order.order.STT,
                        price_old: order.order.PriceOld,
                        products: order.products,
                        note: 0
                    });
                } else {
                    // Nếu không có "id" trùng nhau, lấy dữ liệu từ API "add-payment/get-by-month-and-year"
                    combinedData.push({
                        id: order.order.ID,
                        price: order.order.Price,
                        date: order.order.OrderDate,
                        price_old: order.order.PriceOld,
                        code: order.order.Code,
                        stt: order.order.STT,
                        products: order.products,
                        note: 0
                    });
                }
            });

            // Lấy dữ liệu từ API "add-payment/get-by-month-and-year" cho các "id" chưa có trong combinedData
            paymentData && paymentData.forEach((payment) => {
                if (!orderIdsSet.has(payment.add_payment.id_gold)) {
                    combinedData.push({
                        id: payment.add_payment.id_gold,
                        price: payment.add_payment.amount, // Đặt giá trị order_goal là null vì không tìm thấy dữ liệu từ orderData
                        date_payment: payment.add_payment.date_add_payment,
                        code: payment.order.Code,
                        stt: payment.order.STT,
                        products: payment.products,
                        price_old: null,
                        note: 1
                    });
                }
            });

            return combinedData;
        };
        // Call the combineData function and set the state with the combined data
        const combinedData = combineData(orderData, paymentData);
        setCombinedData(combinedData);
    }, [orderData, paymentData]);


    const dataSource = combinedData && combinedData.map((item, index) => ({
        key: index,
        ID: item.id,
        Price: item.price,
        OrderDate: item.date,
        DateAddPayment: item.date_payment,
        PriceOld: item.price_old,
        Note: item.note,
        Products: item.products,
        Code: item.code,
        STT: item.stt,
    }));
    const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
    // Số lượng tổng cộng các mục dữ liệu
    const totalItems = dataSource && dataSource.length;
    const pageSize = isPrint == 1 ? totalItems : 3; // Kích thước trang

    const handlePageChange = (page) => {
        setCurrentPage(page);
    };
    // Lấy dữ liệu trang hiện tại
    const currentPageData = dataSource && dataSource.slice(
        (currentPage - 1) * pageSize,
        currentPage * pageSize
    );
    const columns = [
        {
            title: "Mã đơn",
            dataIndex: "ID",
            key: "ID",
            render: (id, record) => <div className="text-bold">{record.STT}-{record.Code}</div>
        },
        {
            title: "Sản phẩm",
            dataIndex: "Products",
            key: "Products",
            render: (products) => (<>
                {products && products.map((item, index) => (
                    <div key={index}>
                        <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
                        (
                        {item.DetailProducts &&
                            item.DetailProducts.map((detail, index) => {
                                // Make sure to return the JSX element here
                                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
                            })}
                        ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
                    </div>
                ))}
            </>),
        },
        {
            title: "Ngày cầm",
            dataIndex: "OrderDate",
            key: "OrderDate",
            render: (date) => moment(date).format("DD/MM/YYYY"),
        },
        {
            title: "Giá cầm",
            dataIndex: "Price",
            key: "Price",
            render: (amount) => <FormatCurrency amount={amount} />,
        },
        {
            title: "Ghi chú",
            dataIndex: "Note",
            key: "Note",
            render: (note, record) => {
                let price = record.Price - record.PriceOld
                if (note == 1) {
                    return <div className="add-payment-note">Cầm thêm</div>
                }
                if (record.PriceOld) {
                    return <div className="add-payment-note">
                        {price > 0 && <>Có cầm thêm <FormatCurrency amount={price} /></>}
                    </div>
                }
            }
        },
    ];
    return (
        <div className="table-container">
            {loading ? (
                <Spin size="large" />
            ) : (
                <>
                    <Table dataSource={currentPageData} columns={columns} pagination={false} />
                    {isPrint != 1 &&
                        <Pagination showSizeChanger={false} 
                            className="pagination-category"
                            current={currentPage}
                            pageSize={pageSize}
                            total={totalItems}
                            onChange={handlePageChange}
                        />
                    }
                </>
            )}


        </div>
    );
};
export default SpendTableGold;

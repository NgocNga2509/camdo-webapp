import { useEffect, useState } from "react";
import TheContent from "../../helpers/TheContent";
import { Button } from "antd";
import { Spin } from "antd";
import BaseURL from "../../api/baseURL";
import CategoryProduct from "./CategoryProduct";
import CategoryGoal from "./CategoryGoal";
import CategoryPercent from "./CategoryPercent";

const Category = () => {
  const [loading, setLoading] = useState(true);
  const [subMenu, setSubMenu] = useState(false);
  const [active, setActive] = useState(9);
  const [data, setData] = useState([]);
  const [dataGoal, setDataGoal] = useState([]);
  const [dataPercent, setDataPercent] = useState([]);
  const [expandSidebar, setExpandSidebar] = useState(false)

  const [resetFlag, setResetFlag] = useState(1);
  const [categoryName, setCategoryName] = useState("");

  const dataSource = data.map((item, index) => ({
    key: index,
    CategoryID: item.CategoryID,
    CategoryName: item.CategoryName,
  }));

  const dataSourceGoal = dataGoal.map((item, index) => ({
    key: index,
    CategoryID: item.CategoryID,
    CategoryName: item.CategoryName,
  }));


  const handleSubmit = () => {
    const param = {
      category_name: categoryName,
    };
    BaseURL
      .post("create-category", param)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
        }
      });
  };

  const handleSubmitGoal = () => {
    const param = {
      category_name: categoryName,
    };
    BaseURL
      .post("create-category-goal", param)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
        }
      });
  };

  useEffect(() => {
    setLoading(true);
    BaseURL
      .get("list-category")
      .then((response) => {
        if (response.status === 200) {
          setData(response.data);
          setLoading(false)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-goal")
      .then((response) => {
        if (response.status === 200) {
          setDataGoal(response.data);
          setLoading(false)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setDataPercent(response.data);
          setLoading(false)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [resetFlag]);

  const content = (
    <>
      <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div class="container-fluid">
            <div class="navbar-wrapper">
              <div class="navbar-toggle">
                <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                  <span class="navbar-toggler-bar bar1"></span>
                  <span class="navbar-toggler-bar bar2"></span>
                  <span class="navbar-toggler-bar bar3"></span>
                </Button>
              </div>
              <a class="navbar-brand" href="javascript:;">
                DANH MỤC
              </a>
            </div>
          </div>
        </nav>
        <div class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  {/* <h4 class="card-title">DANH MỤC HÀNG</h4> */}
                </div>
                <div class="card-body">
                  {loading ? (
                    <Spin size="large" /> // Hiển thị loading khi loading = true
                  ) :
                    <div className="category">

                      <CategoryProduct
                        setCategoryName={setCategoryName}
                        handleSubmit={handleSubmit}
                        dataSource={dataSource}
                        resetFlag={resetFlag}
                        setResetFlag={setResetFlag}
                      />
                      <CategoryGoal
                        setCategoryName={setCategoryName}
                        handleSubmitGoal={handleSubmitGoal}
                        dataSourceGoal={dataSourceGoal}
                        resetFlag={resetFlag}
                        setResetFlag={setResetFlag}
                      />
                      <CategoryPercent
                        dataSource={dataPercent}
                        resetFlag={resetFlag}
                        setResetFlag={setResetFlag}
                      />
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
  return (
    <TheContent
      checkSubMenu={subMenu}
      content={content}
      setSubMenu={setSubMenu}
      active={active}
      setActive={setActive}
      isExpand={expandSidebar}
      setIsExpand={setExpandSidebar}
    />
  );
};
export default Category;

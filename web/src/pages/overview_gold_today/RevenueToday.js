import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";

const RevenueToday = ({ isOrder }) => {
  const [dataCollect, setDataCollect] = useState([]);
  useEffect(() => {
    BaseURL
      .get(isOrder == 0 && "overview/payments-gold-today")
      .then((response) => {
        setDataCollect(response.data);
      });
  }, []);

  const dataSource = dataCollect && dataCollect.map((item, index) => ({
    key: index,
    ID: item.payments.payment_id,
    Amount: item.payments.amount,
    OrderID: item.payments.order_id,
    PaymentDate: item.payments.payment_date,
    Products: item.products,
    OrderDate: item.order.OrderDate,
    Code: item.order.Code,
    STT: item.order.STT
  }));

  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const pageSize = 3; // Kích thước trang

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource && dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "OrderID",
      key: "OrderID",
      render: (id, record) => <div className="text-bold">{record.STT}-{record.Code}</div>
    },
    {
      title: "Sản phẩm",
      dataIndex: "Products",
      key: "Products",
      render: (product) =>
      product &&
      product.map((item, index) => (
          <div key={index}>
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
            (
            {item.DetailProducts &&
              item.DetailProducts.map((detail, index) => {
                // Make sure to return the JSX element here
                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
              })}
            ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
          </div>
        ))
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Ngày đóng lãi",
      dataIndex: "PaymentDate",
      key: "PaymentDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Số tiền",
      dataIndex: "Amount",
      key: "Amount",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
  ];

  return (
    <div className="table-container">
      <Table
        dataSource={currentPageData}
        columns={columns}
        pagination={false}
      />
      <Pagination showSizeChanger={false} 
        className="pagination-category"
        current={currentPage}
        pageSize={pageSize}
        total={totalItems}
        onChange={handlePageChange}
      />

    </div>
  );
};

export default RevenueToday;

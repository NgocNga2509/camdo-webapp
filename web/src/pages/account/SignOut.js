import { useNavigate } from "react-router-dom";

const clearToken = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('name');
};

const SignOut = () => {
    
    clearToken();
   window.location.href = '/#/sign-in';
};

export default SignOut

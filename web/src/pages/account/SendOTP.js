import { Form, Input, Modal } from "antd"
import { useEffect, useState } from "react"
import BaseURL from "../../api/baseURL"
import ResetPassword from "./ResetPassword";
import { ToastContainer, toast } from "react-toastify";

const SendOTP = ({ isOpen, setIsOpen }) => {
    const [otp, setOTP] = useState(0);
    const [resetPass, setResetPass] = useState(false)
    const [err, setErr] = useState(false)
    const handleCancel = () => {
        setIsOpen(false)
    }
    const handleChangeOTP = (otp) => {
        setOTP(otp)
    }
    const handleSubmit = () => {
        const param = {
            "otp": otp
        }
        BaseURL.post("check-otp", param).then((response) => {
            if (response.status === 200) {
                setResetPass(true)
            }
        }).catch((err) => toast("OTP sai"))
    }
    useEffect(() => {
        try {
            BaseURL.get("send-otp", { timeout: 5000 }); 
        } catch (error) {
            if (error.response) {
                console.log("Server responded with a non-2xx status");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log("No response received from the server");
                console.log(error.request);
            } else {
                console.log("Error setting up the request");
                console.log(error.message);
            }
            console.log(error.config);
        }
    }, []);
    
    
    return (
        <>
            <Modal open={isOpen} onCancel={handleCancel} onOk={handleSubmit}>
                <Form className="form-add-pay" autoSave="off" autoComplete="off">
                    <div className="title-otp-modal">OTP ĐÃ ĐƯỢC GỬI ĐẾN EMAIL CỦA BẠN, HÃY NHẬP OTP ĐỂ THAY ĐỔI MẬT KHẨU</div>
                    <Form.Item className="form-add-pay-item" label="OTP">
                        <Input
                            className="form-input-pay"
                            name="otp"
                            onChange={(e) => handleChangeOTP(e.target.value)}
                        />
                    </Form.Item>
                </Form>
            </Modal>
            {resetPass && <ResetPassword isOpen={resetPass} setIsOpen={setResetPass} />}
            <ToastContainer autoClose={1000} theme="dark" position="top-center" />
        </>
    )
}

export default SendOTP
import { Button, Modal } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import {
    FormatCurrency,
    FormatDate,
    NumberToWords,
} from "../../helpers/Help";

const PrintOrderGold = ({ isOpen, setIsOpen }) => {
    const contentRef = useRef(null);
    const [dataOrder, setDataOrder] = useState([]);
    const [dataCustomer, setDataCustomer] = useState([]);
    const [percentID, setPercentID] = useState("");
    const [percent, setPercent] = useState("");

    const handleCancel = () => {
        setIsOpen(false);
    };

    const handleOk = () => {
        setIsOpen(false);
        const contentToPrint = contentRef.current.innerHTML;
        // Sử dụng nội dung để in
        const printWindow = window.open("", "_blank");
        printWindow.document.open();
        const userAgent = navigator.userAgent;

        // Sử dụng biểu thức chính quy để kiểm tra User-Agent
        if (/Mobi|Android/i.test(userAgent)) {
            printWindow.document.write(
                "<html><head><style>" +
                getPrintStylesMobile() +
                ".img-container { position: absolute; top: 0; right: 0; }" +
                "body {margin: 0;}" +
                "* {margin: 0;padding: 0;}" +
                /* Các kiểu in dành riêng cho thiết bị di động */
                ".order-id-line-customer { font-size:22px;position: absolute; top: 40; left: 0; }" +
                // Thay đổi kích thước giấy in thành A5
                "</style></head><body>" +
                '<div class="img-container"><img src="./KH.png" alt="Mô tả ảnh" width="170px" /></div>' + // Thêm container chứa ảnh và thuộc tính width để điều chỉnh kích thước ảnh
                // '<img src="https://hungphatsaigon.vn/wp-content/uploads/2022/07/10_hinh-nen-gau-cute.jpg" alt="Mô tả ảnh" width="100px"/>' + // Thêm đoạn mã HTML để chèn ảnh vào
                contentToPrint +
                "</body></html>"
            );
        } else {
            printWindow.document.write(
                "<html><head><style>" +
                getPrintStyles() +
                ".img-container { position: absolute; top: 0; right: 0; }" +
                "@media print {" +
                "@page {" +
                "size: A5;" +
                "}" +
                ".order-id-line-customer { position: absolute; top: 40; left: 0; }" +

                // Thay đổi kích thước giấy in thành A5
                "</style></head><body>" +
                '<div class="img-container"><img src="./KH.png" alt="Mô tả ảnh" width="120px" /></div>' + // Thêm container chứa ảnh và thuộc tính width để điều chỉnh kích thước ảnh
                // '<img src="https://hungphatsaigon.vn/wp-content/uploads/2022/07/10_hinh-nen-gau-cute.jpg" alt="Mô tả ảnh" width="100px"/>' + // Thêm đoạn mã HTML để chèn ảnh vào
                contentToPrint +
                "</body></html>"
            );
        }
        // Sử dụng nội dung để in

        printWindow.document.close();
        printWindow.print();
    };

    const getPrintStyles = () => {
        // Lấy nội dung SCSS và biên dịch thành CSS
        const cssContent = `
        .modal-order-print{
          width: 148mm !important;
          border-radius: 0%; 
          }
          .modal-order-print-content{
            padding-bottom:5px;
            font-size:15px;
            color: #4169E1;
          }
        .modal-order-print .ant-modal-content{
            border-radius: 0%;
        }
        .modal-order-print .modal-order-print-content{
            font-size: 15px;
        }
        .modal-order-print-content .modal-title{
            text-align: center;
            font-weight: bold;     
        }
        .modal-title .label-name{
                font-size: 18px;
            }
        .modal-body .modal-body-content{
            padding: 3.5px;
        }
        
        .modal-body-content .content-label{
            text-decoration:underline;
            font-weight: bold;
            font-size: 15px;
        }
        
        .modal-print-footer{
          padding: 15px 0px; 
        }
        .modal-print-footer .footer-signature{
          display: flex;
          gap:200px;
          padding:8px 5px;
        }
        .print-line-admin{
            font-size: 12px;
            padding-top: 10px; /* Điều chỉnh khoảng cách từ phần trên xuống phần line-admin */
            position: absolute;
            bottom: 0; /* Luôn đặt ở phía dưới cùng của trang */
            left: 0;
            right: 0;
        }
        b{
            color: black;
        }
        .c-red{
            color: red;
        }
        .d-flex{
            display: flex;
            gap: 2px;
        }
        `;

        return cssContent;
    };
    const getPrintStylesMobile = () => {
        // Lấy nội dung SCSS và biên dịch thành CSS
        const cssContent = `
        .modal-order-print-content{
          padding-bottom:10px !important;
          font-size:19.5px !important;
          color: #0234c9;
        }
      .modal-order-print .ant-modal-content{
          border-radius: 0%;
      }
      .modal-order-print .modal-order-print-content{
          font-size: 18px;
      }
      .modal-order-print-content .modal-title{
            padding-top:0 !important;
          text-align: center;
          font-weight: bold;     
      }
      .modal-title .label-name{
              font-size: 22.5px;
          }
      .modal-body .modal-body-content{
          padding: 3px;
      }
      
      .modal-body-content .content-label{
          text-decoration:underline;
          font-weight: bold;
          font-size: 18.5px;
      }
      
      .modal-print-footer{
        padding: 16px 0px; 
      }
      .modal-print-footer .footer-signature{
        display: flex;
        gap:200px;
        padding-top:8px;
        padding-right:5px;
        padding-left:5px;
        padding-bottom:50px !important;
      }
      .print-line-admin{
          font-size: 20px !important;
          padding-top: 14px !important; /* Điều chỉnh khoảng cách từ phần trên xuống phần line-admin */
          position: absolute;
          bottom: 0; /* Luôn đặt ở phía dưới cùng của trang */
          left: 0;
          right: 0;
      }
      .print-line-admin p{
        padding-top:6px;
    }
      b{
          color: black;
      }
      .c-red{
          color: red;
      }
      .d-flex{
          display: flex;
          gap: 2px;
      }
      `;

        return cssContent;
    };
    useEffect(() => {
        BaseURL
            .get("get-newest-order-goal")
            .then((response) => {
                if (response.status === 200) {
                    setDataOrder(response.data);
                    setPercentID(response.data.order.Percent);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, [isOpen]);

    useEffect(() => {
        BaseURL
            .get(`list-customer/${dataOrder.order && dataOrder.order.CustomerID}`)
            .then((response) => {
                if (response.status === 200) {
                    setDataCustomer(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, [dataOrder]);

    useEffect(() => {
        BaseURL
            .get(`list-category-percent/${percentID}`)
            .then((response) => {
                if (response.status === 200) {
                    setPercent(response.data.Percent);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, [percentID]);

    const GetYear = (day) => {
        if (day == null || day == '') return "....."
        const date = dayjs(day);
        // Lấy năm
        const year = date.year();
        return year;
    };

    return (
        <>
            <Modal
                success
                open={isOpen}
                onCancel={handleCancel}
                footer={[
                    <>
                        <Button key="print" onClick={handleCancel}>
                            Đóng
                        </Button>
                        <Button key="print" type="primary" onClick={handleOk}>
                            In
                        </Button>
                    </>,
                ]}
            >
                <p className="print-notification">
                    <i
                        class="fa fa-check-circle"
                        style={{ color: "green", fontSize: "30px", paddingRight: "10px" }}
                        aria-hidden="true"
                    ></i>
                    Thêm đơn cầm đồ thành công!
                </p>
                <p className="print-notification-button">Bạn có muốn in đơn này ?</p>
            </Modal>
            <div ref={contentRef}>
                <div className="order-id-line-customer">{dataOrder.order && dataOrder.order.STT + "-" + dataOrder.order.Code} (VÀNG)</div>
                <div className="modal-order-print-content">
                    <div className="modal-title">
                        <p>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
                        <p>Độc lập - Tự do - Hạnh phúc</p>
                        <div className="label-name">
                            HỢP ĐỒNG CẦM CỐ TÀI SẢN
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="modal-body-content">
                            <div className="content-label">Bên A:</div>
                            <div>
                                Họ và tên:<b> ÂU KIM HÒA</b>, Năm sinh: <b>1961</b>
                            </div>
                            <div className="d-flex">
                                Là đại diện cơ sở:
                                <b className="d-flex"> Dịch Vụ Cầm Đồ <div className="c-red"> KHẢI HOÀN</div> - ĐT: 0293.3848329</b>
                            </div>
                            <div>
                                Địa chỉ: <b>105 Quốc lộ 61 Cái Tắc - Hậu Giang</b>
                            </div>
                        </div>
                        <div className="modal-body-content">
                            <div className="content-label">Bên B:</div>
                            <div>
                                Họ và tên:<b>{dataCustomer.customer_name}</b>, Năm sinh:{" "}
                                <b>{GetYear(dataCustomer.birth_date)}</b>
                            </div>
                            <div>
                                Nơi đăng ký HKTT:<b>{dataCustomer.address}</b>
                            </div>
                            <div>
                                Số CMND/CCCD:<b>{dataCustomer.cccd ? dataCustomer.cccd : "......."}</b> Ngày cấp:
                                <b>
                                    {dataCustomer.day_cccd ? (
                                        <FormatDate date={dataCustomer.day_cccd} />
                                    ) : (
                                        "..............."
                                    )}
                                </b>{" "}
                                Nơi cấp:
                                <b>
                                    {dataCustomer.address_cccd
                                        ? dataCustomer.address_cccd
                                        : "..............."}
                                </b>
                            </div>
                        </div>
                        <div className="modal-body-content">
                            <div className="content-label">NỘI DUNG:</div>
                            <div>
                                Bên A nhận cầm tài sản Bên B mang đến, thống nhất thỏa thuận như
                                sau:
                            </div>
                            <div>
                                - Tên tài sản cầm cố:
                                <b>
                                    {dataOrder.products && dataOrder.products.map((item, index) => (
                                        <span key={index}>
                                            ({item.DetailProducts &&
                                                item.DetailProducts.map((detail, index) => {
                                                    // Make sure to return the JSX element here
                                                    return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
                                                })}
                                            {" "} {item.Product.CategoryGoldName}) {item.Product.WeightGoal} chỉ{" "}
                                        </span>))}
                                </b>
                            </div>
                            <div>
                                - Giá trị tài sản cầm cố ước tính:
                                <b>
                                    {/* <FormatCurrency amount={dataOrder.order.estimated_price} /> */}
                                </b>
                            </div>

                            <div>
                                - Số tiền cầm cố:
                                <b>
                                    <FormatCurrency amount={dataOrder.order && dataOrder.order.Price} />
                                </b>{" "}
                                Ghi bằng chữ:
                                <b>
                                    <NumberToWords amount={dataOrder.order && dataOrder.order.Price} /> đồng
                                </b>
                            </div>
                            <div>- Cầm thêm: </div>
                            <div>
                                - Lãi suất:<b>{percent} %</b>
                            </div>
                            <div>
                                - Đóng lãi:
                            </div>
                            <div>
                                - Thời gian cầm cố tài sản từ ngày:
                                <b>
                                    <FormatDate date={dataOrder.order && dataOrder.order.OrderDate} />
                                </b>{" "}
                                Đến ngày:
                                <b>
                                    <FormatDate date={dataOrder.order && dataOrder.order.ReturnDate} />
                                </b>
                            </div>
                        </div>
                        <div className="modal-print-footer">
                            <div>
                                Sau thời hạn 60 ngày (sáu mươi), kể từ ngày:
                                <b>
                                    <FormatDate date={dataOrder.order && dataOrder.order.OrderDate} />
                                </b>
                                ,Bên B không đóng lãi, chuộc tài sản và thanh lý hợp đồng thì
                                Bên A có quyền hóa giá tài sản thu hồi vốn, Bên B không được
                                quyền khiếu nại gì.
                            </div>

                            <div className="footer-signature">
                                <div>ĐẠI DIỆN BÊN B</div>
                                <div>ĐẠI DIỆN BÊN A</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="print-line-admin">
                    <p>Mã đơn: <b>{dataOrder.order && dataOrder.order.STT + "-" + dataOrder.order.Code} (VÀNG)</b></p>
                    <p>Tên khách hàng: <b>{dataCustomer.customer_name}  - SĐT: {dataCustomer.phone}</b></p>
                    <p>Tên hàng:{dataOrder.products && dataOrder.products.map((item, index) => (<b key={index}>
                        ({item.DetailProducts &&
                            item.DetailProducts.map((detail, index) => {
                                // Make sure to return the JSX element here
                                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
                            })}
                        {" "} {item.Product.CategoryGoldName} {item.Product.WeightGoal} chỉ){" "}
                    </b>))}
                    </p>
                    <p>Số tiền: <b><FormatCurrency amount={dataOrder.order && dataOrder.order.Price} /></b> - Lãi suất: <b>{percent} %</b></p>
                    <p>Ngày cầm: <b><FormatDate date={dataOrder.order && dataOrder.order.OrderDate} /></b></p>
                </div>

            </div>
        </>
    );
};
export default PrintOrderGold;

import { Button, Card, Input, Select } from "antd";
import TheContent from "../../helpers/TheContent";
import { useEffect, useState } from "react";
import BaseURL from "../../api/baseURL";
// import OverOrdersGoal from "./OverOrdersGoal";
import OverOrdersVehicle from "./OverOrdersVehicle";
import OverOrdersGoal from "./OverOrderGoal";

const Notification = () => {
  const [active, setActive] = useState(2);
  const [activeTabKey2, setActiveTabKey2] = useState("overGoal");
  const [dataOver, setDataOver] = useState([]);
  const [isOrder, setIsOrder] = useState(0);
  const [searchGoalContent, setSearchGoalContent] = useState("");
  const [searchVehicleContent, setSearchVehicleContent] = useState("");
  const [resetFlag, setResetFlag] = useState(1);
  const [expandSidebar, setExpandSidebar] = useState(false)
  const [daysDelayed, setDaysDelayed] = useState(0);

  const tabListNoTitle = [
    {
      key: "overGoal",
      tab: "Đơn vàng đã trễ hạn",
    },
    {
      key: "overVehicle",
      tab: "Đơn xe đã trễ hạn",
    },
  ];

  const contentListNoTitle = {
    overVehicle: (
      <p>
        <OverOrdersVehicle daysDelayed={daysDelayed} resetFlag={resetFlag} setResetFlag={setResetFlag} searchContent={searchVehicleContent} />
      </p>
    ),
    overGoal: (
      <p>
        <OverOrdersGoal daysDelayed={daysDelayed} resetFlag={resetFlag} setResetFlag={setResetFlag} searchContent={searchGoalContent} />
      </p>
    ),
  };
 
  const handleFilter = (e) => {
    setDaysDelayed(e);
  }
  const handleSearch = (e) => {
    const content = e.target.value;
    if (activeTabKey2 == "overGoal") {
      setSearchGoalContent(content);
      setSearchVehicleContent("");
    } if (activeTabKey2 == "overVehicle") {
      setSearchGoalContent("");
      setSearchVehicleContent(content);
    }
  };

  const onTab2Change = (key) => {
    setActiveTabKey2(key);
  };

  const content = (
    <>
      <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div class="container-fluid">
            <div class="navbar-wrapper">
              <div class="navbar-toggle">
                <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                  <span class="navbar-toggler-bar bar1"></span>
                  <span class="navbar-toggler-bar bar2"></span>
                  <span class="navbar-toggler-bar bar3"></span>
                </Button>
              </div>
              <a class="navbar-brand" href="javascript:;">
                THÔNG BÁO
              </a>
            </div>

          </div>
        </nav>
        <div class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header ">
                  <Card
                    style={{
                      width: "100%",
                      overflow: "auto",
                    }}
                    tabList= {tabListNoTitle}
                    activeTabKey={activeTabKey2}
                    tabBarExtraContent={
                      <div className="filter-search">
                        <Select defaultValue={0} onChange={handleFilter} className="select-filter">
                          <Select.Option value={0}>----Tất cả----</Select.Option>
                          <Select.Option value={1}>Trễ dưới 1 tháng</Select.Option>
                          <Select.Option value={30}>Trễ 1 tháng</Select.Option>
                          <Select.Option value={60}>Trễ 2 tháng</Select.Option>
                          <Select.Option value={90}>Trễ 3 tháng</Select.Option>
                          <Select.Option value={120}>Trễ 4 tháng</Select.Option>
                          <Select.Option value={150}>Trễ 5 tháng</Select.Option>
                          <Select.Option value={180}>Trễ hơn 5 tháng</Select.Option>
                        </Select>
                        {/* <Input placeholder="Tìm kiếm" onChange={handleSearch} /> */}
                      </div>
                    }
                    onTabChange={onTab2Change}
                  >
                    {contentListNoTitle[activeTabKey2]}
                  </Card>
                </div>
                <div class="card-body ">
                  <canvas id="chartHours" width="400" height="100"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
  return <TheContent resetFlag={resetFlag} content={content} active={active} setActive={setActive} isExpand={expandSidebar}
    setIsExpand={setExpandSidebar} />;
};
export default Notification;

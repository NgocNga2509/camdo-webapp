import { Button, ConfigProvider, DatePicker, Input, Select } from "antd";
import { useEffect, useState } from "react";
import TheContent from "../helpers/TheContent";
import viVN from "antd/lib/locale/vi_VN";
import { FormatCurrency } from "../helpers/Help";
import SpendTable from "./dashboard/SpendTable";
import locale from 'antd/es/date-picker/locale/vi_VN';
import RevenueTable from "./dashboard/RevenueTable";
import CollectTableVehicle from "./dashboard/CollectTableVehicle";
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import PrintOverView from "./print/PrintOverview";
import BaseURL from "../api/baseURL";
import VehicleToday from "./overview_vehicle_today/VehicleToday";
import 'dayjs/locale/vi';
import { Spin } from "antd";
const HomeVehicle = () => {
  const [loading, setLoading] = useState(true);
  const [active, setActive] = useState(1);
  const [subMenu, setSubMenu] = useState(false);
  const [overView, setOverView] = useState([]);
  const [isPrint, setIsPrint] = useState(false);
  const currentDate = new Date();
  const [currentMonth, setCurrentMonth] = useState(currentDate.getMonth() + 1);
  const [currentYear, setCurrentYear] = useState(currentDate.getFullYear());
  const [isTable, setIsTable] = useState(0);
  const [expandSidebar, setExpandSidebar] = useState(false);
  const [isToday, setIsToday] = useState(false);


  const handleExportExcel = (data) => {
    const params = {
      month: currentMonth,
      year: currentYear,
    };
    BaseURL
      .post("overview-vehicle-all-month", params)
      .then((response) => {
        const responseData = response.data;
        const data = Array.isArray(responseData) ? responseData : [responseData]; // Chuyển đổi dữ liệu thành mảng nếu cần thiết
        const worksheet = XLSX.utils.json_to_sheet(data);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
        const excelBuffer = XLSX.write(workbook, { type: 'array', bookType: 'xlsx' });
        const blob = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        saveAs(blob, `(xe)tong-quan-thang-${currentMonth}-${currentYear}.xlsx`);
      });
  }
  useEffect(() => {
    setLoading(true)
    const params = {
      month: currentMonth,
      year: currentYear,
    };
    BaseURL
      .post("overview-vehicle-all-month", params)
      .then((response) => {
        setOverView(response.data);
        setLoading(false)
      });
  }, [currentMonth, currentYear]);


  const handleChangeMonth = (dateString) => {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = date.getMonth() + 1; // Tháng trong JavaScript được đánh số từ 0 đến 11, vì vậy cần cộng thêm 1
    setCurrentYear(year);
    setCurrentMonth(month)
  };

  const handleExport = () => {

  }
  const content = (
    <>
      <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div class="container-fluid">
            <div class="navbar-wrapper">
              <div class="navbar-toggle">
                <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                  <span class="navbar-toggler-bar bar1"></span>
                  <span class="navbar-toggler-bar bar2"></span>
                  <span class="navbar-toggler-bar bar3"></span>
                </Button>
              </div>
              <a class="navbar-brand" href="javascript:;">
                THỐNG KÊ XE
              </a>
            </div>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navigation"
              aria-controls="navigation-index"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-bar navbar-kebab"></span>
              <span class="navbar-toggler-bar navbar-kebab"></span>
              <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div
              class="collapse navbar-collapse justify-content-end"
              id="navigation"
            >
              <div className="navbar-nav">
                <div className="nav-item" onClick={handleExport}>
                  <Button onClick={() => setIsToday(true)}>{" "} Xem hôm nay</Button>

                  <Button onClick={() => setIsPrint(true)}>
                    <i class="fa fa-print" aria-hidden="true"></i>{" "} In
                  </Button>
                  <Button onClick={() => {
                    handleExportExcel()
                  }}>Xuất thống kê</Button>
                </div>
              </div>
              <ConfigProvider locale={viVN}>
                <DatePicker picker="month" onChange={handleChangeMonth} locale={locale} />
              </ConfigProvider>
            </div>
          </div>
        </nav>
        <div class="content">
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-globe text-warning"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">Thu</p>
                        <p class="card-title">
                          <FormatCurrency amount={overView.Collect} />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr />
                  <div class="stats">
                    <i class="fa fa-calendar-check-o"></i>
                    Tháng {currentMonth + "/" + currentYear}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-money-coins text-success"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">Chi</p>
                        <p class="card-title">
                          <FormatCurrency amount={overView.Spend} />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr />
                  <div class="stats">
                    <i class="fa fa-calendar-check-o"></i>
                    Tháng {currentMonth + "/" + currentYear}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-vector text-danger"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">Lợi nhuận</p>
                        <p class="card-title">
                          <FormatCurrency amount={overView.Revenue} />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr />
                  <div class="stats">
                    <i class="fa fa-calendar-check-o"></i>
                    Tháng {currentMonth + "/" + currentYear}
                  </div>
                </div>
              </div>
            </div>
            {/* <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-favourite-28 text-primary"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">Lợi nhuận</p>
                        <p class="card-title">+45K</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr />
                  <div class="stats">
                    <i class="fa fa-refresh"></i>
                    Update now
                  </div>
                </div>
              </div>
            </div> */}
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card card-chart">
                <div class="card-header ">
                  <h5 class="card-title">LỢI NHUẬN</h5>
                  <p class="card-category">Trong tháng {currentMonth}</p>
                </div>
                <div class="card-body ">
                  <RevenueTable month={currentMonth} year={currentYear} isOrder={1} />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card card-chart">
                <div class="card-header ">
                  <h5 class="card-title">CHI TIẾT KHOẢN THU</h5>
                  <p class="card-category">
                    {isTable == 0
                      ? "Thông tin đóng lãi"
                      : "Thông tin thanh toán"}{" "}
                    trong tháng {currentMonth}
                  </p>
                </div>
                <div class="card-body ">
                  <CollectTableVehicle
                    month={currentMonth}
                    year={currentYear}
                    isTable={isTable}
                    setIsTable={setIsTable}
                  />
                </div>
                <div class="card-footer ">
                  <hr />
                  <div className="footer-overview">
                    <Button
                      className="btn-payment"
                      onClick={() => setIsTable(0)}
                    >
                      Thông tin đóng lãi
                    </Button>
                    <Button
                      className="btn-redeem"
                      onClick={() => setIsTable(1)}
                    >
                      Thông tin thanh toán
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-chart">
                <div class="card-header">
                  <h5 class="card-title">CHI TIẾT KHOẢN CHI</h5>
                  <p class="card-category">Trong tháng {currentMonth}</p>
                </div>
                <div class="card-body">
                  <SpendTable month={currentMonth} year={currentYear} />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </>
  );
  return (
    <>
      {loading ? (
        <Spin size="large" />
      ) :
        <TheContent
          subMenu={subMenu}
          setSubMenu={setSubMenu}
          content={content}
          active={active}
          setActive={setActive}
          isExpand={expandSidebar}
          setIsExpand={setExpandSidebar}
        />
      }
      {isPrint && <PrintOverView setIsPrint={setIsPrint} currentMonth={currentMonth} currentYear={currentYear} revenue={overView.AmountRevenue} collect={overView.AmountCollect} spend={overView.AmountSpend} isOrder={1} />}
      {isToday && <VehicleToday isOpen={isToday} setIsOpen={setIsToday} isTable={isTable} setIsTable={setIsTable} />}

    </>);
};
export default HomeVehicle;

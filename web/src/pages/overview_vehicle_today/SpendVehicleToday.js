import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";

const SpendVehicleToday = () => {
  const [combinedData, setCombinedData] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [paymentData, setPaymentData] = useState([]);
  useEffect(() => {
    const fetchOrderData = async () => {
      try {
        const orderDataResponse = await BaseURL.get("overview/order-vehicle-today");
        setOrderData(orderDataResponse.data);
      } catch (error) {
        console.error("Error fetching order data:", error);
      }
    };

    const fetchPaymentData = async () => {
      try {
        const paymentDataResponse = await BaseURL.get("overview/add-payment-vehicle-today");
        setPaymentData(paymentDataResponse.data);
      } catch (error) {
        console.error("Error fetching payment data:", error);
      }
    };

    fetchOrderData();
    fetchPaymentData();
  }, []);

  useEffect(() => {
    const combineData = (orderData, paymentData) => {
      const combinedData = [];
      const orderIdsSet = new Set(orderData && orderData.map((order) => order.id));

      orderData && orderData.forEach((order) => {
        const matchingPayment = paymentData && paymentData.find((payment) => payment.add_payment.id_vehicle === order.id);
        if (matchingPayment) {
          combinedData.push({
            id: order.id,
            price: order.price,
            date: order.order_date,
            price_old: order.price_old,
            product_name: order.product_name,
            license_plate: order.license_plate,
            code: order.code,
            stt: order.stt,
            note: 0
          });
        } else {
          // Nếu không có "id" trùng nhau, lấy dữ liệu từ API "add-payment/get-by-month-and-year"
          combinedData.push({
            id: order.id,
            price: order.price,
            date: order.order_date,
            price_old: order.price_old,
            product_name: order.product_name,
            license_plate: order.license_plate,
            code: order.code,
            stt: order.stt,
            note: 0
          });
        }
      });

      // Lấy dữ liệu từ API "add-payment/get-by-month-and-year" cho các "id" chưa có trong combinedData
      paymentData && paymentData.forEach((payment) => {
        if (!orderIdsSet.has(payment.add_payment.id_vehicle)) {
          combinedData.push({
            id: payment.add_payment.id_vehicle,
            price: payment.add_payment.amount, // Đặt giá trị order_goal là null vì không tìm thấy dữ liệu từ orderData
            date_payment: payment.add_payment.date_add_payment,
            product_name: payment.order.product_name,
            license_plate: payment.order.license_plate,
            code: payment.order.code,
            stt: payment.order.stt,
            price_old: null,
            note: 1
          });
        }
      });

      return combinedData;
    };

    // Call the combineData function and set the state with the combined data
    const combinedData = combineData(orderData, paymentData);
    setCombinedData(combinedData);
  }, [orderData, paymentData]);

  const dataSource = combinedData && combinedData.map((item, index) => ({
    key: index,
    ID: item.id,
    Price: item.price,
    OrderDate: item.date,
    DateAddPayment: item.date_payment,
    PriceOld: item.price_old,
    Note: item.note,
    ProductName: item.product_name,
    LicensePlate: item.license_plate,
    Code: item.code,
    STT: item.stt,
  }));
  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const pageSize = 3; // Kích thước trang

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource && dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "ID",
      key: "ID",
      render: (id, record) => <div className="text-bold">{record.STT}-{record.Code}</div>
    },
    {
      title: "Xe",
      dataIndex: "ProductName",
      key: "ProductName",
      render: (product, record) => (<>
        <div>
          {product} - {record.LicensePlate}
        </div>
      </>),
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Số tiền",
      dataIndex: "Price",
      key: "Price",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
    {
      title: "Ghi chú",
      dataIndex: "Note",
      key: "Note",
      render: (note, record) => {
        let price = record.Price - record.PriceOld
        if (note == 1) {
          return <div className="add-payment-note">Cầm thêm</div>
        }
        if (record.PriceOld) {
          return <div className="add-payment-note">
            {price > 0 && <>Có cầm thêm <FormatCurrency amount={price} /></>}
          </div>
        }
      }
    },
  ];
  return (
    <div className="table-container">
      <Table dataSource={currentPageData} columns={columns} pagination={false} />
      <Pagination showSizeChanger={false}
        className="pagination-category"
        current={currentPage}
        pageSize={pageSize}
        total={totalItems}
        onChange={handlePageChange}
      />
    </div>
  );
};
export default SpendVehicleToday;

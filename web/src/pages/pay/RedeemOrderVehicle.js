import BaseURL from "../../api/baseURL";
import moment from "moment";
import { Button, ConfigProvider, DatePicker, Form, Input, Modal } from "antd";
import { useEffect, useState } from "react";
import { Spin } from "antd";
import dayjs from "dayjs";
import {
  FormatAmountFloatToInt,
  FormatNumberFloat,
} from "../../helpers/Help";
import 'dayjs/locale/vi';
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
const RedeemOrderVehicle = ({
  isOpen,
  setIsOpen,
  orderID,
  resetFlag,
  setResetFlag,
}) => {
  const [loading, setLoading] = useState(true);
  const [dataInterest, setDataInterest] = useState("");
  const [selectedDateEnd, setSelectedDateEnd] = useState(null);
  const [countPayments, setCountPayments] = useState(0);
  const [percentID, setPercentID] = useState("");
  const [percent, setPercent] = useState(0);
  const [dayPayment, setDayPayment] = useState("");
  const [dataOrder, setDataOrder] = useState([]);
  const [redeemAmount, setRedeemAmount] = useState("")
  const handleCancel = () => {
    setIsOpen(false);
  };

  const formatNumber = (number) => {
    if (typeof number === "number") {
      const formattedNumber = number
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return formattedNumber;
    } else {
      return 0;
    }
  };

  useEffect(() => {
    if (countPayments != 0) {
      BaseURL
        .get(`get-payment-day-vehicle/${orderID}`)
        .then((response) => {
          setDayPayment(response.data);
        })
        .catch((error) => console.log(error));
    }
  }, [countPayments]);

  useEffect(() => {
    const fetchPercentID = async () => {
      try {
        const response = await BaseURL.get(
          `get-order-vehicle/${orderID}`
        );
        const fetchedPercentID = response.data.percent;
        setDataOrder(response.data);
        setPercentID(fetchedPercentID);
        setCountPayments(response.data.interest_paid_count);
      } catch (error) {
        console.log("Lỗi khi gọi API lấy percentID:", error);
      }
    };

    fetchPercentID();
  }, [orderID]);

  useEffect(() => {
    const fetchPercent = async () => {
      try {
        const response = await BaseURL.get(
          `list-category-percent/${percentID}`
        );
        const percentValue = response.data.Percent;
        setPercent(percentValue);
      } catch (error) {
        console.log("Lỗi khi gọi API lấy percent:", error);
      }
    };

    percentID != "" && fetchPercent();
  }, [percentID]);

  useEffect(() => {
    setLoading(true);
    const params = {
      start_date: countPayments != 0 ? dayPayment : dataOrder.order_date,
      order_id: orderID,
      end_date:
        selectedDateEnd == null ? formData.payment_date : selectedDateEnd,
      percent: percent,
    };
    const fetchInterest = async () => {
      try {
        const response = await BaseURL.post("interest-order-vehicle", params)
        setDataInterest(response.data);
        setLoading(false);
      } catch (error) {
        console.log("Lỗi khi gọi API lấy percent:", error);
      }
    }

    params.start_date != "" && fetchInterest()
  }, [orderID, selectedDateEnd, percent, dayPayment]);

  useEffect(() => {
    setFormData((formData) => ({
      ...formData,
      amount: formatNumber(dataInterest),
    }));
  }, [dataInterest]);

  const [formData, setFormData] = useState({
    amount: "",
    payment_date: moment(),
    next_payment_date: moment().add(1, "month"),
  });

  useEffect(() => {
    setRedeemAmount(Math.round(dataOrder.price + dataInterest))
  }, [dataInterest])

  const convertNextMonth = (day) => {
    const date = dayjs(day);
    // Lấy ngày sau 1 tháng
    const nextMonth = date.add(1, "month");

    // Lấy thông tin ngày, tháng, năm từ ngày sau 1 tháng
    const nextMonthDay = nextMonth.date();
    const nextMonthMonth = nextMonth.month() + 1; // Tháng trong Day.js bắt đầu từ 0, nên cần cộng thêm 1
    const nextMonthYear = nextMonth.year();
    // Định dạng lại thành dd/mm/yyyy
    const nextMonthFormatted = `${nextMonthDay < 10 ? "0" + nextMonthDay : nextMonthDay
      }/${nextMonthMonth < 10 ? "0" + nextMonthMonth : nextMonthMonth
      }/${nextMonthYear}`;

    const dateS = dayjs(nextMonthFormatted, "DD/MM/YYYY");

    const isoString = dateS.toISOString();

    return isoString;
  };

  const handleOk = () => {
    const params = {
      id: orderID,
      status: '1',
      model_input: {
        order_id: orderID,
        amount: parseInt(FormatAmountFloatToInt(formData.amount)),
        payment_date:
          selectedDateEnd == null ? formData.payment_date : selectedDateEnd,
        next_payment_date:
          selectedDateEnd == null
            ? formData.next_payment_date
            : convertNextMonth(selectedDateEnd),
      },
      create_redeem: {
        amount: dataOrder.price,
        date_redeem:
          selectedDateEnd == null ? formData.payment_date : selectedDateEnd,
        order_id: orderID,
      }
    }
    BaseURL.post("payment/redeem-order-vehicle", params).then((response) => {
      if (response.status === 200) {
        setResetFlag(resetFlag + 1)
        setIsOpen(false)
      }
    }).catch(error => {
      if (error.response) {
        // The request was made and the server responded with a status code
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an error
        console.log('Error', error.message);
      }
      console.log(error.config)
    })
  };
  const originalDate = new Date();
  // Lấy các thông tin ngày, tháng, năm từ ngày ban đầu
  const day = originalDate.getDate();
  const month = originalDate.getMonth() + 1; // Tháng trong JavaScript bắt đầu từ 0, nên cần cộng thêm 1
  const year = originalDate.getFullYear();

  // Định dạng lại thành dd/mm/yyyy
  const formattedDate = `${year}-${month < 10 ? "0" + month : month}-${day < 10 ? "0" + day : day
    }`;

  const handleChangeDateEnd = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDateEnd(nextDay);
      setFormData({ ...formData, payment_date: dateS });
    }
  };
  return (
    <Modal
      open={isOpen}
      onCancel={handleCancel}
      onOk={handleOk}
      footer={[
        <>
          <Button key="submit">Hủy</Button>
          <Button key="submit" type="primary" onClick={handleOk}>
            Thanh toán
          </Button>
        </>,
      ]}
      zIndex={1030}
    >

      <Form className="form-add-pay" autoSave="off" autoComplete="off">
        <Form.Item className="form-add-pay-item" label="Mã đơn">
          <Input className="form-input-pay" value={dataOrder.stt + "-" + dataOrder.code} disabled />
        </Form.Item>

        <Form.Item className="form-add-pay-item" label="Số tiền hàng">
          <Input
            className="form-input-pay-disabled"
            //name="amount"
            value={formatNumber(dataOrder.price)}
            suffix="VNĐ"
          />
        </Form.Item>
        <Form.Item className="form-add-pay-item" label="Lãi suất">
          <Input className="form-input-pay-disabled" value={percent} />
        </Form.Item>
        {loading ? (
          <Spin size="large" />
        ) :
          <div className="hight-light">
            <Form.Item className="form-add-pay-item" label="Số tiền đóng lãi">
              <Input
                className="form-input-pay-disabled"
                name="amount"
                value={FormatNumberFloat(formData.amount)}
                suffix="VNĐ"
              />
            </Form.Item>
            <Form.Item className="form-add-pay-item" label="Số tiền thanh toán">
              <Input
                className="form-input-pay-disabled"
                name="amount_redeem"
                value={formatNumber(Math.round(dataOrder.price + dataInterest))}
                onChange={(e) => setRedeemAmount(e.target.value)}
                suffix="VNĐ"
              />
            </Form.Item>
          </div>
        }
        <div className="hight-light">
          <Form.Item
            label="Ngày thanh toán"
            name="payment_date"
            className="form-add-pay-item"
          >
            <ConfigProvider locale={viVN}>
              <DatePicker
                locale={locale}
                className="form-input-pay"
                format="DD/MM/YYYY"
                defaultValue={dayjs(formattedDate, "YYYY-MM-DD")}
                name="payment_date"
                onChange={handleChangeDateEnd}
              />
            </ConfigProvider>
          </Form.Item>
        </div>
      </Form>

    </Modal>
  );
}
export default RedeemOrderVehicle
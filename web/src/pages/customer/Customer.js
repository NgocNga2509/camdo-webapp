import { useState } from "react";
import TheContent from "../../helpers/TheContent";
import { Button, ConfigProvider, DatePicker, Form, Input, Select } from "antd";
import TextArea from "antd/es/input/TextArea";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';

const Customer = () => {
  const [active, setActive] = useState(8);
  const [subMenu, setSubMenu] = useState(true);
  const [expandSidebar, setExpandSidebar] = useState(false)

  const [formData, setFormData] = useState({
    customer_name: "",
    cccd: "",
    day_cccd: "",
    address_cccd: "",
    gender: "",
    address: "",
    birth_date: null,
    phone: "",
    day_cccd: null,
    address_cccd: "CT CỤC CSHCQTTHXH",
  });
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleChangeDate = (date, dateString) => {
    const momentObj = moment(dateString, "YYYY-MM-DD");

    // Create the target string with the expected format
    const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
    setFormData({ ...formData, birth_date: targetString });
  };
  const handleChangeDateCCCD = (date, dateString) => {
    // Parse the original string as a moment object
    const momentObj = moment(dateString, "YYYY-MM-DD");

    // Create the target string with the expected format
    const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
    setFormData({ ...formData, day_cccd: targetString });
  };

  const onSelectGender = (e) => {
    setFormData({ ...formData, gender: e });
  };

  const handleSubmit = () => {
    const params = {
      customer_name: formData.customer_name,
      cccd: formData.cccd,
      gender: formData.gender,
      address: formData.address,
      birth_date: formData.birth_date,
      phone: formData.phone,
      day_cccd: formData.day_cccd,
      address_cccd: formData.address_cccd,
    };
    if (params.customer_name == "") toast("Vui lòng nhập tên khách hàng")
    else {
      BaseURL
        .post("create-customer", params)
        .then((response) => {
          if (response.status === 200) {
            toast("Thêm khách hàng thành công!");
          }
        })
        .catch((err) => toast("Thêm khách hàng thất bại!"));
    }

  };
  const content = (
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </Button>
            </div>
            <a class="navbar-brand" href="javascript:;">
              KHÁCH HÀNG
            </a>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="row">
          <div class="col-md-8">
            <div class="card ">
              <div class="card-header ">
                <Form className="form-add-pawn" autoComplete="off" autoSave="off">
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Tên khách hàng"
                  >
                    <Input
                      className="form-input-pawn"
                      onChange={handleChange}
                      name="customer_name"
                    />
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Giới tính">
                    <Select
                      placeholder="Chọn giới tính"
                      className="form-input-select"
                      onSelect={onSelectGender}
                    >
                      <Select.Option value="0">Nữ</Select.Option>
                      <Select.Option value="1">Nam</Select.Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Ngày sinh"
                  >
                    <ConfigProvider locale={viVN}>
                      <DatePicker
                        className="form-input-pawn"
                        onChange={handleChangeDate}
                        name="birth_date"
                        locale={locale}
                      />
                    </ConfigProvider>
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="CMND/CCCD">
                    <Input
                      className="form-input-pawn"
                      onChange={handleChange}
                      name="cccd"
                    />
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Ngày cấp">
                    <ConfigProvider locale={viVN}>
                      <DatePicker className="form-input-pawn" onChange={handleChangeDateCCCD}
                        name="day_cccd" />
                    </ConfigProvider>
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Nơi cấp" >
                    <Input
                      defaultValue={formData.address_cccd}
                      className="form-input-pawn"
                      onChange={handleChange}
                      name="address_cccd"
                    />
                  </Form.Item>
                  <Form.Item
                    className="form-add-pawn-item"
                    label="Số điện thoại"
                  >
                    <Input
                      className="form-input-pawn"
                      onChange={handleChange}
                      name="phone"
                    />
                  </Form.Item>
                  <Form.Item className="form-add-pawn-item" label="Hộ khẩu thường trú">
                    <TextArea
                      className="form-input-textarea"
                      onChange={handleChange}
                      name="address"
                    />
                  </Form.Item>

                  <div className="save-form-pawn">
                    <div class="update ml-auto mr-auto">
                      <button
                        onClick={() => {
                          handleSubmit();
                        }}
                        class="btn btn-primary btn-round"

                      >
                        Lưu
                      </button>
                    </div>
                  </div>
                </Form>
              </div>
              <div class="card-body ">
                <canvas id="chartHours" width="400" height="100"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  return (
    <>
      <TheContent
        content={content}
        active={active}
        setActive={setActive}
        checkSubMenuCustomer={subMenu}
        isExpand={expandSidebar}
        setIsExpand={setExpandSidebar}
      />

      <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
  );
};
export default Customer;

import React, { useEffect, useRef } from "react";
import CollectTable from "../dashboard/CollectTable";
import RevenueTable from "../dashboard/RevenueTable";
import SpendTable from "../dashboard/SpendTable";
import { FormatCurrency } from "../../helpers/Help";
import CollectTableVehicle from "../dashboard/CollectTableVehicle";
import SpendTableGold from "../dashboard/SpendTableGold";

const PrintOverView = ({ currentMonth,setIsPrint, currentYear, isOrder, collect, revenue, spend }) => {
  const contentRef = useRef(null);
  const revenueTableRef = useRef(null);
  const spendTableRef = useRef(null);
  const revenueOverviewRef = useRef(null);
  const spendOverviewRef = useRef(null);
  const collectOverviewRef = useRef(null);
  const collectTableRef1 = useRef(null);
  const collectTableRef2 = useRef(null);

  const getPrintStyles = () => {
    // CSS styles for printing
    const cssContent = `
      .table-print-overview {
        padding: 70px 70px;
        font-size: 15px;
      }
      .table-print-overview .title-overview {
        font-size: 30px;
        text-align: center;
      }
      .table-print-overview .header-overview {
        margin-bottom: 50px;
        font-size: 20px;
      }
    `;
    return cssContent;
  };
  useEffect(() => {
    const printContent = () => {
      const printWindow = window.open("", "_blank");
      printWindow && printWindow.document.open();
      printWindow && printWindow.document.write(`
        <html>
          <head>
            <style>${getPrintStyles()}</style>
          </head>
          <body>
            <div class="table-print-overview">
              <div class="title-overview">
                TỔNG QUAN ${isOrder === 0 ? "VÀNG" : "XE"} THÁNG ${currentMonth}/${currentYear}
              </div>
              <div class="header-overview">
                <div>
                  Khoản thu trong tháng ${currentMonth}/${currentYear}: <b>${collectOverviewRef.current.innerHTML}</b>
                </div>
                <div>
                  Khoản chi trong tháng ${currentMonth}/${currentYear}: <b>${spendOverviewRef.current.innerHTML}</b>
                </div>
                <div>
                  Lợi nhuận tháng ${currentMonth}/${currentYear}: <b>${revenueOverviewRef.current.innerHTML}</b>
                </div>
              </div>
              <div class="body-overview">
                <div>
                  <div>Chi tiết lợi nhuận:</div>
                  <div>${revenueTableRef.current.innerHTML}</div>
                </div>
                <div>
                  <div>Chi tiết khoản chi:</div>
                  <div>${spendTableRef.current.innerHTML}</div>
                </div>
                <div>
                  <div>Chi tiết khoản thu:</div>
                  <div>${collectTableRef1.current.innerHTML}</div>
                  <div>${collectTableRef2.current.innerHTML}</div>
                </div>
              </div>
            </div>
          </body>
        </html>
      `);
      printWindow && printWindow.document.close();
      printWindow && printWindow.print();
      setIsPrint(false);
    };
    
    setTimeout(printContent, 500); // Adjust the delay time as needed (milliseconds)
  }, [isOrder]);
  return (
    <>
      <div className="table-print-overview" ref={contentRef}>
        <div className="title-overview">
          TỔNG QUAN {isOrder === 0 ? "VÀNG" : "XE"} THÁNG {currentMonth}/{currentYear}
        </div>
        <div className="header-overview">
          <div>
            Khoản thu trong tháng {currentMonth}/{currentYear}:{" "}
            <b ref={collectOverviewRef}>
              <FormatCurrency amount={collect} />
            </b>
          </div>
          <div>
            Khoản chi trong tháng {currentMonth}/{currentYear}:{" "}
            <b ref={spendOverviewRef}>
              <FormatCurrency amount={spend} />
            </b>
          </div>
          <div>
            Lợi nhuận tháng {currentMonth}/{currentYear}:{" "}
            <b ref={revenueOverviewRef}>
              <FormatCurrency amount={revenue} />
            </b>
          </div>
        </div>
        <div className="body-overview">
          <div>
            <div>Chi tiết lợi nhuận:</div>
            <div ref={revenueTableRef}>
              {isOrder == 0 ? <RevenueTable month={currentMonth} year={currentYear} isOrder={0} isPrint={1} /> : <RevenueTable month={currentMonth} year={currentYear} isOrder={1} isPrint={1} />}
            </div>
          </div>
          <div>
            <div>Chi tiết khoản chi:</div>
            <div ref={spendTableRef}>
              {isOrder == 0 ? <SpendTableGold month={currentMonth} year={currentYear} isOrder={0} isPrint={1} /> : <SpendTable month={currentMonth} year={currentYear} isOrder={1} isPrint={1} />}
            </div>
          </div>
          <div>
            <div>Chi tiết khoản thu:</div>
            <div ref={collectTableRef1}>
              {isOrder == 0 ? <CollectTable month={currentMonth} year={currentYear} isTable={0} isPrint={1} /> : <CollectTableVehicle month={currentMonth} year={currentYear} isTable={0} isPrint={1} />}
            </div>
            <div ref={collectTableRef2}>
              {isOrder == 0 ? <CollectTable month={currentMonth} year={currentYear} isTable={1} isPrint={1} /> : <CollectTableVehicle month={currentMonth} year={currentYear} isTable={1} isPrint={1} />}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PrintOverView;

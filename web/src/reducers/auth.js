import { useReducer } from "react";

const initialTodos = [
    {
      auth:false
    }
];
  
export const reducer = (state, action) => {
    switch (action.type) {
      case "LOGIN":
        return state.auth = true
      default:
        return state;
    }
  };
export const [auth, dispatch] = useReducer(reducer, initialTodos);
import React from "react";
import "./scss/style.scss";
import { HashRouter  as Router, Routes, Route } from "react-router-dom";
import { PrivateRouter, PublicRouter } from "./routes";
import AuthWrapper from "./helpers/AuthWrapper";

const App = () => {
  return (
    <>
      <Router>
        <Routes>
          {PublicRouter.map((route, index) => (
            <Route
              key={index}
              exact={route.exact}
              path={route.path}
              element={<React.Suspense fallback={<>...</>}>{route.component}</React.Suspense>}
            />
          ))}
          {PrivateRouter.map((route, index) => (
            <Route
              key={index}
              exact={route.exact}
              path={route.path}
              element={<React.Suspense fallback={<>...</>}>{route.component}</React.Suspense>}
            />
          ))}
        </Routes>
      </Router>
    </>
  );
};

export default App;

--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: delete_collect_goal_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_goal_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền đóng lãi của đơn hàng trong bảng "collect_goal"
    UPDATE collect_goal
    SET amount = amount - OLD.amount
    WHERE month = EXTRACT(MONTH FROM OLD.payment_date) AND year = EXTRACT(YEAR FROM OLD.payment_date);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_goal_amount_function() OWNER TO postgres;

--
-- Name: delete_collect_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_goal_function() OWNER TO postgres;

--
-- Name: delete_collect_redeem_goal_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_redeem_goal_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền đóng lãi của đơn hàng trong bảng "collect_goal"
    UPDATE collect_goal
    SET amount = amount - OLD.amount
    WHERE month = EXTRACT(MONTH FROM OLD.date_redeem) AND year = EXTRACT(YEAR FROM OLD.date_redeem);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_redeem_goal_amount_function() OWNER TO postgres;

--
-- Name: delete_collect_redeem_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_redeem_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ đơn đổi bị xóa
    month_val := EXTRACT(MONTH FROM OLD.date_redeem);
    year_val := EXTRACT(YEAR FROM OLD.date_redeem);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_redeem_goal_function() OWNER TO postgres;

--
-- Name: delete_collect_redeem_vehicle_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_redeem_vehicle_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền đóng lãi của đơn hàng trong bảng "collect_vehicle"
    UPDATE collect_vehicle
    SET amount = amount - OLD.amount
    WHERE month = EXTRACT(MONTH FROM OLD.date_redeem) AND year = EXTRACT(YEAR FROM OLD.date_redeem);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_redeem_vehicle_amount_function() OWNER TO postgres;

--
-- Name: delete_collect_redeem_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_redeem_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ lệnh rút bị xóa
    month_val := EXTRACT(MONTH FROM OLD.date_redeem);
    year_val := EXTRACT(YEAR FROM OLD.date_redeem);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount - paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_redeem_vehicle_function() OWNER TO postgres;

--
-- Name: delete_collect_vehicle_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_vehicle_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền đóng lãi của đơn hàng trong bảng "collect_vehicle"
    UPDATE collect_vehicle
    SET amount = amount - OLD.amount
    WHERE month = EXTRACT(MONTH FROM OLD.payment_date) AND year = EXTRACT(YEAR FROM OLD.payment_date);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_vehicle_amount_function() OWNER TO postgres;

--
-- Name: delete_collect_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_collect_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount - paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_collect_vehicle_function() OWNER TO postgres;

--
-- Name: delete_revenue_goal_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_revenue_goal_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền doanh thu của đơn hàng trong bảng "revenue_goal"
    UPDATE revenue_goal
    SET amount = amount - OLD.amount
    WHERE month = EXTRACT(MONTH FROM OLD.payment_date) AND year = EXTRACT(YEAR FROM OLD.payment_date);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_revenue_goal_amount_function() OWNER TO postgres;

--
-- Name: delete_revenue_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_revenue_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_goal" chưa
    IF EXISTS (SELECT * FROM revenue_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_revenue_goal_function() OWNER TO postgres;

--
-- Name: delete_revenue_vehicle_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_revenue_vehicle_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền doanh thu của đơn hàng trong bảng "revenue_vehicle"
    UPDATE revenue_vehicle
    SET amount = amount - OLD.payment_amount
    WHERE month = EXTRACT(MONTH FROM OLD.payment_date) AND year = EXTRACT(YEAR FROM OLD.payment_date);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_revenue_vehicle_amount_function() OWNER TO postgres;

--
-- Name: delete_revenue_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_revenue_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_vehicle" chưa
    IF EXISTS (SELECT * FROM revenue_vehicle WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_vehicle
        SET amount = amount - paymentAmount
        WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_revenue_vehicle_function() OWNER TO postgres;

--
-- Name: delete_spend_goal_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_spend_goal_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền đặt hàng của đơn hàng trong bảng "spend_goal"
    UPDATE spend_goal
    SET amount = amount - OLD.price
    WHERE month = EXTRACT(MONTH FROM OLD.order_date) AND year = EXTRACT(YEAR FROM OLD.order_date);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_spend_goal_amount_function() OWNER TO postgres;

--
-- Name: delete_spend_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_spend_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ đơn hàng bị xóa
    month_val := EXTRACT(MONTH FROM OLD.order_date);
    year_val := EXTRACT(YEAR FROM OLD.order_date);
    paymentAmount := OLD.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_goal" chưa
    IF EXISTS (SELECT * FROM spend_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_spend_goal_function() OWNER TO postgres;

--
-- Name: delete_spend_vehicle_amount_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_spend_vehicle_amount_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Xóa số tiền đặt hàng của đơn hàng trong bảng "spend_vehicle"
    UPDATE spend_vehicle
    SET amount = amount - OLD.price
    WHERE month = EXTRACT(MONTH FROM OLD.order_date) AND year = EXTRACT(YEAR FROM OLD.order_date);
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_spend_vehicle_amount_function() OWNER TO postgres;

--
-- Name: delete_spend_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_spend_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ lệnh đặt bị xóa
    month_val := EXTRACT(MONTH FROM OLD.order_date);
    year_val := EXTRACT(YEAR FROM OLD.order_date);
    paymentAmount := OLD.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_vehicle" chưa
    IF EXISTS (SELECT * FROM spend_vehicle WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_vehicle
        SET amount = amount - paymentAmount
        WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_spend_vehicle_function() OWNER TO postgres;

--
-- Name: increase_collect_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_collect_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_collect_goal_function() OWNER TO postgres;

--
-- Name: increase_collect_redeem_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_collect_redeem_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.date_redeem);
    year_val := EXTRACT(YEAR FROM NEW.date_redeem);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_collect_redeem_goal_function() OWNER TO postgres;

--
-- Name: increase_collect_redeem_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_collect_redeem_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.date_redeem);
    year_val := EXTRACT(YEAR FROM NEW.date_redeem);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount + paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_collect_redeem_vehicle_function() OWNER TO postgres;

--
-- Name: increase_collect_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_collect_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_vehicle" chưa
    IF EXISTS (SELECT * FROM collect_vehicle WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_vehicle
        SET amount = amount + paymentAmount
        WHERE collect_vehicle.month = month_val AND collect_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO collect_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_collect_vehicle_function() OWNER TO postgres;

--
-- Name: increase_revenue_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_revenue_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_goal" chưa
    IF EXISTS (SELECT * FROM revenue_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO revenue_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_revenue_goal_function() OWNER TO postgres;

--
-- Name: increase_revenue_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_revenue_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.payment_date);
    year_val := EXTRACT(YEAR FROM NEW.payment_date);
    paymentAmount := NEW.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_vehicle" chưa
    IF EXISTS (SELECT * FROM revenue_vehicle WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_vehicle
        SET amount = amount + paymentAmount
        WHERE revenue_vehicle.month = month_val AND revenue_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO revenue_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_revenue_vehicle_function() OWNER TO postgres;

--
-- Name: increase_spend_goal_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_spend_goal_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.order_date);
    year_val := EXTRACT(YEAR FROM NEW.order_date);
    paymentAmount := NEW.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_goal" chưa
    IF EXISTS (SELECT * FROM spend_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_goal
        SET amount = amount + paymentAmount
        WHERE month = month_val AND year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO spend_goal (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_spend_goal_function() OWNER TO postgres;

--
-- Name: increase_spend_vehicle_function(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.increase_spend_vehicle_function() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán mới
    month_val := EXTRACT(MONTH FROM NEW.order_date);
    year_val := EXTRACT(YEAR FROM NEW.order_date);
    paymentAmount := NEW.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_vehicle" chưa
    IF EXISTS (SELECT * FROM spend_vehicle WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_vehicle
        SET amount = amount + paymentAmount
        WHERE spend_vehicle.month = month_val AND spend_vehicle.year = year_val;
    ELSE
        -- Chưa có bản ghi, thêm bản ghi mới
        INSERT INTO spend_vehicle (month, year, amount)
        VALUES (month_val, year_val, paymentAmount);
    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_spend_vehicle_function() OWNER TO postgres;

--
-- Name: update_interest_paid_goal_count(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.update_interest_paid_goal_count() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF TG_OP = 'INSERT' THEN
    UPDATE order_goal
    SET interest_paid_count = interest_paid_count + 1
    WHERE id = NEW.order_id;
  ELSIF TG_OP = 'DELETE' THEN
    UPDATE order_goal
    SET interest_paid_count = interest_paid_count - 1
    WHERE id = OLD.order_id;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.update_interest_paid_goal_count() OWNER TO postgres;

--
-- Name: update_interest_paid_vehicle_count(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.update_interest_paid_vehicle_count() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF TG_OP = 'INSERT' THEN
    UPDATE order_vehicle
    SET interest_paid_count = interest_paid_count + 1
    WHERE id = NEW.order_id;
  ELSIF TG_OP = 'DELETE' THEN
    UPDATE order_vehicle
    SET interest_paid_count = interest_paid_count - 1
    WHERE id = OLD.order_id;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.update_interest_paid_vehicle_count() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account (
    id character varying NOT NULL,
    name character varying NOT NULL,
    username character varying NOT NULL,
    gender integer NOT NULL,
    avatar character varying NOT NULL,
    password character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT products_avatar_check CHECK (((avatar)::text <> (''::character varying)::text)),
    CONSTRAINT products_name_check CHECK (((name)::text <> (''::character varying)::text)),
    CONSTRAINT products_password_check CHECK (((password)::text <> (''::character varying)::text)),
    CONSTRAINT products_username_check CHECK (((username)::text <> (''::character varying)::text))
);


ALTER TABLE public.account OWNER TO postgres;

--
-- Name: add_payments_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.add_payments_goal (
    id bigint NOT NULL,
    id_gold bigint,
    amount integer,
    date_add_payment date,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.add_payments_goal OWNER TO postgres;

--
-- Name: add_payments_goal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.add_payments_goal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.add_payments_goal_id_seq OWNER TO postgres;

--
-- Name: add_payments_goal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.add_payments_goal_id_seq OWNED BY public.add_payments_goal.id;


--
-- Name: add_payments_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.add_payments_vehicle (
    id bigint NOT NULL,
    id_vehicle bigint,
    amount integer,
    date_add_payment date,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.add_payments_vehicle OWNER TO postgres;

--
-- Name: add_payments_vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.add_payments_vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.add_payments_vehicle_id_seq OWNER TO postgres;

--
-- Name: add_payments_vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.add_payments_vehicle_id_seq OWNED BY public.add_payments_vehicle.id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    category_id integer NOT NULL,
    category_name character varying(255),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_category_id_seq OWNER TO postgres;

--
-- Name: category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_category_id_seq OWNED BY public.category.category_id;


--
-- Name: category_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_goal (
    category_id integer NOT NULL,
    category_name character varying(255),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.category_goal OWNER TO postgres;

--
-- Name: category_goal_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_goal_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_goal_category_id_seq OWNER TO postgres;

--
-- Name: category_goal_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_goal_category_id_seq OWNED BY public.category_goal.category_id;


--
-- Name: category_percent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_percent (
    percent_id integer NOT NULL,
    percent double precision,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.category_percent OWNER TO postgres;

--
-- Name: category_percent_percent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_percent_percent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_percent_percent_id_seq OWNER TO postgres;

--
-- Name: category_percent_percent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_percent_percent_id_seq OWNED BY public.category_percent.percent_id;


--
-- Name: collect_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.collect_goal (
    id bigint NOT NULL,
    month integer,
    year integer,
    amount bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.collect_goal OWNER TO postgres;

--
-- Name: collect_goal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.collect_goal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.collect_goal_id_seq OWNER TO postgres;

--
-- Name: collect_goal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.collect_goal_id_seq OWNED BY public.collect_goal.id;


--
-- Name: collect_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.collect_vehicle (
    id bigint NOT NULL,
    month integer,
    year integer,
    amount bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.collect_vehicle OWNER TO postgres;

--
-- Name: collect_vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.collect_vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.collect_vehicle_id_seq OWNER TO postgres;

--
-- Name: collect_vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.collect_vehicle_id_seq OWNED BY public.collect_vehicle.id;


--
-- Name: customers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customers (
    customer_id bigint NOT NULL,
    customer_name character varying(255) NOT NULL,
    cccd character varying(100),
    day_cccd date,
    address_cccd character varying(255),
    gender character varying(10),
    address character varying(255),
    birth_date date,
    phone character varying(20),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT customer_name_check CHECK (((customer_name)::text <> (''::character varying)::text))
);


ALTER TABLE public.customers OWNER TO postgres;

--
-- Name: customers_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customers_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customers_customer_id_seq OWNER TO postgres;

--
-- Name: customers_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customers_customer_id_seq OWNED BY public.customers.customer_id;


--
-- Name: detail_product_gold; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detail_product_gold (
    id bigint NOT NULL,
    detail_product_id bigint,
    category_id bigint,
    count integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.detail_product_gold OWNER TO postgres;

--
-- Name: detail_product_gold_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detail_product_gold_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detail_product_gold_id_seq OWNER TO postgres;

--
-- Name: detail_product_gold_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detail_product_gold_id_seq OWNED BY public.detail_product_gold.id;


--
-- Name: interest_payments_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.interest_payments_goal (
    payment_id bigint NOT NULL,
    order_id bigint,
    payment_date date,
    next_payment_date date,
    amount integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.interest_payments_goal OWNER TO postgres;

--
-- Name: interest_payments_goal_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.interest_payments_goal_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.interest_payments_goal_payment_id_seq OWNER TO postgres;

--
-- Name: interest_payments_goal_payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.interest_payments_goal_payment_id_seq OWNED BY public.interest_payments_goal.payment_id;


--
-- Name: interest_payments_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.interest_payments_vehicle (
    payment_id bigint NOT NULL,
    order_id bigint,
    payment_date date,
    next_payment_date date,
    amount integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.interest_payments_vehicle OWNER TO postgres;

--
-- Name: interest_payments_vehicle_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.interest_payments_vehicle_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.interest_payments_vehicle_payment_id_seq OWNER TO postgres;

--
-- Name: interest_payments_vehicle_payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.interest_payments_vehicle_payment_id_seq OWNED BY public.interest_payments_vehicle.payment_id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    version integer NOT NULL,
    created_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: order_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_goal (
    id bigint NOT NULL,
    note text,
    miss integer,
    price_old integer,
    product_name character varying,
    add_payments integer,
    price integer,
    order_date date,
    percent bigint,
    customer_id bigint,
    return_date date,
    status character varying(20),
    interest_paid_count integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.order_goal OWNER TO postgres;

--
-- Name: order_goal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_goal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_goal_id_seq OWNER TO postgres;

--
-- Name: order_goal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_goal_id_seq OWNED BY public.order_goal.id;


--
-- Name: order_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_vehicle (
    id bigint NOT NULL,
    product_name character varying,
    license_plate character varying,
    add_payments integer,
    note text,
    miss integer,
    price_old integer,
    price integer,
    order_date date,
    percent bigint,
    return_date date,
    status character varying(20),
    interest_paid_count integer,
    customer_id bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.order_vehicle OWNER TO postgres;

--
-- Name: order_vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_vehicle_id_seq OWNER TO postgres;

--
-- Name: order_vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_vehicle_id_seq OWNED BY public.order_vehicle.id;


--
-- Name: product_gold; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_gold (
    id bigint NOT NULL,
    id_goal bigint,
    weight_goal double precision,
    category_goal_id bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.product_gold OWNER TO postgres;

--
-- Name: product_gold_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_gold_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_gold_id_seq OWNER TO postgres;

--
-- Name: product_gold_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_gold_id_seq OWNED BY public.product_gold.id;


--
-- Name: redeem_order_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.redeem_order_goal (
    id bigint NOT NULL,
    amount integer,
    date_redeem date,
    order_id bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.redeem_order_goal OWNER TO postgres;

--
-- Name: redeem_order_goal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.redeem_order_goal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.redeem_order_goal_id_seq OWNER TO postgres;

--
-- Name: redeem_order_goal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.redeem_order_goal_id_seq OWNED BY public.redeem_order_goal.id;


--
-- Name: redeem_order_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.redeem_order_vehicle (
    id bigint NOT NULL,
    amount integer,
    date_redeem date,
    order_id bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.redeem_order_vehicle OWNER TO postgres;

--
-- Name: redeem_order_vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.redeem_order_vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.redeem_order_vehicle_id_seq OWNER TO postgres;

--
-- Name: redeem_order_vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.redeem_order_vehicle_id_seq OWNED BY public.redeem_order_vehicle.id;


--
-- Name: revenue_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.revenue_goal (
    id bigint NOT NULL,
    month integer,
    year integer,
    amount bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.revenue_goal OWNER TO postgres;

--
-- Name: revenue_goal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.revenue_goal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.revenue_goal_id_seq OWNER TO postgres;

--
-- Name: revenue_goal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.revenue_goal_id_seq OWNED BY public.revenue_goal.id;


--
-- Name: revenue_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.revenue_vehicle (
    id bigint NOT NULL,
    month integer,
    year integer,
    amount bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.revenue_vehicle OWNER TO postgres;

--
-- Name: revenue_vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.revenue_vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.revenue_vehicle_id_seq OWNER TO postgres;

--
-- Name: revenue_vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.revenue_vehicle_id_seq OWNED BY public.revenue_vehicle.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: spend_goal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.spend_goal (
    id bigint NOT NULL,
    month integer,
    year integer,
    amount bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.spend_goal OWNER TO postgres;

--
-- Name: spend_goal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.spend_goal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spend_goal_id_seq OWNER TO postgres;

--
-- Name: spend_goal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.spend_goal_id_seq OWNED BY public.spend_goal.id;


--
-- Name: spend_vehicle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.spend_vehicle (
    id bigint NOT NULL,
    month integer,
    year integer,
    amount bigint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.spend_vehicle OWNER TO postgres;

--
-- Name: spend_vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.spend_vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spend_vehicle_id_seq OWNER TO postgres;

--
-- Name: spend_vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.spend_vehicle_id_seq OWNED BY public.spend_vehicle.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: add_payments_goal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.add_payments_goal ALTER COLUMN id SET DEFAULT nextval('public.add_payments_goal_id_seq'::regclass);


--
-- Name: add_payments_vehicle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.add_payments_vehicle ALTER COLUMN id SET DEFAULT nextval('public.add_payments_vehicle_id_seq'::regclass);


--
-- Name: category category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN category_id SET DEFAULT nextval('public.category_category_id_seq'::regclass);


--
-- Name: category_goal category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_goal ALTER COLUMN category_id SET DEFAULT nextval('public.category_goal_category_id_seq'::regclass);


--
-- Name: category_percent percent_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_percent ALTER COLUMN percent_id SET DEFAULT nextval('public.category_percent_percent_id_seq'::regclass);


--
-- Name: collect_goal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.collect_goal ALTER COLUMN id SET DEFAULT nextval('public.collect_goal_id_seq'::regclass);


--
-- Name: collect_vehicle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.collect_vehicle ALTER COLUMN id SET DEFAULT nextval('public.collect_vehicle_id_seq'::regclass);


--
-- Name: customers customer_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers ALTER COLUMN customer_id SET DEFAULT nextval('public.customers_customer_id_seq'::regclass);


--
-- Name: detail_product_gold id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detail_product_gold ALTER COLUMN id SET DEFAULT nextval('public.detail_product_gold_id_seq'::regclass);


--
-- Name: interest_payments_goal payment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interest_payments_goal ALTER COLUMN payment_id SET DEFAULT nextval('public.interest_payments_goal_payment_id_seq'::regclass);


--
-- Name: interest_payments_vehicle payment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interest_payments_vehicle ALTER COLUMN payment_id SET DEFAULT nextval('public.interest_payments_vehicle_payment_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: order_goal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_goal ALTER COLUMN id SET DEFAULT nextval('public.order_goal_id_seq'::regclass);


--
-- Name: order_vehicle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_vehicle ALTER COLUMN id SET DEFAULT nextval('public.order_vehicle_id_seq'::regclass);


--
-- Name: product_gold id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_gold ALTER COLUMN id SET DEFAULT nextval('public.product_gold_id_seq'::regclass);


--
-- Name: redeem_order_goal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redeem_order_goal ALTER COLUMN id SET DEFAULT nextval('public.redeem_order_goal_id_seq'::regclass);


--
-- Name: redeem_order_vehicle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redeem_order_vehicle ALTER COLUMN id SET DEFAULT nextval('public.redeem_order_vehicle_id_seq'::regclass);


--
-- Name: revenue_goal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.revenue_goal ALTER COLUMN id SET DEFAULT nextval('public.revenue_goal_id_seq'::regclass);


--
-- Name: revenue_vehicle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.revenue_vehicle ALTER COLUMN id SET DEFAULT nextval('public.revenue_vehicle_id_seq'::regclass);


--
-- Name: spend_goal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spend_goal ALTER COLUMN id SET DEFAULT nextval('public.spend_goal_id_seq'::regclass);


--
-- Name: spend_vehicle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spend_vehicle ALTER COLUMN id SET DEFAULT nextval('public.spend_vehicle_id_seq'::regclass);


--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account (id, name, username, gender, avatar, password, created_at, updated_at) FROM stdin;
1	hoang	hoang	0	hoang4987	hoang	2023-08-14 15:59:19.000052+07	2023-08-14 15:59:35.427582+07
\.


--
-- Data for Name: add_payments_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.add_payments_goal (id, id_gold, amount, date_add_payment, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: add_payments_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.add_payments_vehicle (id, id_vehicle, amount, date_add_payment, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (category_id, category_name, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: category_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_goal (category_id, category_name, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: category_percent; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_percent (percent_id, percent, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: collect_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.collect_goal (id, month, year, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: collect_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.collect_vehicle (id, month, year, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customers (customer_id, customer_name, cccd, day_cccd, address_cccd, gender, address, birth_date, phone, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: detail_product_gold; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detail_product_gold (id, detail_product_id, category_id, count, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: interest_payments_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.interest_payments_goal (payment_id, order_id, payment_date, next_payment_date, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: interest_payments_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.interest_payments_vehicle (payment_id, order_id, payment_date, next_payment_date, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, version, created_at) FROM stdin;
\.


--
-- Data for Name: order_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_goal (id, note, miss, price_old, product_name, add_payments, price, order_date, percent, customer_id, return_date, status, interest_paid_count, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: order_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_vehicle (id, product_name, license_plate, add_payments, note, miss, price_old, price, order_date, percent, return_date, status, interest_paid_count, customer_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_gold; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_gold (id, id_goal, weight_goal, category_goal_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: redeem_order_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.redeem_order_goal (id, amount, date_redeem, order_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: redeem_order_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.redeem_order_vehicle (id, amount, date_redeem, order_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: revenue_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.revenue_goal (id, month, year, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: revenue_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.revenue_vehicle (id, month, year, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.schema_migrations (version, dirty) FROM stdin;
14	f
\.


--
-- Data for Name: spend_goal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spend_goal (id, month, year, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: spend_vehicle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spend_vehicle (id, month, year, amount, created_at, updated_at) FROM stdin;
\.


--
-- Name: add_payments_goal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.add_payments_goal_id_seq', 4, true);


--
-- Name: add_payments_vehicle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.add_payments_vehicle_id_seq', 3, true);


--
-- Name: category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_category_id_seq', 4, true);


--
-- Name: category_goal_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_goal_category_id_seq', 5, true);


--
-- Name: category_percent_percent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_percent_percent_id_seq', 5, true);


--
-- Name: collect_goal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.collect_goal_id_seq', 1, true);


--
-- Name: collect_vehicle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.collect_vehicle_id_seq', 1, true);


--
-- Name: customers_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customers_customer_id_seq', 1, false);


--
-- Name: detail_product_gold_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detail_product_gold_id_seq', 11, true);


--
-- Name: interest_payments_goal_payment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.interest_payments_goal_payment_id_seq', 6, true);


--
-- Name: interest_payments_vehicle_payment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.interest_payments_vehicle_payment_id_seq', 2, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 1, false);


--
-- Name: order_goal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_goal_id_seq', 5, true);


--
-- Name: order_vehicle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_vehicle_id_seq', 8, true);


--
-- Name: product_gold_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_gold_id_seq', 10, true);


--
-- Name: redeem_order_goal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.redeem_order_goal_id_seq', 1, true);


--
-- Name: redeem_order_vehicle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.redeem_order_vehicle_id_seq', 1, false);


--
-- Name: revenue_goal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.revenue_goal_id_seq', 1, true);


--
-- Name: revenue_vehicle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.revenue_vehicle_id_seq', 1, true);


--
-- Name: spend_goal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.spend_goal_id_seq', 3, true);


--
-- Name: spend_vehicle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.spend_vehicle_id_seq', 3, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: account account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- Name: add_payments_goal add_payments_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.add_payments_goal
    ADD CONSTRAINT add_payments_goal_pkey PRIMARY KEY (id);


--
-- Name: add_payments_vehicle add_payments_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.add_payments_vehicle
    ADD CONSTRAINT add_payments_vehicle_pkey PRIMARY KEY (id);


--
-- Name: category_goal category_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_goal
    ADD CONSTRAINT category_goal_pkey PRIMARY KEY (category_id);


--
-- Name: category_percent category_percent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_percent
    ADD CONSTRAINT category_percent_pkey PRIMARY KEY (percent_id);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category_id);


--
-- Name: collect_goal collect_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.collect_goal
    ADD CONSTRAINT collect_goal_pkey PRIMARY KEY (id);


--
-- Name: collect_vehicle collect_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.collect_vehicle
    ADD CONSTRAINT collect_vehicle_pkey PRIMARY KEY (id);


--
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (customer_id);


--
-- Name: detail_product_gold detail_product_gold_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detail_product_gold
    ADD CONSTRAINT detail_product_gold_pkey PRIMARY KEY (id);


--
-- Name: interest_payments_goal interest_payments_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interest_payments_goal
    ADD CONSTRAINT interest_payments_goal_pkey PRIMARY KEY (payment_id);


--
-- Name: interest_payments_vehicle interest_payments_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interest_payments_vehicle
    ADD CONSTRAINT interest_payments_vehicle_pkey PRIMARY KEY (payment_id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: order_goal order_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_goal
    ADD CONSTRAINT order_goal_pkey PRIMARY KEY (id);


--
-- Name: order_vehicle order_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_vehicle
    ADD CONSTRAINT order_vehicle_pkey PRIMARY KEY (id);


--
-- Name: product_gold product_gold_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_gold
    ADD CONSTRAINT product_gold_pkey PRIMARY KEY (id);


--
-- Name: redeem_order_goal redeem_order_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redeem_order_goal
    ADD CONSTRAINT redeem_order_goal_pkey PRIMARY KEY (id);


--
-- Name: redeem_order_vehicle redeem_order_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redeem_order_vehicle
    ADD CONSTRAINT redeem_order_vehicle_pkey PRIMARY KEY (id);


--
-- Name: revenue_goal revenue_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.revenue_goal
    ADD CONSTRAINT revenue_goal_pkey PRIMARY KEY (id);


--
-- Name: revenue_vehicle revenue_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.revenue_vehicle
    ADD CONSTRAINT revenue_vehicle_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: spend_goal spend_goal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spend_goal
    ADD CONSTRAINT spend_goal_pkey PRIMARY KEY (id);


--
-- Name: spend_vehicle spend_vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spend_vehicle
    ADD CONSTRAINT spend_vehicle_pkey PRIMARY KEY (id);


--
-- Name: interest_payments_goal delete_collect_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_collect_goal AFTER DELETE ON public.interest_payments_goal FOR EACH ROW EXECUTE FUNCTION public.delete_collect_goal_function();


--
-- Name: redeem_order_goal delete_collect_redeem_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_collect_redeem_goal AFTER DELETE ON public.redeem_order_goal FOR EACH ROW EXECUTE FUNCTION public.delete_collect_redeem_goal_function();


--
-- Name: redeem_order_vehicle delete_collect_redeem_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_collect_redeem_vehicle AFTER DELETE ON public.redeem_order_vehicle FOR EACH ROW EXECUTE FUNCTION public.delete_collect_redeem_vehicle_function();


--
-- Name: interest_payments_vehicle delete_collect_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_collect_vehicle AFTER DELETE ON public.interest_payments_vehicle FOR EACH ROW EXECUTE FUNCTION public.delete_collect_vehicle_function();


--
-- Name: interest_payments_goal delete_revenue_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_revenue_goal AFTER DELETE ON public.interest_payments_goal FOR EACH ROW EXECUTE FUNCTION public.delete_revenue_goal_function();


--
-- Name: interest_payments_vehicle delete_revenue_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_revenue_vehicle AFTER DELETE ON public.interest_payments_vehicle FOR EACH ROW EXECUTE FUNCTION public.delete_revenue_vehicle_function();


--
-- Name: order_goal delete_spend_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_spend_goal AFTER DELETE ON public.order_goal FOR EACH ROW EXECUTE FUNCTION public.delete_spend_goal_function();


--
-- Name: order_vehicle delete_spend_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_spend_vehicle AFTER DELETE ON public.order_vehicle FOR EACH ROW EXECUTE FUNCTION public.delete_spend_vehicle_function();


--
-- Name: interest_payments_goal increase_collect_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_collect_goal AFTER INSERT ON public.interest_payments_goal FOR EACH ROW EXECUTE FUNCTION public.increase_collect_goal_function();


--
-- Name: redeem_order_goal increase_collect_redeem_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_collect_redeem_goal AFTER INSERT ON public.redeem_order_goal FOR EACH ROW EXECUTE FUNCTION public.increase_collect_redeem_goal_function();


--
-- Name: redeem_order_vehicle increase_collect_redeem_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_collect_redeem_vehicle AFTER INSERT ON public.redeem_order_vehicle FOR EACH ROW EXECUTE FUNCTION public.increase_collect_redeem_vehicle_function();


--
-- Name: interest_payments_vehicle increase_collect_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_collect_vehicle AFTER INSERT ON public.interest_payments_vehicle FOR EACH ROW EXECUTE FUNCTION public.increase_collect_vehicle_function();


--
-- Name: interest_payments_goal increase_revenue_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_revenue_goal AFTER INSERT ON public.interest_payments_goal FOR EACH ROW EXECUTE FUNCTION public.increase_revenue_goal_function();


--
-- Name: interest_payments_vehicle increase_revenue_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_revenue_vehicle AFTER INSERT ON public.interest_payments_vehicle FOR EACH ROW EXECUTE FUNCTION public.increase_revenue_vehicle_function();


--
-- Name: order_goal increase_spend_goal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_spend_goal AFTER INSERT ON public.order_goal FOR EACH ROW EXECUTE FUNCTION public.increase_spend_goal_function();


--
-- Name: order_vehicle increase_spend_vehicle; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER increase_spend_vehicle AFTER INSERT ON public.order_vehicle FOR EACH ROW EXECUTE FUNCTION public.increase_spend_vehicle_function();


--
-- Name: interest_payments_goal update_interest_count_goal_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_interest_count_goal_trigger AFTER INSERT OR DELETE ON public.interest_payments_goal FOR EACH ROW EXECUTE FUNCTION public.update_interest_paid_goal_count();


--
-- Name: interest_payments_vehicle update_interest_count_vehicle_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_interest_count_vehicle_trigger AFTER INSERT OR DELETE ON public.interest_payments_vehicle FOR EACH ROW EXECUTE FUNCTION public.update_interest_paid_vehicle_count();


--
-- Name: add_payments_goal add_payments_goal_id_gold_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.add_payments_goal
    ADD CONSTRAINT add_payments_goal_id_gold_fkey FOREIGN KEY (id_gold) REFERENCES public.order_goal(id);


--
-- Name: add_payments_vehicle add_payments_vehicle_id_vehicle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.add_payments_vehicle
    ADD CONSTRAINT add_payments_vehicle_id_vehicle_fkey FOREIGN KEY (id_vehicle) REFERENCES public.order_vehicle(id);


--
-- Name: detail_product_gold detail_product_gold_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detail_product_gold
    ADD CONSTRAINT detail_product_gold_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.category(category_id);


--
-- Name: detail_product_gold detail_product_gold_detail_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detail_product_gold
    ADD CONSTRAINT detail_product_gold_detail_product_id_fkey FOREIGN KEY (detail_product_id) REFERENCES public.product_gold(id);


--
-- Name: interest_payments_goal interest_payments_goal_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interest_payments_goal
    ADD CONSTRAINT interest_payments_goal_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_goal(id);


--
-- Name: interest_payments_vehicle interest_payments_vehicle_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interest_payments_vehicle
    ADD CONSTRAINT interest_payments_vehicle_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_vehicle(id);


--
-- Name: order_goal order_goal_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_goal
    ADD CONSTRAINT order_goal_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customers(customer_id);


--
-- Name: order_goal order_goal_percent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_goal
    ADD CONSTRAINT order_goal_percent_fkey FOREIGN KEY (percent) REFERENCES public.category_percent(percent_id);


--
-- Name: order_vehicle order_vehicle_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_vehicle
    ADD CONSTRAINT order_vehicle_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customers(customer_id);


--
-- Name: order_vehicle order_vehicle_percent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_vehicle
    ADD CONSTRAINT order_vehicle_percent_fkey FOREIGN KEY (percent) REFERENCES public.category_percent(percent_id);


--
-- Name: product_gold product_gold_category_goal_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_gold
    ADD CONSTRAINT product_gold_category_goal_id_fkey FOREIGN KEY (category_goal_id) REFERENCES public.category_goal(category_id);


--
-- Name: product_gold product_gold_id_goal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_gold
    ADD CONSTRAINT product_gold_id_goal_fkey FOREIGN KEY (id_goal) REFERENCES public.order_goal(id);


--
-- Name: redeem_order_goal redeem_order_goal_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redeem_order_goal
    ADD CONSTRAINT redeem_order_goal_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_goal(id);


--
-- Name: redeem_order_vehicle redeem_order_vehicle_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redeem_order_vehicle
    ADD CONSTRAINT redeem_order_vehicle_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_vehicle(id);


--
-- PostgreSQL database dump complete
--

